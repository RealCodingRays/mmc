#pragma once

#include "umc\Platform.h"

#include <map>
#include <vector>

namespace umc {

	class InputHandler {
	public:
		virtual void onKeyDown(int key) = 0;
		virtual void onKeyUp(int key) = 0;
	};

	enum class InputLayer : int {
		GAMEPLAY = 0b100,
		MENU = 0b010,
		DEBUG = 0b001,
		ALL = 0b111,
		NONE = 0b000
	};

	class InputManager {
	public:
		void registerHandler(InputHandler *cb, int key, InputLayer layer);
		UI32 registerAxis(int positive, int negative, InputLayer layer);

		F32 getAxis(UI32 id);

		F32 getCursorX();
		F32 getCursorY();

		F32 getCursordX(InputLayer layer);
		F32 getCursordY(InputLayer layer);

		void lockCursor();
		void unlockCursor();
		void setCursorLock(bool lock);

		void enableLayer(InputLayer layer);
		void disableLayer(InputLayer layer);

		void update();

		void onKeyDown(int key);
		void onKeyUp(int key);
		void onCursorMove(F32 x, F32 y);

	private:
		InputManager();
		~InputManager();

		InputLayer layermask{ InputLayer::NONE };

		F32 cposx, cposy, cdx, cdy;

		struct axis {
			int positive;
			int negative;
			InputLayer mask;
		};

		std::map<UI32, axis> axes;
		UI32 nextAxisId{ 0 };

		struct handler {
			handler(InputHandler *hdlr, InputLayer mask) : hdlr{ hdlr }, mask{ mask } {}

			InputHandler *hdlr;
			InputLayer mask;
		};

		std::map<int, std::vector<handler>> handlers;

		friend class Engine;
	};
}