
/* PLATFORM MODULE
* Defines platform independant datatypes and definitions and provides dll loading and handling functions.
*/

#ifndef PLATFORM_H
#define PLATFORM_H

#include <assert.h>

/* Operating system definitions
* Do not use these for non preprocessor usage.
* Use platform::os, platform::getOS() and platform::getFileSeparator() instead.
*/

//Check for windows
#ifdef _WIN32
#define _P_OS
#define P_WIN32
#define P_FILE_SEPARATOR '\\'
#endif

//Check for linux
#ifdef __linux__
#define _P_OS
#define P_LINUX
#define P_FILE_SEPARATOR '/'
#endif

//Check for mac
#ifdef __APPLE__
#define _P_OS
#define P_MACOSX
#define P_FILE_SEPARATOR '/'
#endif

#ifndef _P_OS
#error Unsupported or unknown operating system.
#endif


/* Architecture definitions
* Do not use these for non preprocessor usage.
* Use platform::arch and platform::getARCH() or platform::bits and platform::getBITS() instead.
*/

//Check for x86
#if defined(_M_IX86) || defined(__i368__)
#define _P_ARCH
#define P_X86
#define P_32BIT
#endif

//Check for amd64
#if defined(__amd64__) || defined(_M_X64)
#define _P_ARCH
#define P_AMD64
#define P_64BIT
#endif

#ifndef _P_ARCH
#error Unsupported or unknown architecture.
#endif


//Standard number types definition
#define P_USE_CSTDINT
#ifdef P_USE_CSTDINT
#include <cstdint>
typedef int8_t I8;
typedef uint8_t UI8;
typedef int16_t I16;
typedef uint16_t UI16;
typedef int32_t I32;
typedef uint32_t UI32;
typedef int64_t I64;
typedef uint64_t UI64;
typedef float F32;
typedef double F64;

// Margin of error definitions for floating point types
#define F32_MOE		1e-5f
#define F64_MOE		1e-80
#endif

namespace platform {

	//Enum representing all supported operating systems
	enum class OS {
		WINDOWS,
		LINUX,
		MACOSX
	};

	//The current operating system
#if defined(P_WIN32)
	const OS os = OS::WINDOWS;
#elif defined(P_LINUX)
	const OS os = OS::LINUX;
#elif defined(P_MACOSX)
	const OS os = OS::MACOSX;
#endif

	//Enum representing all supported architectures
	enum class ARCH {
		X86,
		AMD64
	};


	//The current architecture
#if defined(P_X86)
	const ARCH arch = ARCH::X86;
#elif defined(P_AMD64)
	const ARCH arch = ARCH::AMD64;
#endif

	enum class BITS {
		B32 = 32,
		B64 = 64
	};

#if defined(P_32BIT)
	const BITS bits = BITS::B32;
#elif defined(P_64BIT)
	const BITS bits = BITS::B64;
#endif

	inline OS getOS() {
		return os;
	}

	inline ARCH getARCH() {
		return arch;
	}

	inline BITS getBITS() {
		return bits;
	}

	inline char getFileSeparator() {
		return P_FILE_SEPARATOR;
	}

	//Loading methods for dlls
	enum class LOAD_HINTS {
		LAZY,
		NOW
	};
	const LOAD_HINTS defaultHint = LOAD_HINTS::NOW;

	/* Initializes the platform module
	* Returns 1 if sucessfull and 0 if an error occured.
	*/
	int init();

	/*
	*/
	void finalize();


	/* If nescessary tries to load a library using the provided loading method if supported.
	* Returns a id > 0 used to reference the Library or an error code if loading failed.
	* Returns 0 to indicate an loading error.
	* Returns -1 to indicate that the library cannot be found.
	*/
	int loadLibrary(const char* file, LOAD_HINTS method = defaultHint);

	/* Does not load the library if it is not already loaded.
	* Returns a id > 0 used to reference the Library or 0 if it does not exist.
	*/
	int getLibrary(const char* file);

	/* Unloads a library freeing all its occupied resources.
	*/
	void freeLibrary(const int library);

	/* Unloads a library freeing all its occupied resources.
	*/
	void freeLibrary(const char *file);


	/* Searches for a function in all loaded libraries.
	* Returns a functionpointer or nullptr if the function is not found.
	*/
	void* getFunc(const char* func);

	/* Searches for a function in the specified library.
	* Returna a functionpointer or nullptr if the function is not found.
	*/
	void* getFunc(const char* func, const int library);

	/* Searches for a function in all loaded libraries and casts it to the appropriate type.
	* Returns a functionpointer or nullptr if the function is not found.
	*/
	template<typename T>
	inline T* getFunc(const char* func) {
		return static_cast<T*>(getFunc(func));
	}

	/* Searches for a function in the specified library and casts it to the appropriate type.
	* Returna a functionpointer or nullptr if the function is not found.
	*/
	template<typename T>
	inline T* getFunc(const char* func, const int library) {
		return static_cast<T*>(getFunc(func, library));
	}
}

#endif // !PLATFORM_H