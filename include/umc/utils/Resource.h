#pragma once

#include "umc\Platform.h"

namespace umc {

	class ResourceID {
	public:
		ResourceID();
		explicit ResourceID(UI64 hash);
		ResourceID(const char *name);
		ResourceID(const char *package, const char *name);
		~ResourceID();
		
		ResourceID(const ResourceID &other);
		ResourceID(ResourceID &&other);

		ResourceID &operator =(UI64 hash);
		ResourceID &operator =(const char *name);
		ResourceID &operator =(const ResourceID &other);
		ResourceID &operator =(ResourceID &&other);

		inline bool operator >(const ResourceID &other) const;
		inline bool operator >=(const ResourceID &other) const;
		inline bool operator <(const ResourceID &other) const;
		inline bool operator <=(const ResourceID &other) const;
		inline bool operator ==(const ResourceID &other) const;

		inline UI64 getID() const;
		inline operator UI64() const;

	protected:
		UI64 hash{ 0 };
	};

	class ResourceName : public ResourceID {
	public:
		ResourceName();
		ResourceName(const char *name);
		ResourceName(const char *package, const char *name);
		~ResourceName();

		ResourceName(const ResourceName &other);
		ResourceName(ResourceName &&other);

		ResourceID &operator =(UI64 hash) = delete;
		ResourceID &operator =(const ResourceID &other) = delete;
		ResourceID &operator =(ResourceID &&other) = delete;

		ResourceName &operator =(const char *name);
		ResourceName &operator =(const ResourceName &other);
		ResourceName &operator =(ResourceName &&other);

		void set(const char *name);
		void set(const char *package, const char *name);
		void setPackage(const char *package);
		void setName(const char *name);

		void addSuffix(const char *suffix);

		bool isValid() const;

		inline const char *getPackage() const;
		inline const char *getName() const;

		static bool isValidName(const char *name);

	protected:
		char *package{ nullptr };
		char *name{ nullptr };
	};

	//
	// Inline function definitions
	//

	bool ResourceID::operator >(const ResourceID &other) const {
		return hash > other.hash;
	}

	bool ResourceID::operator >=(const ResourceID &other) const {
		return hash >= other.hash;
	}

	bool ResourceID::operator <(const ResourceID &other) const {
		return hash < other.hash;
	}

	bool ResourceID::operator <=(const ResourceID &other) const {
		return hash <= other.hash;
	}

	bool ResourceID::operator ==(const ResourceID &other) const {
		return hash == other.hash;
	}

	UI64 ResourceID::getID() const {
		return hash;
	}

	ResourceID::operator UI64() const {
		return hash;
	}

	const char *ResourceName::getPackage() const {
		return package;
	}

	const char *ResourceName::getName() const {
		return name;
	}
}