#pragma once

#include "umc\Platform.h"

#include "umc\utils\ResourcePath.h"

#include <boost\filesystem.hpp>
#include <map>
#include <vector>
#include <string>

namespace umc {

	class Properties {
	public:
		Properties();
		Properties(const boost::filesystem::path &file);
		Properties(const ResourcePath &name);

		~Properties();

		Properties(const Properties &other);
		Properties(Properties &&other);

		const char *getoc(const char *name, const char *def);
		bool getocBool(const char *name, const bool def);
		I64 getocInt(const char *name, const I64 def);
		F64 getocFloat(const char *name, const F64 def);

		const char *get(const char *name) const;
		bool getBool(const char *name) const;
		I64 getInt(const char *name) const;
		F64 getFloat(const char *name) const;

		void set(const char *name, const char *val);
		void setBool(const char *name, const bool val);
		void setInt(const char *name, const I64 val);
		void setFloat(const char *name, const F64 val);

		void setFile(const ResourcePath &name);
		void setFile(const boost::filesystem::path &file);

		void load();
		void load(const boost::filesystem::path &file);
		void load(const ResourcePath &name);

		void write();
		void write(const boost::filesystem::path &file);
		void write(const ResourcePath &name);

		Properties &operator =(const Properties &other);
		Properties &operator =(Properties &&other);

	private:
		enum class valuetype {
			INTEGER,
			FLOAT,
			BOOLEAN,
			STRING
		};

		union datatype {
			char *str;
			bool b;
			UI64 ui64;
			I64 i64;
			F64 f64;
		};

		struct valueptr {
			valuetype type;
			datatype data;
		};

		std::map<std::string, valueptr> props;
		std::vector<char *> strs;

		boost::filesystem::path path;
	};

	//
	// Inline function definitions
	//

}