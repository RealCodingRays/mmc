#pragma once

#include "umc\Platform.h"

#include <glm\glm.hpp>

namespace umc {

	class AABB {
	public:
		inline AABB();
		inline AABB(glm::vec3 p1, glm::vec3 p2);
		inline AABB(glm::vec3 mid, float radius);

		inline bool raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &normal) const;
		inline bool isInside(glm::vec3 pos) const;
		inline bool isInside(const AABB &other) const;

		glm::vec3 min, max;
	};

	//
	// Inline function definitions
	//

	AABB::AABB() {}

	AABB::AABB(glm::vec3 p1, glm::vec3 p2) : min{ glm::min(p1, p2) }, max{ glm::max(p1, p2) } {
	}

	AABB::AABB(glm::vec3 mid, float radius) : min{ mid - radius }, max{ mid + radius } {
	}

	bool AABB::raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &normal) const {
		glm::vec3 min = this->min - F32_MOE;
		glm::vec3 max = this->max + F32_MOE;

		// Positive X
		float dot = glm::dot(dir, glm::vec3{ 1, 0, 0 });
		if (dot < 0) {
			float d = max.x;
			float l = (d - origin.x) / dir.x;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ this->max.x - F32_MOE, min.y, min.z })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ this->max.x + F32_MOE, max.y, max.z }))) {
				hit = pos;
				normal = glm::vec3{ 1, 0, 0 };
				return true;
			}
		}

		// Negative X
		else if (dot > 0) {
			float d = min.x;
			float l = (d - origin.x) / dir.x;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ this->min.x - F32_MOE, min.y, min.z })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ this->min.x + F32_MOE, max.y, max.z }))) {
				hit = pos;
				normal = glm::vec3{ -1, 0, 0 };
				return true;
			}
		}

		// Positive Z
		dot = glm::dot(dir, glm::vec3{ 0, 0, 1 });
		if (dot < 0) {
			float d = max.z;
			float l = (d - origin.z) / dir.z;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ min.x, min.y, this->max.z - F32_MOE })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ max.x, max.y, this->max.z + F32_MOE }))) {
				hit = pos;
				normal = glm::vec3{ 0, 0, 1 };
				return true;
			}
		}

		// Negative Z
		else if (dot > 0) {
			float d = min.z;
			float l = (d - origin.z) / dir.z;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ min.x, min.y, this->min.z - F32_MOE })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ max.x, max.y, this->min.z + F32_MOE }))) {
				hit = pos;
				normal = glm::vec3{ 0, 0, -1 };
				return true;
			}
		}

		// Positive Y
		dot = glm::dot(dir, glm::vec3{ 0, 1, 0 });
		if (dot < 0) {
			float d = max.y;
			float l = (d - origin.y) / dir.y;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ min.x, this->max.y - F32_MOE, min.z })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ max.x, this->max.y + F32_MOE, max.z }))) {
				hit = pos;
				normal = glm::vec3{ 0, 1, 0 };
				return true;
			}
		}

		// Negative Y
		else if (dot > 0) {
			float d = min.y;
			float l = (d - origin.y) / dir.y;
			glm::vec3 pos = origin + l * dir;

			if (glm::all(glm::greaterThanEqual(pos, glm::vec3{ min.x, this->min.y - F32_MOE, min.z })) && glm::all(glm::lessThanEqual(pos, glm::vec3{ max.x, this->min.y + F32_MOE, max.z }))) {
				hit = pos;
				normal = glm::vec3{ 0, -1, 0 };
				return true;
			}
		}

		return false;
	}

	bool AABB::isInside(glm::vec3 pos) const {
		return glm::all(glm::greaterThanEqual(pos, min - F32_MOE)) && glm::all(glm::lessThanEqual(pos, max + F32_MOE));
	}

	bool AABB::isInside(const AABB &other) const {
		return glm::all(glm::greaterThanEqual(other.min, min - F32_MOE)) && glm::all(glm::lessThanEqual(other.max, max + F32_MOE));
	}
}