#pragma once

#include "umc\Platform.h"

#include "umc\utils\Resource.h"

#include <boost\filesystem\path.hpp>

namespace umc {

	class ResourcePath : public ResourceID {
	public:
		ResourcePath();
		ResourcePath(const char *name);
		ResourcePath(const char *package, const char *name);
		ResourcePath(const char *package, const boost::filesystem::path &path);
		ResourcePath(const ResourceName &name);
		~ResourcePath();

		ResourcePath(const ResourcePath &other);
		ResourcePath(ResourcePath &&other);

		ResourceID &operator =(const ResourceID &other) = delete;
		ResourceID &operator =(ResourceID &&other) = delete;

		ResourcePath &operator =(const char *name);
		ResourcePath &operator =(const ResourceName &name);
		ResourcePath &operator =(const ResourcePath &other);
		ResourcePath &operator =(ResourcePath &&other);

		void set(const char *name);
		void set(const char *package, const char *name);
		void setPackage(const char *package);
		void setName(const char *name);

		const char *getPackage() const;

		void setRelativePath(const boost::filesystem::path &path);
		const boost::filesystem::path &getRelativePath() const;

		boost::filesystem::path toAbsolutePath() const;
		operator boost::filesystem::path() const;

		ResourceName toResourceName() const;
		operator ResourceName() const;

	protected:
		char *package{ nullptr };
		boost::filesystem::path path;
	};
}