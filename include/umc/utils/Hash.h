#pragma once

#include "umc\Platform.h"

#include <cstring>

namespace umc {

	/* 
	 * Array hash algorithm using FNV-1a
	 */
	template<class T>
	inline UI64 arrayHash(const T *str, size_t size) {
		const UI64 *ptr = reinterpret_cast<const UI64 *>(str);
		size_t nsize = (size * sizeof(T)) / 8;

		UI64 hash = 14695981039346656037;
		for (size_t i = 0; i < nsize; i++) {
			hash = hash ^ (*(ptr + i));
			hash *= 1099511628211;
		}
		size_t left = (size * sizeof(T)) % 8;
		if (left) {
			UI64 num = 0;
			for (size_t i = 0; i < left; i++) {
				num |= static_cast<UI64>(*(reinterpret_cast<const UI8 *>(ptr + nsize - 1) + i)) << (i * 8);
			}
			hash = hash ^ num;
			hash *= 1099511628211;
		}
		return hash;
	}

	inline UI64 stringHash(const char *str) {
		return arrayHash<char>(str, std::strlen(str));
	}

	inline UI64 resourceHash(const char *package, const char *name) {
		size_t plen = strlen(package);
		size_t nlen = strlen(name);
		char *fullname = new char[plen + nlen + 2];
		strcpy_s(fullname, plen, package);
		fullname[plen] = ':';
		strcpy_s(fullname + plen + 1, nlen, name);
		fullname[plen + nlen + 1] = 0;

		UI64 hash = stringHash(fullname);

		delete[] fullname;

		return hash;
	}
}