#pragma once

#include "umc\Platform.h"

#include "umc\world\Collider.h"

namespace umc {
	
	class BoxCollider {
	public:
		BoxCollider();
		BoxCollider(const AABB &box);
		BoxCollider(glm::vec3 p1, glm::vec3 p2);
		BoxCollider(glm::vec3 mid, F32 radius);

		void setDomain();
		void setDomainNoUpdate();

		virtual bool isInRange(glm::vec3 origin, glm::vec3 dir) const;
		virtual bool raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &norm) const;

		virtual const AABB &getDomain() const;

		virtual WorldObject *getParentObject();
		virtual WorldObject *getControllObject();

	private:
		AABB box;
	};
}