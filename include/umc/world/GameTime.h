#pragma once

#include "umc\Platform.h"

namespace umc {

	class GameTime {
	public:
		GameTime();
		explicit GameTime(UI64 time);
		GameTime(UI32 d, UI32 h = 0, UI32 m = 0, UI32 s = 0);

		GameTime(const GameTime &other);
		GameTime(GameTime &&other);

	private:
		UI64 time;
	};

	class GameTimeClock {
	public:

	};
}