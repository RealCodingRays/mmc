#pragma once

#include "umc\Platform.h"

#include "umc\world\Block.h"
#include "umc\world\BlockRegistry.h"
#include "umc\world\BlocksRenderer.h"
#include "umc\world\Collision.h"

#include "umc\world\WorldObject.h"

#include "umc\renderer\GLObjects.h"
#include "umc\renderer\Renderer.h"

#include <boost\pool\pool_alloc.hpp>
#include <map>

namespace umc {

	class BlockDomain {
	public:
		virtual bool next(ivec3 &vec) = 0;
		virtual size_t getMaxCount() const = 0;
	};

	class BlockCreationRule {
		virtual bool generate(Block &block) = 0;
	};

	class World {
	public:
		World();
		~World();

		Block &getBlock(ivec3 pos);

		void fill(BlockDomain &domain, BlockCreationRule &rule);
		void setBlock(ivec3 pos, BlockFacing dir, BlockID id);
		void setBlock(const Block &block);

		BlockInstance *getInstance(ivec3 pos);
		void setInstance(ivec3 pos, BlockInstance *instance);

		BlockID getBlockID(ivec3 pos);
		bool isSolidBlock(ivec3 pos);
		bool isVisible(ivec3 pos);

		void updateBlock(ivec3 pos);

		Renderer *getRenderer();
		WorldCollision &getCollsion();

		void update(F32 dt);

		static constexpr I8 BLOCK_MIN{ -64 };
		static constexpr I8 BLOCK_MAX{ 63 };
		static constexpr F32 WORLD_CENTER{ -0.5f };
		static constexpr F32 WORLD_DIAMETER{ 128.f };

		WorldObject publobj;

	private:

		struct SolidBlock {
			BlockInstance *instance;
			BlockFacing dir;
		};

		struct AirBlock {
			glm::vec3 flow;
			F32 pressure;
		};

		union BlockUnion {
			BlockUnion();

			SolidBlock solid;
			AirBlock air;
		};

		struct BlockData {
			BlockID id{ 0 };
			BlockUnion data;
		};

		BlockData blocks[128*128*128];

		BlocksRenderer renderer;
		WorldCollision collision;

		inline BlockData &findBlock(ivec3 pos);
	};

	//
	// Inline function definitions
	//

	inline WorldCollision &World::getCollsion() {
		return collision;
	}
}