#pragma once

#include "umc\Platform.h"

#include "umc\renderer\Renderer.h"
#include "umc\renderer\TextureRegistry.h"
#include "umc\world\Block.h"

#include "umc\renderer\GLObjects.h"

#include <boost\pool\pool_alloc.hpp>
#include <vector>
#include <map>

namespace umc {

	class BlocksRenderer : public Renderer {
	public:
		BlocksRenderer();
		~BlocksRenderer();

		void deinit();

		/* 
		 * Specifies a block face to render. BlockSide is local
		 */
		void insert(const ivec3 *pos, const BlockFacing *dir, const UI8 *side, ResourceID texture, size_t len = 1);
		void remove(ivec3 pos);
		void remove(ivec3 pos, UI8 side);

		virtual void draw(const PipelineInfo &info);

		static void initStatic();
		static void deinitStatic();

	private:
		struct faceData {
			ivec3 pos;
			imat3 rot;
		};

		class ttypeset {
		public:
			void init(gl::GLsizei size);
			void deinit();

			I32 reserve(size_t n);
			void insert(I32 index, faceData *data, size_t n);
			bool removeFace(I32 index, ivec3 &lpos, I32 &oindex);

			void resize(gl::GLsizei nsize);

			ResourceID texture;
			gl::GLsizei facecnt{ 0 };
			gl::GLsizei memsize{ 0 };
			gl::GLuint buffer{ 0 };
			gl::GLuint vao{ 0 };
		};

		struct faceInfo {
			ResourceID texture[6];
			I32 index[6]{ -1, -1, -1, -1, -1, -1, };
		};

		std::vector<ttypeset> data;
		std::map<ResourceID, UI32, std::less<ResourceID>, boost::pool_allocator<UI32>> indexes;
		std::map<UI32, faceInfo, std::less<UI32>, boost::pool_allocator<faceInfo>> faces;

		void removeFace(I32 index, ResourceID id);

		static void configureVao(gl::GLuint vao, gl::GLuint buffer);

		static GLBuffer vertecies;
		static GLProgram program;
	};
}