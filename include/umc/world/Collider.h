#pragma once

#include "umc\Platform.h"

#include "umc\utils\Resource.h"
#include "umc\utils\AABB.h"
#include "umc\world\WorldObject.h"

namespace umc {
	
	class Collider {
	public:
		virtual ~Collider();

		virtual bool isInRange(glm::vec3 origin, glm::vec3 dir) const = 0;
		virtual bool raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &norm) const = 0;

		virtual const AABB &getDomain() const = 0;

		virtual WorldObject *getParentObject() = 0;
		virtual WorldObject *getControllObject() = 0;
	};
}