#pragma once

#include "umc\Platform.h"

#include <glm\vec3.hpp>
#include <glm\mat3x3.hpp>

namespace umc {

	typedef glm::tvec3<I8, glm::highp> ivec3;
	typedef glm::tmat3x3<I8, glm::highp> imat3;

	inline UI32 ivec3ToHash(ivec3 vec);
	inline ivec3 hashToIvec3(UI32 hash);

	inline ivec3 up(const ivec3 &pos, I8 steps = 1);
	inline ivec3 down(const ivec3 &pos, I8 steps = 1);
	inline ivec3 east(const ivec3 &pos, I8 steps = 1);
	inline ivec3 west(const ivec3 &pos, I8 steps = 1);
	inline ivec3 north(const ivec3 &pos, I8 steps = 1);
	inline ivec3 south(const ivec3 &pos, I8 steps = 1);

	class WorldDirection {
	public:
		static const ivec3 UP;
		static const ivec3 DOWN;
		static const ivec3 NORTH;
		static const ivec3 SOUTH;
		static const ivec3 EAST;
		static const ivec3 WEST;
	};

	class BlockSide {
	public:
		static const UI8 FRONT{ 0 };
		static const UI8 RIGHT{ 1 };
		static const UI8 BACK{ 2 };
		static const UI8 LEFT{ 3 };
		static const UI8 TOP{ 4 };
		static const UI8 BOTTOM{ 5 };

		static ivec3 sideToDir(UI8 side);
		static UI8 dirToSide(ivec3 dir);
	};

	class BlockFacing {
	public:
		BlockFacing();
		BlockFacing(ivec3 forward, ivec3 upward = WorldDirection::UP);

		BlockFacing(const BlockFacing &other);
		BlockFacing(BlockFacing &&other);

		BlockFacing &operator =(const BlockFacing &other);
		BlockFacing &operator =(BlockFacing &&other);

		bool operator ==(const BlockFacing &other) const;

		void setForward(ivec3 forward);
		void setUpward(ivec3 upward);
		void rotate(I8 steps);
		void roll(I8 steps);

		ivec3 front() const;
		ivec3 back() const;
		ivec3 right() const;
		ivec3 left() const;
		ivec3 up() const;
		ivec3 down() const;

		imat3 toMatrixLTW() const;
		imat3 toMatrixWTL() const;
		ivec3 translateLTW(ivec3 dir) const;
		ivec3 translateWTL(ivec3 dir) const;
		UI8 getSideLTW(ivec3 dir) const;
		UI8 getSideWTL(ivec3 dir) const;

		static const BlockFacing NORTH;
		static const BlockFacing SOUTH;
		static const BlockFacing EAST;
		static const BlockFacing WEST;

	private:
		UI8 facing;

		static bool isValidDir(ivec3 forward, ivec3 upward);
	};

	typedef UI16 BlockID;

	class BlockInstance;

	class Block {
	public:
		inline Block();
		inline Block(BlockID id);

		inline operator BlockID() const;
		inline operator BlockInstance *();

		BlockInstance *instance;
		BlockID id;
		ivec3 position;
		BlockFacing facing;
	};

	//
	// Inline Function definitions
	//
	inline UI32 ivec3ToHash(ivec3 vec) {
		return static_cast<UI32>((static_cast<UI8>(vec.x) << 16) | (static_cast<UI8>(vec.y) << 8) | static_cast<UI8>(vec.z));
	}

	inline ivec3 hashToIvec3(UI32 hash) {
		return ivec3{ static_cast<I8>((hash >> 16) & 0xFF), static_cast<I8>((hash >> 8) & 0xFF), static_cast<I8>((hash) & 0xFF) };
	}

	inline ivec3 up(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::UP * steps;
	}

	inline ivec3 down(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::DOWN * steps;
	}

	inline ivec3 east(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::EAST * steps;
	}

	inline ivec3 west(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::WEST * steps;
	}

	inline ivec3 north(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::NORTH * steps;
	}

	inline ivec3 south(const ivec3 &pos, I8 steps) {
		return pos + WorldDirection::SOUTH * steps;
	}

	Block::Block() {
	}

	Block::Block(BlockID id) : id{ id } {
	}

	Block::operator umc::BlockID() const {
		return id;
	}

	Block::operator umc::BlockInstance *() {
		return instance;
	}
}