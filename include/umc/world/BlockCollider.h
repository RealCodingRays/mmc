#pragma once

#include "umc\Platform.h"

#include "umc\world\Collider.h"

#include "umc\utils\AABB.h"
#include "umc\world\Block.h"

namespace umc {

	class BlockCollider : public Collider {
	public:
		BlockCollider(ivec3 pos);
		virtual ~BlockCollider();

		virtual bool isInRange(glm::vec3 origin, glm::vec3 dir) const;
		virtual bool raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &norm) const;

		virtual const AABB &getDomain() const;

		virtual WorldObject *getParentObject();
		virtual WorldObject *getControllObject();

	private:
		AABB box;
	};
}