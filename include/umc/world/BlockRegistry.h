#pragma once

#include "umc\Platform.h"
#include "umc\world\Block.h"

#include "umc\utils\Resource.h"

#include <vector>
#include <map>

namespace umc {

	class BlockInstane;

	class BlockFactory {
	public:
		virtual void initBlock(Block &block) = 0;
	};

	class BlockRegistry {
	public:
		BlockRegistry();
		~BlockRegistry();

		BlockID registerBlock(ResourceName name);
		BlockID getBlockID(ResourceID name);

		void setBlockFactory(BlockID id, BlockFactory *factory);
		void setBlockTexture(BlockID id, UI8 side, ResourceID texture);
		void setBlockTexture(BlockID id, ResourceID texture);
		void setBlockTexture(BlockID id, ResourceID side, ResourceID up, ResourceID down);
		void setBlockTexture(BlockID id, ResourceID front, ResourceID right, ResourceID back, ResourceID left, ResourceID up, ResourceID down);

		ResourceID getBlockTexture(BlockID id, UI8 side);

		void createBlock(BlockID id, Block &block);
		Block createBlock(BlockID id);

	private:
		struct BlockConfig {
			BlockFactory *factory{ nullptr };
			ResourceID textures[6];
		};

		std::map<ResourceID, BlockID> names;
		std::vector<BlockConfig> blocks;
	};
}