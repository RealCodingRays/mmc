#pragma once

#include "umc\Platform.h"

#include "umc\world\Collider.h"
#include "umc\world\Block.h"

#include <glm\vec3.hpp>
#include <boost\pool\pool_alloc.hpp>
#include <vector>
#include <map>

namespace umc {

	class RaycastHit {
	public:
		enum class HitType {
			NONE,
			BLOCK,
			ENTITY,
			OTHER
		};

		RaycastHit();
		RaycastHit(const glm::vec3 &origin, const glm::vec3 &direction);

		glm::vec3 origin{ 0.f };
		glm::vec3 direction{ 0.f };

		glm::vec3 hit{ 0.f };
		glm::vec3 normal{ 0.f };
		ivec3 blockpos{ 0 };

		HitType type{ HitType::NONE };
		Collider *object{ nullptr };
	};

	class WorldCollision {
	public:
		WorldCollision();
		~WorldCollision();

		void registerBlock(ivec3 pos);
		void registerBlockCollider(Collider *collider, ivec3 position);
		void registerCollider(Collider *collider);

		void removeBlock(ivec3 pos);
		void removeCollider(Collider *collider);

		bool raycast(RaycastHit &ray);
		bool raycastBlocks(RaycastHit &ray);
		bool raycastEntities(RaycastHit &ray);

	private:
		struct collref {
			Collider *collider{ nullptr };
			ivec3 position{ CHAR_MIN };
		};

		class TreeNode {
		public:
			TreeNode(glm::vec3 mid, F32 size);
			~TreeNode();

			void insertCollider(Collider *collider, ivec3 position, int level);

			bool removeCollider(Collider *collider);
			bool removeColliderAll(Collider *collider);

			bool isInside(glm::vec3 pos);
			bool isInside(const AABB &aabb);

			void raycast(RaycastHit &ray, F32 &dist2);
			bool raycastBlocks(RaycastHit &ray, F32 &dist2);
			bool raycastEntities(RaycastHit &ray, F32 &dist2);

			bool isEmpty();

			TreeNode *next[8]{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
			std::vector<collref> colliders;
			glm::vec3 mid;
			F32 radius;

			static boost::pool_allocator<TreeNode> nodeAllocator;
		};

		TreeNode root;
		std::map<UI32, Collider*, std::less<UI32>, boost::pool_allocator<Collider*>> blocks;
	};

	//
	// Inline function definitions
	//

	inline RaycastHit::RaycastHit() {
	}

	inline RaycastHit::RaycastHit(const glm::vec3 &origin, const glm::vec3 &direction) : origin{ origin }, direction{ direction } {
	}
}