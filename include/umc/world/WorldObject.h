#pragma once

#include "umc\Platform.h"

#include "umc\utils\Resource.h"
#include "umc\world\Transform.h"

#include <vector>

namespace umc {

	class LogicComponent {
	public:
		virtual ~LogicComponent();

		virtual void attach(WorldObject &obj);
		virtual void detach(WorldObject &obj);

		virtual void update(F32 dt, WorldObject &obj) = 0;
	};

	class WorldObject {
	public:
		WorldObject();
		~WorldObject();

		WorldObject(WorldObject &other);

		void setCollider(const ResourceID &collider);
		void removeCollider();

		void setLogicComponent(LogicComponent *component);
		LogicComponent *getLogicComponent();

		void update(F32 dt);

		Transform transform;

	private:
		void deepClone(WorldObject &other);

		LogicComponent *logic{ nullptr };
	};
}