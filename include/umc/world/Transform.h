#pragma once

#include "umc\Platform.h"

#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>

#include <vector>

namespace umc {

	class WorldObject;

	class Transform {
	public:
		Transform();
		Transform(const glm::quat &rotation, const glm::vec3 &scale, const glm::vec3 &position);
		~Transform();

		Transform(const Transform &other);
		Transform(Transform &&other);

		Transform &operator =(const Transform &other);
		Transform &operator =(Transform &&other);

		void rotate(F32 rad, glm::vec3 axis);
		inline void rotateDeg(F32 deg, glm::vec3 axis);

		void rotateX(F32 rad);
		inline void rotateXDeg(F32 deg);

		void rotateY(F32 rad);
		inline void rotateYDeg(F32 deg);

		void rotateZ(F32 rad);
		inline void rotateZDeg(F32 deg);

		glm::quat getRotation() const;

		void setRotation(glm::quat rot);

		void scale(glm::vec3 factor);
		
		void scaleX(F32 factor);
		void scaleY(F32 factor);
		void scaleZ(F32 factor);

		glm::vec3 getScale() const;

		void setScale(glm::vec3 scale);

		void translate(glm::vec3 trans);
		void translateForward(glm::vec3 trans);

		glm::vec3 getPosition() const;

		void setPosition(glm::vec3 pos);

		void setParent(Transform *transform);

		glm::mat4 worldToLocal() const;
		glm::mat4 localToWorld() const;

		Transform *parent{ nullptr };
		std::vector<Transform *> children;

		WorldObject *object;

	protected:
		void removeChild(Transform *child);
		void addChild(Transform *child);
		void removeParent();

		glm::quat rotation;
		glm::vec3 fscale{ 1.f };
		glm::vec3 position{ 0.f };
	};

	class CameraTransform {
	public:
		CameraTransform();
		CameraTransform(F32 limits);

		CameraTransform(const CameraTransform &other);
		CameraTransform(CameraTransform &&other);

		CameraTransform &operator =(const CameraTransform &other);
		CameraTransform &operator =(CameraTransform &&other);

		void setLimits(F32 rad);
		inline void setLimitsDeg(F32 deg);

		F32 getLimits() const;
		inline F32 getLimitsDeg() const;

		void rotateVertical(F32 rad);
		inline void rotateVerticalDeg(F32 deg);

		F32 getRotationVertical() const;
		inline F32 getRotationVerticalDeg() const;

		void setRotationVertical(F32 rad);
		inline void setRotationVerticalDeg(F32 deg);

		void rotateHorizontal(F32 rad);
		inline void rotateHorizontalDeg(F32 deg);

		F32 getRotationHorizontal() const;
		inline F32 getRotationHorizontalDeg() const;

		void setRotationHorizontal(F32 rad);
		inline void setRotationHorizontalDeg(F32 deg);

		glm::vec3 getForward();

		void translate(F32 x, F32 y, F32 z);
		void translate(glm::vec3 trans);

		void translateForward(F32 x, F32 y, F32 z);
		void translateForward(glm::vec3 trans);

		F32 getPositionX() const;
		F32 getPositionY() const;
		F32 getPositionZ() const;
		glm::vec3 getPosition() const;

		void setPosition(F32 x, F32 y, F32 z);
		void setPosition(glm::vec3 pos);

		void lookAt(glm::vec3 pos);

		glm::mat4 worldToLocal() const;
		glm::mat4 localToWorld() const;

		static inline F32 radToDeg(const F32 rad);
		static inline F32 degToRad(const F32 deg);

		Transform *parent{ nullptr };

	private:
		F32 vlim{ degToRad(89) };
		F32 rotv{ 0 }, roth{ 0 };
		glm::vec3 position;
	};

	//
	// Inline function definitions
	//
	
	void Transform::rotateDeg(F32 deg, glm::vec3 axis) {
		rotate(CameraTransform::degToRad(deg), axis);
	}

	void Transform::rotateXDeg(F32 deg) {
		rotateX(CameraTransform::degToRad(deg));
	}

	void Transform::rotateYDeg(F32 deg) {
		rotateY(CameraTransform::degToRad(deg));
	}

	void Transform::rotateZDeg(F32 deg) {
		rotateZ(CameraTransform::degToRad(deg));
	}

	void CameraTransform::setLimitsDeg(F32 deg) {
		setLimits(degToRad(deg));
	}

	F32 CameraTransform::getLimitsDeg() const {
		return radToDeg(getLimits());
	}

	void CameraTransform::rotateVerticalDeg(F32 deg) {
		rotateVertical(degToRad(deg));
	}

	F32 CameraTransform::getRotationVerticalDeg() const {
		return radToDeg(getRotationVertical());
	}

	void CameraTransform::setRotationVerticalDeg(F32 deg) {
		setRotationVertical(degToRad(deg));
	}

	void CameraTransform::rotateHorizontalDeg(F32 deg) {
		rotateHorizontal(degToRad(deg));
	}

	F32 CameraTransform::getRotationHorizontalDeg() const {
		return radToDeg(getRotationHorizontal());
	}

	void CameraTransform::setRotationHorizontalDeg(F32 deg) {
		setRotationHorizontal(degToRad(deg));
	}

	F32 CameraTransform::radToDeg(const F32 rad) {
		return rad * (180.f / 3.1415926f);
	}

	F32 CameraTransform::degToRad(const F32 deg) {
		return deg * (3.1415926f / 180.f);
	}
}