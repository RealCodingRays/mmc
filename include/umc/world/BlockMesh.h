#pragma once

#include "umc\world\Block.h"

#include <glm\glm.hpp>

namespace umc {

	class AAQuad {
	public:
		inline AAQuad();
		inline AAQuad(ivec3 dir, glm::vec3 min, glm::vec2 uvmin, glm::vec3 max, glm::vec2 uvmax);
		inline AAQuad(BlockFacing dir, glm::vec3 min, glm::vec2 uvmin, glm::vec3 max, glm::vec2 uvmax);

		inline void setOffset(F32 offset);
		inline float getOffset();

		inline void setSize(glm::vec3 min, glm::vec3 max);
		inline glm::vec3 getMin();
		inline glm::vec3 getMax();

		inline void setUVs(glm::vec2 min, glm::vec2 max);
		inline glm::vec2 getMinUV();
		inline glm::vec2 getMaxUV();

		inline void setNormal(ivec3 norm);
		inline void setNormal(BlockFacing dir);
		inline glm::vec3 getNormal();

		inline bool collide(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec2 &uv);

	private:
		glm::vec3 normal;
		glm::vec3 min, max;
		glm::vec2 uvmin, uvmax;
		F32 offset;
	};

	class BlockMesh {
	public:
		BlockMesh();
		BlockMesh(BlockMesh &other);

		UI32 insertQuad(glm::vec3 p1, glm::vec3 p2);
		UI32 insertQuad(ivec3 normal, glm::vec3 p1, glm::vec3 p2);
		UI32 insertQuad(BlockFacing normal, glm::vec3 p1, glm::vec3 p2);

		AAQuad &getQuad(UI32 index);

	private:

	};

	//
	// Inline function definitions
	//

	AAQuad::AAQuad() {
	}

	AAQuad::AAQuad(ivec3 dir, glm::vec3 min, glm::vec2 uvmin, glm::vec3 max, glm::vec2 uvmax) : normal{ dir }, min{ min }, max{ max }, uvmin{ uvmin }, uvmax{ uvmax } {
		assert(normal.x + normal.y + normal.z == 1);
		assert(normal * min == normal * max);

		glm::vec3 noff = normal * min;
		offset = noff.x + noff.y + noff.z;

		// Remove offset component
		min -= min * normal;
		max += max * normal;

		// Add margin of error
		min -= F32_MOE * normal;
		max += F32_MOE * normal;
	}

	AAQuad::AAQuad(BlockFacing dir, glm::vec3 min, glm::vec2 uvmin, glm::vec3 max, glm::vec2 uvmax) : normal{ dir.front() }, min{ min }, max{ max }, uvmin{ uvmin }, uvmax{ uvmax } {
		assert(normal.x + normal.y + normal.z == 1);
		assert(normal * min == normal * max);

		glm::vec3 noff = normal * min;
		offset = noff.x + noff.y + noff.z;

		// Remove offset component
		min -= min * normal;
		max += max * normal;

		// Add margin of error
		min -= F32_MOE * normal;
		max += F32_MOE * normal;
	}

	void AAQuad::setOffset(F32 noffset) {
		offset = noffset;
	}

	float AAQuad::getOffset() {
		return offset;
	}

	void AAQuad::setSize(glm::vec3 min, glm::vec3 max) {
		// Remove offset component
		min -= min * normal;
		max += max * normal;

		// Add margin of error
		min -= F32_MOE * normal;
		max += F32_MOE * normal;
	}

	glm::vec3 AAQuad::getMin() {
		return min;
	}

	glm::vec3 AAQuad::getMax() {
		return max;
	}

	void AAQuad::setUVs(glm::vec2 min, glm::vec2 max) {
		uvmin = min;
		uvmax = max;
	}

	glm::vec2 AAQuad::getMinUV() {
		return uvmin;
	}

	glm::vec2 AAQuad::getMaxUV() {
		return uvmax;
	}

	void AAQuad::setNormal(ivec3 nnorm) {
		assert(nnorm.x + nnorm.y + nnorm.z == 1);
		normal = glm::vec3{ nnorm };
	}

	void AAQuad::setNormal(BlockFacing dir) {
		setNormal(dir.front());
	}

	glm::vec3 AAQuad::getNormal() {
		return normal;
	}

	bool AAQuad::collide(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec2 &uv) {
		F32 l = (offset - origin.x - origin.y - origin.z) / (dir.x + dir.y + dir.z);
		glm::vec3 pos = origin + l * dir;
		if (glm::all(glm::greaterThanEqual(pos, min)) && glm::all(glm::lessThanEqual(pos, max))) {
			hit = pos;
			return true;
		}
		return false;
	}
}