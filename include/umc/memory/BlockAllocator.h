#pragma once

#include "umc\Platform.h"

namespace umc {

	template<class I, class T, int A = 16>
	class IndexedBlockAllocator {
	public:
		I allocate();
		void free(I index);

		T* get(I index);

	private:
		union memtype {
			I next;
			T data;
		};

		memtype *data;
		I size;
	};
}