#pragma once

#include "umc\renderer\native\OpenGL.h"
#include "umc\renderer\GLObjects.h"

namespace umc {

	class GBuffer {
	public:
		GBuffer();
		GBuffer(UI32 width, UI32 height);

		~GBuffer();

		void init(UI32 width, UI32 height);

		void bind();
		void unbind();

		void clear();

		void configureTextureTargets(gl::GLenum pos, gl::GLenum nor, gl::GLenum alb, gl::GLenum mat);
		void bindTextures();
		void unbindTextures();

		inline GLFramebuffer &get();

	private:
		GLRenderbuffer texdep;
		GLTexture texpos;
		GLTexture texnor;
		GLTexture texalb;
		GLTexture texmat;

		GLFramebuffer fbuff;

		UI32 width, height;

		gl::GLenum trgpos{ gl::GL_TEXTURE0 };
		gl::GLenum trgnor{ gl::GL_TEXTURE1 };
		gl::GLenum trgalb{ gl::GL_TEXTURE2 };
		gl::GLenum trgmat{ gl::GL_TEXTURE3 };
	};

	//
	// Inline function definitions
	//

	GLFramebuffer &GBuffer::get() {
		return fbuff;
	}
}