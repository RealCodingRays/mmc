#pragma once

#include "umc\Platform.h"

#include "umc\renderer\Renderer.h"
#include "umc\renderer\GLObjects.h"

#include <glm\glm.hpp>

namespace umc {

	class ParticleSystem : public Renderer {
	public:
		ParticleSystem();
		~ParticleSystem();

		void init(size_t count);

		void setPosition(glm::vec3 min, glm::vec3 max);
		void setVelocity(glm::vec3);
		void setLifetime(F32 t);

		void update(F32 dt);

		virtual void draw(const PipelineInfo &info);

	private:
		struct Particle {
			glm::vec3 pos;
			glm::vec3 vel;
			F32 age{ -1.f };
		};

		Particle *particles;
		GLBuffer buffer;
		size_t particlecnt;

		glm::vec3 pmin, pmax;
		glm::vec3 vel;
		F32 maxage;
	};
}