#pragma once

#include "umc\Platform.h"

#include "umc\renderer\native\OpenGL.h"
#include "umc\renderer\GLObjects.h"

#include <glm\vec3.hpp>
#include <vector>

namespace umc {

	class DebugRenderer {
	public:
		void draw(const glm::mat4 &wtoc, const glm::mat4 &ctos);

		void drawPoint(glm::vec3 pos);

		static void init();
		static void deinit();

	private:
		static GLBuffer vertecies;
		static GLVertexArray vao;
		static GLProgram program;
		std::vector<glm::vec3> points;
	};
}