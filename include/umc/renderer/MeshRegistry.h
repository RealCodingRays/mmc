#pragma once

#include "umc\Platform.h"

#include "umc\renderer\Mesh.h"
#include "umc\utils\Resource.h"

#include <glbinding\gl\values.h>
#include <map>
#include <vector>

namespace umc {

	class MeshRegistry {
	public:
		MeshRegistry();
		~MeshRegistry();

		Mesh &getMesh(const ResourceID &id);

		Mesh &cloneMesh(const ResourceID &dest, const ResourceID &src);

		gl::GLuint getBuffer(const ResourceID &id);
		void updateBuffer(const ResourceID &id);

		void linkVertexArray(const ResourceID &id, gl::GLuint vao, gl::GLint position, gl::GLint normal, gl::GLint tangent, gl::GLint uv);
		void unlinkVertexArray(const ResourceID &id, gl::GLuint vao);

		void init();
		void deinit();

	private:
		struct vaodata {
			gl::GLuint vao{ 0 };
			gl::GLint pos;
			gl::GLint nor;
			gl::GLint tan;
			gl::GLint uv;
		};

		struct meshdata {
			Mesh mesh;
			gl::GLuint buffer{ 0 };
			std::vector<vaodata> vaos;
		};

		std::map<ResourceID, meshdata> meshes;
	};
}