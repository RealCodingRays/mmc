#pragma once

#include "glm\matrix.hpp"
#include "umc\Platform.h"
#include "umc\renderer\GLObjects.h"
#include "umc\renderer\GBuffer.h"
#include "umc\renderer\Renderer.h"
#include "umc\world\Transform.h"

namespace umc {

	class Camera {
	public:
		Camera();
		~Camera();

		void configureProjectionPerspective(F32 fov, F32 znear, F32 zfar, F32 aspect);
		void configureProjectionOrtho(F32 znear, F32 zfar, F32 aspect);
		void configureProjection(glm::mat4 matrix);

		CameraTransform &getTransform();
		const glm::mat4 &getProjectionMatrix() const;

	private:
		CameraTransform transform;
		glm::mat4 projection;
	};

	//
	// Inline function definitions
	//

	inline CameraTransform &Camera::getTransform() {
		return transform;
	}

	inline const glm::mat4 &Camera::getProjectionMatrix() const {
		return projection;
	}
}