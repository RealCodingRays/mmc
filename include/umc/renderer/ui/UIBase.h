#pragma once

#include "umc\renderer\ui\UI.h"

namespace umc {

	class UIBaseComponent : public UIComponent {
	public:
		virtual void setPosition(glm::vec2 pos);
		virtual glm::vec2 getPosition() const;

		virtual void setSize(glm::vec2 size);
		virtual glm::vec2 getSize() const;

		virtual bool isInside(glm::vec2 pos) const;

	protected:
		glm::vec2 position;
		glm::vec2 size;
	};
}