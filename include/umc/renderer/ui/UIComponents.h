#pragma once

#include "umc\renderer\ui\UIBase.h"
#include "umc\renderer\ui\FontRenderer.h"

namespace umc {

	class UILabel : public UIBaseComponent {

	};

	class UIButton : public UIBaseComponent {
	public:
		class ButtonHandler {
		public:
			virtual void onPress(UIButton *button) = 0;
		};

		UIButton();
		UIButton(const char *text);

		void setHandler(ButtonHandler *handler);

		void setText(const char *text);
		void setFont(const glm::vec4 &color, F32 size);
		void setFontHover(const glm::vec4 &color, F32 size);
		void setFontPress(const glm::vec4 &color, F32 size);

		void setBackground(const glm::vec4 &color);
		void setBackgroundHover(const glm::vec4 &color);
		void setBackgroundPress(const glm::vec4 &color);

		void setBorder(const glm::vec4 &color, int width);
		void setBorderHover(const glm::vec4 &color, int width);
		void setBorderPress(const glm::vec4 &color, int width);

	private:
		FontRenderer renderer;
	};
}