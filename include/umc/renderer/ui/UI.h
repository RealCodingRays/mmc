#pragma once

#include <glm\vec2.hpp>
#include "umc\Platform.h"

#include <vector>

namespace umc {
	 
	struct UIPipelineStatus {
	public:
		// Frabuffer size
		UI32 fb_width, fb_height;
	};

	class UIComponent {
	public:
		virtual bool isInside(glm::vec2 pos) const = 0;

		virtual void onMouseClick(int button);
		virtual void onMouseScroll(F32 scroll);

		virtual void render(const UIPipelineStatus &status) = 0;
	};

	class UILayer {
	public:
		UILayer();

		UILayer(const UILayer &other);
		UILayer(UILayer &&other);

		void registerComponent(UIComponent *renderer);

		void pushToTop(UIComponent *renderer);
		void pushToBottom(UIComponent *renderer);
		void pushAbove(UIComponent *renderer, UIComponent *other);
		void pushBelow(UIComponent *renderer, UIComponent *other);

		void render(const UIPipelineStatus &status);

		UIComponent *findComponent(glm::ivec2 pos);
		UIComponent *findComponent(glm::vec2 pos);

		void setVisible(bool enabled);
		bool isVisible();

		UILayer &operator =(const UILayer &other);
		UILayer &operator =(UILayer &&other);

	private:
		std::vector<UIComponent*> comps;

		bool enabled;
	};
};