#pragma once

#include "umc\Platform.h"

#include <map>

#include "umc\renderer\GLObjects.h"
#include "umc\io\Resource.h"
#include "umc\io\resource\ImageResource.h"

namespace umc {

	class Font {
	public:
		struct CharDims {
		public:
			UI32 chr;
			UI32 x, y;
			UI32 width, height;
			I32 xoffset, yoffset;
			UI32 advance;
			UI8 chnl;
		};

		Font();
		Font(ResourceReference<ImageResource> &texture, UI32 size, UI32 baseoff);

		~Font();

		void setTexture(ResourceReference<ImageResource> &texture, UI32 size, UI32 baseoff);
		void addChar(UI32 chr, UI32 x, UI32 y, UI32 width, UI32 height, I32 xoff, I32 yoff, UI32 advance, UI8 chnl);

		const CharDims *getChar(UI32 chr) const;
		UI32 getBaseSize() const;
		F32 getBaseOffset() const;
		ImageResource *getTexture();

	private:
		std::map<UI32, CharDims> characters;
		ResourceReference<ImageResource> texture;
		UI32 size;
		F32 baseoffset;
	};

	class FontRenderer {
	public:
		FontRenderer();
		FontRenderer(Font &font);

		~FontRenderer();

		void setFont(Font &font);

		void setText(const char *text);
		void setPosition(F32 x, F32 y);
		void setSize(F32 size);
		void setColor(F32 r, F32 g, F32 b, F32 a);

		void render(UI32 width, UI32 height);

		static void init();
		static void deinit();

	private:
		Font *font;

		size_t tlen{ 0 };

		F32 x{ 0 }, y{ 0 };
		F32 size{ 0.2f };
		F32 r{ 1 }, g{ 1 }, b{ 1 }, a{ 1 };

		GLTexture texture;
		GLTexture dims;
		GLTexture uvs;

		static GLBuffer quad;
		static GLVertexArray vao;

		static int p_base_size;
		static int p_base_pos;
		static int p_uv_scale;
		static int p_frag_color;
		static int p_font;
		static int p_uvs;
		static int p_dim;
	};
}