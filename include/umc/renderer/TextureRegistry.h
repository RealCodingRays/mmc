#pragma once

#include "umc\Platform.h"

#include "umc\io\resource\ImageResource.h"
#include "umc\utils\Resource.h"

#include <glbinding\gl\types.h>
#include <map>

namespace umc {

	struct TextureGroup {
		ResourceID albedo;
		ResourceID normal;
		ResourceID roughness;
		ResourceID ao;
		ResourceID metallic;
	};

	class TextureRegistry {
	public:
		TextureRegistry();
		~TextureRegistry();

		void init();
		void deinit();

		void registerTexture(ResourceID name, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels = 0);
		void registerTexture(ResourceID name, void *data, gl::GLenum dform, gl::GLenum dtype, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels = 0);
		void registerTexture(ResourceID name, const ImageResource &img, gl::GLsizei levels = 0);
		void registerTexture(ResourceID name, const ImageResource &img, gl::GLenum format, gl::GLsizei levels = 0);
		void registerTexture(ResourceID name, gl::GLuint texid, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels);

		void removeTexture(ResourceID name, bool del = true);

		void getTextureSize(ResourceID name, gl::GLsizei &width, gl::GLsizei &height);
		gl::GLsizei getTextureLevels(ResourceID name);
		gl::GLuint getGLTexture(ResourceID name);

		void bindTexture(ResourceID name, gl::GLint target);

		TextureGroup *registerTextureGroup(ResourceID name);
		TextureGroup *getTextureGroup(ResourceID name);

		void bindTextureGroup(ResourceID name, gl::GLint alloc = 0, gl::GLint noloc = 1, gl::GLint roloc = 2, gl::GLint aoloc = 3, gl::GLint meloc = 4);

	private:
		gl::GLuint errortex;
		gl::GLuint nulltex;

		struct texinfo {
			gl::GLuint texid;
			gl::GLsizei width, height;
			gl::GLsizei levels;
			gl::GLenum format;
		};

		std::map<ResourceID, texinfo> textures;
		std::map<ResourceID, TextureGroup> groups;

		static gl::GLenum bitDepthToType(int depth);
		static gl::GLenum channelsToType(int chnls);
		static gl::GLenum infoToType(int depth, int chnls);
	};
}