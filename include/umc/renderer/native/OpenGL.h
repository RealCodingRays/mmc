#pragma once

#include "GLFW.h"

#pragma warning(push, 0)
#include <glbinding\Binding.h>
#pragma warning(pop)
#include <glbinding\gl\gl.h>