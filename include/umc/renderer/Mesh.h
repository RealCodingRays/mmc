#pragma once

#include "umc\Platform.h"

#include <glm\vec2.hpp>
#include <glm\vec3.hpp>

#include "umc\utils\AABB.h"

namespace umc {

	struct Vertex {
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec3 tangent;
		glm::vec2 uv;

		static glm::vec3 generateNormal(const Vertex &v1, const Vertex &v2, const Vertex &v3);
		static glm::vec3 generateTangent(const Vertex &v1, const Vertex &v2, const Vertex &v3);
	};

	class Mesh {
	public:
		Mesh();
		Mesh(size_t facecnt);
		~Mesh();

		Mesh(const Mesh &other);
		Mesh(Mesh &&other);

		Mesh &operator =(const Mesh &other);
		Mesh &operator =(Mesh &&other);

		/* 
		 * Adds a triangle at the end of the mesh
		 */
		void appendFace(const Vertex &v1, const Vertex &v2, const Vertex &v3);

		/*
		 * Adds a triangle at the end of the mesh and calculates the tangent vector
		 */
		void appendFaceCT(const Vertex &v1, const Vertex &v2, const Vertex &v3);

		/*
		* Adds a triangle at the end of the mesh and calculates the normal vector
		*/
		void appendFaceCN(const Vertex &v1, const Vertex &v2, const Vertex &v3);

		/* 
		 * Adds a triangle at the end of the mesh and calculates the normal and tangent vector
		 */
		void appendFaceCNT(const Vertex &v1, const Vertex &v2, const Vertex &v3);

		Vertex &getVertex(size_t face, int vertex);
		Vertex &getVertex(size_t vertex);

		Vertex *getVertecies();
		size_t getNumVertecies();

		void generateNormals();
		void generateTangents();

		AABB generateAABB();

	private:
		void resize(size_t nsize);

		Vertex *vertecies;
		size_t size{ 0 };
		size_t mem{ 0 };
	};
}