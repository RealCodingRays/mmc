#pragma once

#include <glbinding\gl\types.h>
#include <glm\matrix.hpp>

#include "umc\io\resource\ImageResource.h"

namespace umc {

	class GLBuffer {
	public:
		GLBuffer();
		GLBuffer(gl::GLsizei size, gl::GLenum target);
		GLBuffer(gl::GLsizei size, gl::GLenum target, gl::BufferStorageMask hints);
		GLBuffer(gl::GLsizei size, gl::GLenum target, const void *data);
		GLBuffer(gl::GLsizei size, gl::GLenum target, const void *data, gl::BufferStorageMask hints);
		template<class T>
		inline GLBuffer(gl::GLsizei size, gl::GLenum target, const T *data);
		template<class T>
		inline GLBuffer(gl::GLsizei size, gl::GLenum target, const T *data, gl::BufferStorageMask hints);

		GLBuffer(const GLBuffer &other);
		GLBuffer(GLBuffer &&other);

		~GLBuffer();

		GLBuffer &operator =(const GLBuffer &other);
		GLBuffer &operator =(GLBuffer &&other);

		void init(gl::GLsizei size, gl::GLenum target);
		void init(gl::GLsizei size, gl::GLenum target, gl::BufferStorageMask hints);
		void init(gl::GLsizei size, gl::GLenum target, const void *data);
		void init(gl::GLsizei size, gl::GLenum target, const void *data, gl::BufferStorageMask hints);
		template<class T>
		inline void init(gl::GLsizei size, gl::GLenum target, const T *data);
		template<class T>
		inline void init(gl::GLsizei size, gl::GLenum target, const T *data, gl::BufferStorageMask hints);

		inline bool isReady() const;

		inline gl::GLsizei getSizeRaw() const;
		template<class T>
		inline gl::GLsizei getSize() const;

		inline gl::GLuint getID() const;

		void bind();
		void bind(gl::GLenum target);
		void unbind();
		static void unbind(gl::GLenum target);

		void bindIndexed(gl::GLenum target, gl::GLuint index);
		void bindIndexed(gl::GLenum target, gl::GLuint index, gl::GLintptr offset, gl::GLsizei length);
		template<class T>
		inline void bindIndexed(gl::GLenum target, gl::GLuint index, gl::GLintptr offset, gl::GLsizei length);
		static void unbindIndexed(gl::GLenum target, gl::GLuint index);

		void *map();
		void *map(gl::GLintptr offset, gl::GLsizei length);
		template<class T>
		inline T *map();
		template<class T>
		inline T *map(gl::GLintptr offset, gl::GLsizei length);

		void unmap();

		void setDataRaw(gl::GLintptr offset, gl::GLsizei length, const void *data);
		template<class T>
		inline void setData(gl::GLintptr offset, gl::GLsizei length, const T *data);

		void getDataRaw(gl::GLintptr offset, gl::GLsizei length, void *target) const;
		template<class T>
		inline void getData(gl::GLintptr offset, gl::GLsizei length, T *target) const;

		void resizeRaw(gl::GLsizei size, bool copy = true);
		template<class T>
		inline void resize(gl::GLsizei size, bool copy = true);

		void copyRaw(gl::GLintptr offset, gl::GLsizei length, gl::GLuint src, gl::GLintptr srcoffset);
		inline void copyRaw(gl::GLintptr offset, gl::GLsizei length, const GLBuffer &src, gl::GLintptr srcoffset);
		template<class T>
		inline void copy(gl::GLintptr offset, gl::GLsizei length, gl::GLuint src, gl::GLintptr srcoffset);
		template<class T>
		inline void copy(gl::GLintptr offset, gl::GLsizei length, const GLBuffer &src, gl::GLintptr srcoffset);

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id{ 0 };
		gl::GLsizei bsize;
		gl::GLenum trgt;
		gl::BufferStorageMask flags;
	};

	class GLTexture {
	public:
		GLTexture();
		GLTexture(gl::GLsizei width, gl::GLenum format, gl::GLint levels, gl::GLenum type);
		GLTexture(gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLint levels, gl::GLenum type);
		GLTexture(gl::GLsizei width, gl::GLsizei height, gl::GLsizei depth, gl::GLenum format, gl::GLint levels, gl::GLenum type);

		GLTexture(const GLTexture &other) = delete;
		GLTexture(GLTexture &&other);

		~GLTexture();

		void init(ImageResource *res, gl::GLint levels);
		void initCube(ImageResource *fr, ImageResource *ri, ImageResource *ba, ImageResource *le, ImageResource *to, ImageResource *bo);
		void init1D(gl::GLsizei width, gl::GLenum format, gl::GLint levels);
		void init2D(gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLint levels);
		void init3D(gl::GLsizei width, gl::GLsizei height, gl::GLsizei depth, gl::GLenum format, gl::GLint levels);

		GLTexture &operator =(const GLTexture &other) = delete;
		GLTexture &operator =(GLTexture &&other);

		void bind();
		void bind(gl::GLenum target);

		void unbind();
		static void unbind(gl::GLenum target);

		void setData1D(gl::GLint level, gl::GLint x, gl::GLsizei width, gl::GLenum type, gl::GLenum format, const void *data);
		void setData2D(gl::GLint level, gl::GLint x, gl::GLint y, gl::GLsizei width, gl::GLsizei height, gl::GLenum type, gl::GLenum format, const void *data);
		void setData2D(gl::GLint level, gl::GLint x, gl::GLint y, gl::GLint z, gl::GLsizei width, gl::GLsizei height, gl::GLsizei depth, gl::GLenum type, gl::GLenum format, const void *data);

		void getData(gl::GLint level, gl::GLenum format, gl::GLenum type, void *data);

		void setParameter(gl::GLenum name, gl::GLfloat value);
		void setParameter(gl::GLenum name, gl::GLint value);
		void setParameter(gl::GLenum name, gl::GLenum value);
		void setParameter(gl::GLenum name, gl::GLfloat *value);
		void setParameter(gl::GLenum name, gl::GLint *value);
		void setParameterI(gl::GLenum name, gl::GLint *value);
		void setParameterI(gl::GLenum name, gl::GLuint *value);

		void generateMipmaps();

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id{ 0 };
		gl::GLsizei width, height, depth;
		gl::GLenum type;
	};

	class GLRenderbuffer {
	public:
		GLRenderbuffer();
		GLRenderbuffer(gl::GLenum format, gl::GLsizei width, gl::GLsizei height);
		GLRenderbuffer(gl::GLsizei samples, gl::GLenum format,  gl::GLsizei width, gl::GLsizei height);

		GLRenderbuffer(const GLRenderbuffer &other) = delete;
		GLRenderbuffer(GLRenderbuffer &&other);

		~GLRenderbuffer();

		GLRenderbuffer &operator =(const GLRenderbuffer &other) = delete;
		GLRenderbuffer &operator =(GLRenderbuffer &&other);

		void init(gl::GLenum format, gl::GLsizei width, gl::GLsizei height);
		void init(gl::GLsizei, gl::GLenum format, gl::GLsizei width, gl::GLsizei height);

		inline bool isReady() const;

		inline gl::GLsizei getWidth() const;
		inline gl::GLsizei getHeight() const;

		inline gl::GLenum getFormat() const;

		void bind();
		static void unbind();

		inline operator gl::GLuint() const;

		void resize(gl::GLenum format, gl::GLsizei width, gl::GLsizei height);
		void resize(gl::GLsizei, gl::GLenum format, gl::GLsizei width, gl::GLsizei height);

	private:
		gl::GLuint id{ 0 };
		gl::GLsizei width, height;
		gl::GLenum format;
	};

	class GLFramebuffer {
	public:
		GLFramebuffer();

		GLFramebuffer(const GLFramebuffer &other) = delete;
		GLFramebuffer(GLFramebuffer &&other);

		~GLFramebuffer();

		GLFramebuffer &operator =(const GLFramebuffer &other) = delete;
		GLFramebuffer &operator =(GLFramebuffer &&other);

		void init();

		void attachRenderbuffer(gl::GLenum target, gl::GLuint renderbuffer);
		void attachTexture(gl::GLenum target, gl::GLuint texture, gl::GLint level);
		inline void attach(gl::GLenum target, const GLRenderbuffer &renderbuffer);
		inline void attach(gl::GLenum target, const GLTexture &texture, gl::GLint level);

		bool ready();

		void bind(gl::GLenum target);
		static void unbind(gl::GLenum target);

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id;
	};

	class GLShader {
	public:
		GLShader();
		GLShader(const gl::GLchar* data, gl::GLsizei size, gl::GLenum type);

		GLShader(const GLShader &other) = delete;
		GLShader(GLShader &&other);

		~GLShader();

		GLShader &operator =(const GLShader &other) = delete;
		GLShader &operator =(GLShader &&other);

		void init(const gl::GLchar* data, gl::GLsizei size, gl::GLenum type);

		inline bool isReady() const;

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id{ 0 };
	};

	class GLProgram {
	public:
		GLProgram();

		GLProgram(const GLProgram &other) = delete;
		GLProgram(GLProgram &&other);
		
		~GLProgram();

		GLProgram &operator =(const GLProgram &other) = delete;
		GLProgram &operator =(GLProgram &&other);

		void init();

		void attach(gl::GLuint shader);
		inline void attach(const GLShader &shader);

		bool link();

		bool isReady() const;

		void use();
		static void unuse();

		void setAttribLocation(const char *name, gl::GLint location);
		gl::GLint getAttribLocation(const char *name) const;

		gl::GLint getUniformLocation(const char *name) const;

		void setUniformValue(gl::GLint location, gl::GLfloat v0);
		void setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1);
		void setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2);
		void setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2, gl::GLfloat v3);

		void setUniformValue(gl::GLint location, gl::GLint v0);
		void setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1);
		void setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1, gl::GLint v2);
		void setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1, gl::GLint v2, gl::GLint v3);

		void setUniformValue(gl::GLint location, gl::GLuint v0);
		void setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1);
		void setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2);
		void setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2, gl::GLuint v3);

		void setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data);

		void setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLint *data);

		void setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLuint *data);

		void setUniformMatrix2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix2x3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3x2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix2x4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4x2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3x4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4x3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		inline void setUniformMatrix2(gl::GLint location, gl::GLboolean transpose, const glm::mat2 &data);
		inline void setUniformMatrix3(gl::GLint location, gl::GLboolean transpose, const glm::mat3 &data);
		inline void setUniformMatrix4(gl::GLint location, gl::GLboolean transpose, const glm::mat4 &data);
		inline void setUniformMatrix2x3(gl::GLint location, gl::GLboolean transpose, const glm::mat2x3 &data);
		inline void setUniformMatrix3x2(gl::GLint location, gl::GLboolean transpose, const glm::mat3x2 &data);
		inline void setUniformMatrix2x4(gl::GLint location, gl::GLboolean transpose, const glm::mat2x4 &data);
		inline void setUniformMatrix4x2(gl::GLint location, gl::GLboolean transpose, const glm::mat4x2 &data);
		inline void setUniformMatrix3x4(gl::GLint location, gl::GLboolean transpose, const glm::mat3x4 &data);
		inline void setUniformMatrix4x3(gl::GLint location, gl::GLboolean transpose, const glm::mat4x3 &data);

		void setUniformValue(const gl::GLchar *name, gl::GLfloat v0);
		void setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1);
		void setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2);
		void setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2, gl::GLfloat v3);

		void setUniformValue(const gl::GLchar *name, gl::GLint v0);
		void setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1);
		void setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1, gl::GLint v2);
		void setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1, gl::GLint v2, gl::GLint v3);

		void setUniformValue(const gl::GLchar *name, gl::GLuint v0);
		void setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1);
		void setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2);
		void setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2, gl::GLuint v3);

		void setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data);
		void setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data);

		void setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data);
		void setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data);

		void setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data);
		void setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data);

		void setUniformMatrix2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix2x3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3x2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix2x4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4x2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix3x4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		void setUniformMatrix4x3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data);
		inline void setUniformMatrix2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2 &data);
		inline void setUniformMatrix3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3 &data);
		inline void setUniformMatrix4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4 &data);
		inline void setUniformMatrix2x3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2x3 &data);
		inline void setUniformMatrix3x2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3x2 &data);
		inline void setUniformMatrix2x4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2x4 &data);
		inline void setUniformMatrix4x2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4x2 &data);
		inline void setUniformMatrix3x4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3x4 &data);
		inline void setUniformMatrix4x3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4x3 &data);

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id{ 0 };
	};

	class GLVertexArray {
	public:
		GLVertexArray();

		GLVertexArray(const GLVertexArray &other) = delete;
		GLVertexArray(GLVertexArray &&other);

		~GLVertexArray();

		GLVertexArray &operator =(const GLVertexArray &other) = delete;
		GLVertexArray &operator =(GLVertexArray &&other);

		void init();

		void setupFPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLboolean normalized,	gl::GLsizei stride, const void *offset);
		void setupIPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLsizei stride, const void *offset);
		void setupLPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLsizei stride, const void *offset);

		void enableLocation(gl::GLuint index);
		void disableLocation(gl::GLuint index);

		void bind();
		static void unbind();

		inline operator gl::GLuint() const;

	private:
		gl::GLuint id{ 0 };
	};

	// 
	//------------------- Inline function definitions --------------------------
	//

	//
	// GLBuffer
	//
	template<class T> 
	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target, const T *data) : GLBuffer(size * sizeof(T), target, static_cast<void *>(data)) {}

	template<class T>
	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target, const T *data, gl::BufferStorageMask hints) : GLBuffer(size * sizeof(T), target, static_cast<void *>(data), hints) {}

	template<class T>
	void GLBuffer::init(gl::GLsizei size, gl::GLenum target, const T *data) {
		init(size * sizeof(T), target, static_cast<const void *>(data));
	}

	template<class T>
	void GLBuffer::init(gl::GLsizei size, gl::GLenum target, const T *data, gl::BufferStorageMask hints) {
		init(size * sizeof(T), target, static_cast<const void *>(data), hints);
	}

	bool GLBuffer::isReady() const {
		return id != 0;
	}

	gl::GLsizei GLBuffer::getSizeRaw() const {
		return bsize;
	}

	template<class T>
	gl::GLsizei GLBuffer::getSize() const {
		return getSize() / sizeof(T);
	}

	gl::GLuint GLBuffer::getID() const {
		return id;
	}

	template<class T>
	void GLBuffer::bindIndexed(gl::GLenum target, gl::GLuint index, gl::GLintptr offset, gl::GLsizei length) {
		bindIndexed(target, index, offset * sizeof(T), length * sizeof(T));
	}

	template<class T>
	T *GLBuffer::map() {
		return reinterpret_cast<T*>(map());
	}

	template<class T>
	T *GLBuffer::map(gl::GLintptr offset, gl::GLsizei length) {
		return reinterpret_cast<T*>(map(offset * sizeof(T), length * sizeof(T)));
	}

	template<class T>
	void GLBuffer::setData(gl::GLintptr offset, gl::GLsizei length, const T *data) {
		setDataRaw(offset * sizeof(T), length * sizeof(T), static_cast<const void*>(data));
	}

	template<class T>
	void GLBuffer::getData(gl::GLintptr offset, gl::GLsizei length, T *target) const {
		getDataRaw(offset * sizeof(T), length * sizeof(T), static_cast<void*>(target));
	}

	template<class T>
	void GLBuffer::resize(gl::GLsizei size, bool copy) {
		resizeRaw(size * sizeof(T), copy);
	}

	void GLBuffer::copyRaw(gl::GLintptr offset, gl::GLsizei length, const GLBuffer &src, gl::GLintptr srcoffset) {
		copyRaw(offset, length, src.id, srcoffset);
	}

	template<class T>
	void GLBuffer::copy(gl::GLintptr offset, gl::GLsizei length, gl::GLuint src, gl::GLintptr srcoffset) {
		copy(offset * sizeof(T), length * sizeof(T), src, srcoffset * sizeof(T));
	}

	template<class T>
	void GLBuffer::copy(gl::GLintptr offset, gl::GLsizei length, const GLBuffer &src, gl::GLintptr srcoffset) {
		copy(offset * sizeof(T), length * sizeof(T), src.id, srcoffset * sizeof(T));
	}

	GLBuffer::operator gl::GLuint() const {
		return id;
	}

	//
	// GLTexture
	//
	GLTexture::operator gl::GLuint() const {
		return id;
	}

	//
	// GLRenderbuffer
	//
	bool GLRenderbuffer::isReady() const {
		return id != 0;
	}

	gl::GLsizei GLRenderbuffer::getWidth() const {
		return width;
	}

	gl::GLsizei GLRenderbuffer::getHeight() const {
		return height;
	}

	gl::GLenum GLRenderbuffer::getFormat() const {
		return format;
	}

	GLRenderbuffer::operator gl::GLuint() const {
		return id;
	}

	//
	// GLFramebuffer
	//
	void GLFramebuffer::attach(gl::GLenum target, const GLRenderbuffer &renderbuffer) {
		attachRenderbuffer(target, static_cast<gl::GLuint>(renderbuffer));
	}

	void GLFramebuffer::attach(gl::GLenum target, const GLTexture &texture, gl::GLint level) {
		attachTexture(target, static_cast<gl::GLuint>(texture), level);
	}

	GLFramebuffer::operator gl::GLuint() const {
		return id;
	}

	//
	// GLShader
	//
	bool GLShader::isReady() const {
		return id != 0;
	}

	GLShader::operator gl::GLuint() const {
		return id;
	}

	//
	// GLProgram
	//
	void GLProgram::attach(const GLShader &shader) {
		attach(static_cast<gl::GLuint>(shader));
	}

	void GLProgram::setUniformMatrix2(gl::GLint location, gl::GLboolean transpose, const glm::mat2 &data) {
		setUniformMatrix2(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3(gl::GLint location, gl::GLboolean transpose, const glm::mat3 &data) {
		setUniformMatrix3(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4(gl::GLint location, gl::GLboolean transpose, const glm::mat4 &data) {
		setUniformMatrix4(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix2x3(gl::GLint location, gl::GLboolean transpose, const glm::mat2x3 &data) {
		setUniformMatrix2x3(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3x2(gl::GLint location, gl::GLboolean transpose, const glm::mat3x2 &data) {
		setUniformMatrix3x2(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix2x4(gl::GLint location, gl::GLboolean transpose, const glm::mat2x4 &data) {
		setUniformMatrix2x4(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4x2(gl::GLint location, gl::GLboolean transpose, const glm::mat4x2 &data) {
		setUniformMatrix4x2(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3x4(gl::GLint location, gl::GLboolean transpose, const glm::mat3x4 &data) {
		setUniformMatrix3x4(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4x3(gl::GLint location, gl::GLboolean transpose, const glm::mat4x3 &data) {
		setUniformMatrix4x3(location, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2 &data) {
		setUniformMatrix2(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3 &data) {
		setUniformMatrix3(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4 &data) {
		setUniformMatrix4(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix2x3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2x3 &data) {
		setUniformMatrix2x3(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3x2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3x2 &data) {
		setUniformMatrix3x2(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix2x4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat2x4 &data) {
		setUniformMatrix2x4(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4x2(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4x2 &data) {
		setUniformMatrix4x2(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix3x4(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat3x4 &data) {
		setUniformMatrix3x4(name, 1, transpose, &(data[0][0]));
	}

	void GLProgram::setUniformMatrix4x3(const gl::GLchar *name, gl::GLboolean transpose, const glm::mat4x3 &data) {
		setUniformMatrix4x3(name, 1, transpose, &(data[0][0]));
	}

	GLProgram::operator gl::GLuint() const {
		return id;
	}

	//
	// GLVertexArray
	//

	GLVertexArray::operator gl::GLuint() const {
		return id;
	}
}