#pragma once

#include "umc\Platform.h"

#include "umc\renderer\native\GLFW.h"

namespace umc {

	class Window {
	public:
		bool init();
		void deinit();

		inline GLFWwindow *get();
		inline operator GLFWwindow *();

		void setWindowSize(UI32 width, UI32 height);

		void getFramebufferSize(UI32 &width, UI32 &height);
		UI32 getFramebufferWidth();
		UI32 getFramebufferHeight();

		void setTitle(const char *title);

		void show();
		void hide();
		void setVisible(bool visible);

		void pollEvents();

		bool shouldExit();

		static void framebufferResizeCallback(GLFWwindow *window, int width, int height);
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouseCallback(GLFWwindow *window, int key, int action, int mod);
		static void cursorCallback(GLFWwindow *window, double x, double y);

	private:
		Window();
		~Window();

		GLFWwindow *window;

		friend class Engine;
	};

	//
	// Inline function definitions
	//

	GLFWwindow *Window::get() {
		return window;
	}

	Window::operator GLFWwindow *() {
		return window;
	}
}