#pragma once

#include "umc\renderer\Renderer.h"
#include "umc\renderer\GLObjects.h"

#include "umc\world\Transform.h"

#include "umc\io\Resource.h"

namespace umc {

	class DebugCube : public Renderer {
	public:
		virtual void draw(const PipelineInfo &info);

		static void init();
		static void deint();

		Transform &getTransform();

		inline GLTexture &getAlbedo();
		inline GLTexture &getNormal();
		inline GLTexture &getRoughness();

	private:
		Transform trans;

		GLTexture albedo;
		GLTexture normal;
		GLTexture roughn;
		GLTexture displa;
		GLTexture occlus;

		static GLBuffer vertecies;
		static GLVertexArray vao;
	};

	inline GLTexture &DebugCube::getAlbedo() {
		return albedo;
	}

	inline GLTexture &DebugCube::getNormal() {
		return normal;
	}

	inline GLTexture &DebugCube::getRoughness() {
		return roughn;
	}
}