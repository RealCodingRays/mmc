#pragma once

#include "umc\Platform.h"

#include <glm\mat4x4.hpp>

namespace umc {

	struct PipelineInfo {
	public:
		// Framebuffer width and height
		UI32 fb_width, fb_height;

		// word to camera matrix
		glm::mat4 world_to_camera;

		// projection matrix
		glm::mat4 camera_to_screen;

		// world to camera matrix transformed for normals
		glm::mat4 world_to_camera_norm;

		// projection matrix transformed for normals
		glm::mat4 camera_to_screen_norm;
	};

	class Renderer {
	public:
		virtual void draw(const PipelineInfo &info) = 0;
		//virtual void drawShadow(const PipelineInfo &info) = 0;
	};
}