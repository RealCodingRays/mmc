#pragma once

#include "umc\renderer\GLObjects.h"

namespace umc {

	class DefaultPrograms {
	public:
		inline GLProgram &textureViewer();
		inline GLProgram &fontRenderer();
		inline GLProgram &defaultRenderer();

		void init();
		void deinit();

		inline static DefaultPrograms &get();

	private:
		static DefaultPrograms instance;

		GLProgram texview;
		GLProgram fontren;
		GLProgram defrend;
	};

	//
	// Inline function definitions
	//

	DefaultPrograms &DefaultPrograms::get() {
		return instance;
	}

	GLProgram &DefaultPrograms::textureViewer() {
		return texview;
	}

	GLProgram &DefaultPrograms::fontRenderer() {
		return fontren;
	}

	GLProgram &DefaultPrograms::defaultRenderer() {
		return defrend;
	}

}