#pragma once

#include "umc\Platform.h"

#include "umc\utils\Properties.h"
#include "umc\renderer\Renderer.h"
#include "umc\renderer\Camera.h"
#include "umc\renderer\DebugRenderer.h"

#include "umc\renderer\GLObjects.h"

#include <vector>

namespace umc {

	class RenderManager {
	public:
		void init();
		void deinit();

		void registerRenderer(Renderer *renderer);

		void setSunDir(glm::vec3 dir);

		inline DebugRenderer &getDebug();

		Camera &getDefaultCamera();
		UI32 getScreenWidth();
		UI32 getScreenHeight();
		UI32 getRenderWidth();
		UI32 getRenderHeight();
		F32 getRenderScale();
		void setRenderScale(F32 scale);

		void renderScene(F32 dt);

		void onFramebufferResize(UI32 width, UI32 height);

		static void debugMsgCallback(gl::GLenum source, gl::GLenum type, gl::GLuint id, gl::GLenum severity, gl::GLsizei length, const gl::GLchar *msg, const void *param);

	private:
		RenderManager();
		~RenderManager();

		void resizeScreenBuffer(UI32 width, UI32 height);
		void resizeRenderBuffer(UI32 width, UI32 height);
		void resizeShadowBuffer(UI32 size);

		UI32 fb_width, fb_height, re_width, re_height;
		F32 render_scale;

		Camera defCam;
		DebugRenderer debug;

		std::vector<Renderer *> renderers;

		glm::vec3 sunDir;

		GLBuffer quadverts;
		GLBuffer skybverts;
		GLProgram prgcombine;
		GLProgram prgbloextr;
		GLProgram prgblurga1;
		GLProgram prgblurga2;
		GLProgram prgblocomb;
		GLProgram prgcolrmap;
		GLProgram prgrescale;
		GLProgram prgluminace;
		GLProgram prgskybox;
		GLVertexArray vaoquad;
		GLVertexArray vaoskyb;

		GLFramebuffer buffg;
		GLFramebuffer buffrend;
		GLFramebuffer buffblur0;
		GLFramebuffer buffblur1;
		GLFramebuffer buffblur2;
		GLFramebuffer buffblur3;
		GLFramebuffer buffshad;

		GLRenderbuffer rengdepth;
		GLTexture texgpos;
		GLTexture texgnor;
		GLTexture texgalb;
		GLTexture texgmat;
		GLTexture texcolor0;
		GLTexture texcolor1;
		GLTexture texblur0;
		GLTexture texblur1;
		GLTexture texshadow;
		GLTexture texluminance;

		UI32 shadowres;

		F32 lumfacr, lumfacg, lumfacb;
		F32 lumfacbloextr, lumfacexposure;

		friend class Engine;
	};

	//
	// Inline function definitions
	//

	DebugRenderer &RenderManager::getDebug() {
		return debug;
	}
}