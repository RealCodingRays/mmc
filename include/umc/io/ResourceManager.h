#pragma once

#include "umc\Platform.h"
#include <boost\filesystem.hpp>

#include <map>
#include <typeinfo>

#include "umc\io\Resource.h"
#include "umc\io\ResourceLoader.h"
#include "umc\utils\Resource.h"
#include "umc\utils\ResourcePath.h"

namespace umc {

	class ResourceManager {
	public:
		static ResourceManager &get();

		bool registerPackage(const char *name, const boost::filesystem::path &path);
		const boost::filesystem::path getPackage(const char *name);

		LoaderID registerLoader(ResourceLoader *loader);
		LoaderID getLoader(const char *name);

		template<class T>
		inline ResourceReference<T> loadResource(const ResourcePath &name);
		template<class T>
		inline ResourceReference<T> loadResource(const boost::filesystem::path &path);

		template<class T>
		inline ResourceReference<T> loadResourceExplicit(const ResourcePath &name, LoaderID id);
		template<class T>
		inline ResourceReference<T> loadResourceExplicit(const boost::filesystem::path &path, LoaderID id);

		template<class T>
		inline ResourceReference<T> loadResourceExplicit(const ResourcePath &name, const char *lname);
		template<class T>
		inline ResourceReference<T> loadResourceExplicit(const boost::filesystem::path &path, const char *lname);

		void doGC();

	private:
		static ResourceManager mgr;

		ResourceManager();
		~ResourceManager();

		struct ResourceData {
			ResourceInstance *instance;
			boost::filesystem::path path;
			int refcounter;
		};

		ResourceLoader *findLoader(const boost::filesystem::path &path, const type_info &info);

		ResourceData *getResource(const boost::filesystem::path &path, const type_info &info);
		ResourceData *getResourceExplicit(const boost::filesystem::path &path, const type_info &info, LoaderID id);

		std::map<UI64, ResourceData> resources;
		std::map<std::string, boost::filesystem::path> packages;
		LoaderID nextID;
		std::map<std::string, LoaderID> loaderNames;
		std::map<LoaderID, ResourceLoader*> loaders;
	};

	// ---------------------
	// Inline function definitions
	// ---------------------

	template<class T>
	inline ResourceReference<T> ResourceManager::loadResource(const ResourcePath &name) {
		return loadResource<T>(name.toAbsolutePath());
	}

	template<class T>
	inline ResourceReference<T> ResourceManager::loadResource(const boost::filesystem::path &path) {
		ResourceData *res = getResource(path, typeid(T));
		if (res) {
			return ResourceReference<T>{ static_cast<T*>(res->instance->getReference()), &(res->refcounter) };
		}
		else {
			return ResourceReference<T>{};
		}
	}

	template<class T>
	ResourceReference<T> ResourceManager::loadResourceExplicit(const ResourcePath &name, LoaderID id) {
		return loadResourceExplicit<T>(name.toAbsolutePath(), id);
	}
	template<class T>
	ResourceReference<T> ResourceManager::loadResourceExplicit(const boost::filesystem::path &path, LoaderID id) {
		ResourceData *res = getResourceExplicit(path, typeid(T), id);
		if (res) {
			return ResourceReference<T>{ static_cast<T*>(res->instance->getReference()), &(res->refcounter) };
		}
		else {
			return ResourceReference<T>{};
		}
	}

	template<class T>
	ResourceReference<T> ResourceManager::loadResourceExplicit(const ResourcePath &name, const char *lname) {
		return loadResourceExplicit<T>(name.toAbsolutePath(), lname);
	}
	template<class T>
	ResourceReference<T> ResourceManager::loadResourceExplicit(const boost::filesystem::path &path, const char *lname) {
		LoaderID id = getLoader(lname);
		if (id == 0) {
			std::cout << "[ERROR][ResourceManager.h]: Unable to find specified resource loader!" << std::endl;
			return ResourceReference<T>{};
		}
		ResourceData *res = getResourceExplicit(path, typeid(T), id);
		if (res) {
			return ResourceReference<T>{ static_cast<T*>(res->instance->getReference()), &(res->refcounter) };
		}
		else {
			return ResourceReference<T>{};
		}
	}
}