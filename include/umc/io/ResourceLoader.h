#pragma once

#include "umc\Platform.h"

#include <boost\filesystem.hpp>
#include <typeinfo>

namespace umc {

	typedef UI64 LoaderID;

	class ResourceInstance {
	public:
		virtual ~ResourceInstance();

		virtual void *getReference() = 0;
		virtual const type_info &getResourceType() const = 0;

		virtual void close() = 0;
	};

	class ResourceLoader {
	public:
		virtual ~ResourceLoader();

		virtual ResourceInstance *load(const boost::filesystem::path &path) = 0;

		virtual bool canLoad(const boost::filesystem::path &path) = 0;
		virtual int loadPriority(const boost::filesystem::path &path) = 0;

		virtual const char *getName() const = 0;
		virtual const type_info &getResourceType() const = 0;
	};
}