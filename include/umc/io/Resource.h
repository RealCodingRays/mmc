#pragma once

namespace umc {

	template<class T>
	class ResourceReference {
	public:
		inline ResourceReference();
		inline ResourceReference(T* resource, int *refcounter);

		inline ResourceReference(const ResourceReference<T> &other);
		inline ResourceReference(ResourceReference<T> &&other);

		inline ~ResourceReference();

		inline ResourceReference<T> &operator =(const ResourceReference<T> &other);
		inline ResourceReference<T> &operator =(ResourceReference<T> &&other);

		inline T* get();
		inline T* operator ->();

		inline bool ready();
		inline operator bool();

	private:
		T *res{ nullptr };
		int *refcnt{ nullptr };
	};

	//
	// Inline function definitions
	//

	template<class T>
	ResourceReference<T>::ResourceReference() : res{ nullptr }, refcnt{ nullptr } {}

	template<class T>
	ResourceReference<T>::ResourceReference(T* resource, int *refcounter) : res{ resource }, refcnt{ refcounter } {
		if (resource != nullptr && refcounter != nullptr) {
			(*refcounter)++;
		}
		else {
			res = nullptr;
			refcnt = nullptr;
		}
	}

	template<class T>
	ResourceReference<T>::ResourceReference(const ResourceReference &other) : res{ other.res }, refcnt{ other.refcnt } {
		if (res != nullptr && refcnt != nullptr) {
			(*refcnt)++;
		}
		else {
			res = nullptr;
			refcnt = nullptr;
		}
	}

	template<class T>
	ResourceReference<T>::ResourceReference(ResourceReference &&other) : res{ other.res }, refcnt{ other.refcnt } {
		other.res = nullptr;
		other.refcnt = nullptr;

		if (res == nullptr || refcnt == nullptr) {
			res = nullptr;
			refcnt = nullptr;
		}
	}

	template<class T>
	ResourceReference<T>::~ResourceReference() {
		if (refcnt) {
			(*refcnt)--;
		}

		res = nullptr;
		refcnt = nullptr;
	}

	template<class T>
	ResourceReference<T> &ResourceReference<T>::operator =(const ResourceReference<T> &other) {
		if (refcnt) {
			(*refcnt)--;
		}

		res = other.res;
		refcnt = other.refcnt;

		if (res != nullptr && refcnt != nullptr) {
			(*refcnt)++;
		}
		else {
			res = nullptr;
			refcnt = nullptr;
		}

		return *this;
	}

	template<class T>
	ResourceReference<T> &ResourceReference<T>::operator =(ResourceReference<T> &&other) {
		if (refcnt) {
			(*refcnt)--;
		}

		res = other.res;
		refcnt = other.refcnt;
		other.res = nullptr;
		other.refcnt = nullptr;

		if (res == nullptr || refcnt == nullptr) {
			res = nullptr;
			refcnt = nullptr;
		}

		return *this;
	}

	template<class T>
	T* ResourceReference<T>::get() {
		return res;
	}

	template<class T>
	T* ResourceReference<T>::operator ->() {
		return res;
	}

	template<class T>
	bool ResourceReference<T>::ready() {
		return res != nullptr;
	}

	template<class T>
	ResourceReference<T>::operator bool() {
		return res != nullptr;
	}
}