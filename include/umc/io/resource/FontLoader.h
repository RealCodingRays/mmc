#pragma once

#include "umc\io\ResourceLoader.h"
#include "umc\io\resource\ImageResource.h"
#include "umc\renderer\ui\FontRenderer.h"

namespace umc {

	class FontResourceInstance : public ResourceInstance {
	public:
		FontResourceInstance();

		virtual ~FontResourceInstance();

		virtual void *getReference();
		virtual const type_info &getResourceType() const;

		virtual void close();

		Font &getFont();

	private:
		Font font;
	};

	class FontResourceLoader : public ResourceLoader {
	public:
		FontResourceLoader();

		virtual ~FontResourceLoader();

		virtual ResourceInstance *load(const boost::filesystem::path &path);

		virtual bool canLoad(const boost::filesystem::path &path);
		virtual int loadPriority(const boost::filesystem::path &path);

		virtual const char *getName() const;
		virtual const type_info &getResourceType() const;

		static const LoaderID id;
	};
}