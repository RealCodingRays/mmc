#pragma once

#include "umc\io\resource\TextResource.h"
#include "umc\io\ResourceLoader.h"

namespace umc {

	class TextResourceInstance : public ResourceInstance {
	public:
		TextResourceInstance(const char *text, size_t size);

		virtual ~TextResourceInstance();

		virtual void *getReference();
		virtual const type_info &getResourceType() const;

		virtual void close();

	private:
		TextResource resource;
	};

	class TextResourceLoader : public ResourceLoader {
	public:
		TextResourceLoader();

		virtual ~TextResourceLoader();

		virtual ResourceInstance *load(const boost::filesystem::path &path);

		virtual bool canLoad(const boost::filesystem::path &path);
		virtual int loadPriority(const boost::filesystem::path &path);

		virtual const char *getName() const;
		virtual const type_info &getResourceType() const;

		static const LoaderID id{ 1 };
	};
}