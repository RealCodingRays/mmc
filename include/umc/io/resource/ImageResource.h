#pragma once

#include "umc\Platform.h"

namespace umc {

	class ImageResource {
	public:
		ImageResource(void *data, size_t width, size_t height, int chncnt, int depth);
		~ImageResource();

		ImageResource(const ImageResource &other) = delete;
		ImageResource(ImageResource &&other);

		UI8 &operator()(size_t x, size_t y, int chn);

		void *get();
		const void *get() const;

		size_t getWidth() const;
		size_t getHeight() const;

		int getNumChannels() const;
		int getBitDepth() const;

	private:
		void *data;
		size_t width, height;
		int chncnt;
		int depth;
	};
}