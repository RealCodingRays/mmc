#pragma once

#include "umc\io\ResourceLoader.h"

#include "umc\utils\Properties.h"

namespace umc {

	class PropertiesResourceInstance : public ResourceInstance {
	public:
		PropertiesResourceInstance(const boost::filesystem::path &path);

		virtual ~PropertiesResourceInstance();

		virtual void *getReference();
		virtual const type_info &getResourceType() const;

		virtual void close();

	private:
		Properties properties;
	};

	class PropertiesResourceLoader : public ResourceLoader {
	public:
		PropertiesResourceLoader();

		virtual ~PropertiesResourceLoader();

		virtual ResourceInstance *load(const boost::filesystem::path &path);

		virtual bool canLoad(const boost::filesystem::path &path);
		virtual int loadPriority(const boost::filesystem::path &path);

		virtual const char *getName() const;
		virtual const type_info &getResourceType() const;

		static const LoaderID id{ 2 };
	};
}