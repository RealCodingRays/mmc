#pragma once

namespace umc {

	class TextResource {
	public:
		TextResource(const char *text, size_t size);

		~TextResource();

		inline const char *get();
		inline const char *operator ->();

		inline size_t size();

	private:
		const char *data;
		size_t siz;
	};

	//
	// Inline function definitions
	//

	const char *TextResource::get() {
		return data;
	}

	const char *TextResource::operator ->() {
		return data;
	}

	size_t TextResource::size() {
		return siz;
	}
}