#pragma once

#include "umc\io\ResourceLoader.h"
#include "umc\io\resource\ImageResource.h"

namespace umc {

	class ImageResourceInstance : public ResourceInstance {
	public:
		ImageResourceInstance(void *data, size_t width, size_t height, int chncnt, int depth);
		virtual ~ImageResourceInstance();

		virtual void *getReference();
		virtual const type_info &getResourceType() const;

		virtual void close();

	private:
		ImageResource res;
	};

	class ImageResourceLoader : public ResourceLoader {
	public:
		ImageResourceLoader();
		virtual ~ImageResourceLoader();

		virtual ResourceInstance *load(const boost::filesystem::path &path);

		virtual bool canLoad(const boost::filesystem::path &path);
		virtual int loadPriority(const boost::filesystem::path &path);

		virtual const char *getName() const;
		virtual const type_info &getResourceType() const;

		static const LoaderID id;
	private:
		static bool libpnginit;
	};
}