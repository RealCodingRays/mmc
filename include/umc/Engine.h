#pragma once

#include "umc\Platform.h"

#include "umc\utils\Properties.h"
#include "umc\renderer\Window.h"
#include "umc\renderer\RenderManager.h"
#include "umc\renderer\TextureRegistry.h"
#include "umc\renderer\MeshRegistry.h"
#include "umc\input\InputManager.h"
#include "umc\world\BlockRegistry.h"

namespace umc {

	class Engine {
	public:
		static Properties startupConfig;

		static Window window;
		static RenderManager renderManager;
		static InputManager inputManager;

		static TextureRegistry textureRegistry;
		static MeshRegistry meshRegistry;
		static BlockRegistry blockResistry;

		static void init();
		static void deinit();
	};
}