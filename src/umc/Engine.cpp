#include "umc\Engine.h"

#include <iostream>

namespace umc {
	Properties Engine::startupConfig;

	Window Engine::window;

	RenderManager Engine::renderManager;

	InputManager Engine::inputManager;

	TextureRegistry Engine::textureRegistry;

	MeshRegistry Engine::meshRegistry;

	BlockRegistry Engine::blockResistry;

	void Engine::init() {
		if (!Engine::window.init()) {
			std::cout << "Failed to initialize Window!" << std::endl;
			return;
		}
		Engine::renderManager.init();
		Engine::textureRegistry.init();
		Engine::meshRegistry.init();
	}

	void Engine::deinit() {
		Engine::meshRegistry.deinit();
		Engine::textureRegistry.deinit();
		Engine::renderManager.deinit();
		Engine::window.deinit();
	}
}