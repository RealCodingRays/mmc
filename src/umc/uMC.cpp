#include "umc\Platform.h"
#include "umc\Engine.h"

#include <iostream>
#include <boost\filesystem.hpp>

#include "umc\io\ResourceManager.h"
#include "umc\io\resource\ImageResource.h"
#include "umc\renderer\native\GLFW.h"
#include "umc\utils\ResourcePath.h"

#include "umc\world\World.h"

#include <glm\gtx\string_cast.hpp>

UI32 fb;
UI32 rl;
UI32 tb;

class testinputhandler : public umc::InputHandler {
public:
	virtual void onKeyDown(int key) {

	}

	virtual void onKeyUp(int key) {
		if (key == GLFW_KEY_ESCAPE) {
			umc::Engine::inputManager.disableLayer(umc::InputLayer::GAMEPLAY);
			umc::Engine::inputManager.enableLayer(umc::InputLayer::MENU);
			umc::Engine::inputManager.unlockCursor();
		}
		if (key == GLFW_MOUSE_BUTTON_1) {
			umc::Engine::inputManager.disableLayer(umc::InputLayer::MENU);
			umc::Engine::inputManager.enableLayer(umc::InputLayer::GAMEPLAY);
			umc::Engine::inputManager.lockCursor();
		}
	}
};

class controller : public umc::InputHandler, public umc::LogicComponent {
public:
	controller(umc::World &world) : world{ world } {
	}

	virtual ~controller() {
	}

	virtual void onKeyDown(int key) {

	}

	virtual void onKeyUp(int key) {
		if (key == GLFW_MOUSE_BUTTON_1 || key == GLFW_MOUSE_BUTTON_2) {
			glm::vec3 fwd = umc::Engine::renderManager.getDefaultCamera().getTransform().getForward();
			glm::vec3 ori = umc::Engine::renderManager.getDefaultCamera().getTransform().getPosition();

			umc::RaycastHit hit;
			hit.origin = umc::Engine::renderManager.getDefaultCamera().getTransform().getPosition();
			hit.direction = umc::Engine::renderManager.getDefaultCamera().getTransform().getForward();
			if (world.getCollsion().raycast(hit)) {
				if (hit.type == umc::RaycastHit::HitType::BLOCK) {
					if (key == GLFW_MOUSE_BUTTON_1) {
						umc::ivec3 dir{ round(hit.normal.x), round(hit.normal.y), round(hit.normal.z) };
						world.setBlock(hit.blockpos + dir, umc::BlockFacing{}, 3);
					}
					else {
						world.setBlock(hit.blockpos, umc::BlockFacing{}, 0);
					}
				}
			}
		}
	}

	virtual void attach(umc::WorldObject &obj) {
		umc::Engine::renderManager.getDefaultCamera().getTransform().parent = &(obj.transform);
	}

	virtual void detach(umc::WorldObject &obj) {
		umc::Engine::renderManager.getDefaultCamera().getTransform().parent = nullptr;
	}

	virtual void update(F32 dt, umc::WorldObject &obj) {
		glm::vec3 dir;
		dir.z += umc::Engine::inputManager.getAxis(fb) * dt * 6;
		dir.x += umc::Engine::inputManager.getAxis(rl) * dt * 6;
		dir.y += umc::Engine::inputManager.getAxis(tb) * dt * 6;

		//obj.transform.translate(glm::vec3{ glm::vec4{dir, 0} *umc::Engine::renderManager.getDefaultCamera().getTransform().worldToLocal() });

		F32 dx = umc::Engine::inputManager.getCursordX(umc::InputLayer::GAMEPLAY);
		F32 dy = umc::Engine::inputManager.getCursordY(umc::InputLayer::GAMEPLAY);

		umc::Engine::renderManager.getDefaultCamera().getTransform().translateForward(dir);
		umc::Engine::renderManager.getDefaultCamera().getTransform().rotateVertical(-dy);
		umc::Engine::renderManager.getDefaultCamera().getTransform().rotateHorizontal(dx);
	}

private:
	umc::World &world;
};

umc::World world;

float rand32() {
	return static_cast<float>(std::rand()) / RAND_MAX;
}

void main() {
	umc::Engine::init();

	testinputhandler hdlr;

	umc::Engine::inputManager.registerHandler(&hdlr, GLFW_KEY_ESCAPE, umc::InputLayer::GAMEPLAY);
	umc::Engine::inputManager.registerHandler(&hdlr, GLFW_MOUSE_BUTTON_1, umc::InputLayer::MENU);

	fb = umc::Engine::inputManager.registerAxis(GLFW_KEY_S, GLFW_KEY_W, umc::InputLayer::GAMEPLAY);
	rl = umc::Engine::inputManager.registerAxis(GLFW_KEY_D, GLFW_KEY_A, umc::InputLayer::GAMEPLAY);
	tb = umc::Engine::inputManager.registerAxis(GLFW_KEY_LEFT_SHIFT, GLFW_KEY_LEFT_CONTROL, umc::InputLayer::GAMEPLAY);

	umc::Engine::inputManager.enableLayer(umc::InputLayer::MENU);

	umc::ResourceName stone{ "umc:Stone" }, brick{ "umc:Brick" }, wood1{ "umc:Wood1" };
	{
		umc::ResourceReference<umc::ImageResource> alb;
		umc::ResourceReference<umc::ImageResource> nor;
		umc::ResourceReference<umc::ImageResource> rou;
		umc::ResourceReference<umc::ImageResource> hei;

		umc::ResourcePath stoneAlb{ "umc:assets/textures/Stone-albedo.png" };
		umc::ResourcePath stoneNor{ "umc:assets/textures/Stone-normal.png" };
		umc::ResourcePath stoneRou{ "umc:assets/textures/Stone-roughness.png" };
		umc::ResourcePath stoneHei{ "umc:assets/textures/Stone-height.png" };

		alb = umc::ResourceManager::get().loadResource<umc::ImageResource>(stoneAlb);
		nor = umc::ResourceManager::get().loadResource<umc::ImageResource>(stoneNor);
		rou = umc::ResourceManager::get().loadResource<umc::ImageResource>(stoneRou);
		hei = umc::ResourceManager::get().loadResource<umc::ImageResource>(stoneHei);

		umc::Engine::textureRegistry.registerTexture(stoneAlb, *alb.get(), gl::GL_SRGB8);
		umc::Engine::textureRegistry.registerTexture(stoneNor, *nor.get());
		umc::Engine::textureRegistry.registerTexture(stoneRou, *rou.get());
		umc::Engine::textureRegistry.registerTexture(stoneHei, *hei.get());
		umc::TextureGroup *group = umc::Engine::textureRegistry.registerTextureGroup(stone);
		group->albedo = stoneAlb;
		group->normal = stoneNor;
		group->roughness = stoneRou;
		group->ao = "umc:zero";
		group->metallic = "umc:zero";

		umc::ResourceName brickAlb{ "umc:assets/textures/Brick-albedo.png" };
		umc::ResourceName brickNor{ "umc:assets/textures/Brick-normal.png" };
		umc::ResourceName brickRou{ "umc:assets/textures/Brick-roughness.png" };
		umc::ResourceName brickHei{ "umc:assets/textures/Brick-height.png" };

		alb = umc::ResourceManager::get().loadResource<umc::ImageResource>(brickAlb);
		nor = umc::ResourceManager::get().loadResource<umc::ImageResource>(brickNor);
		rou = umc::ResourceManager::get().loadResource<umc::ImageResource>(brickRou);
		hei = umc::ResourceManager::get().loadResource<umc::ImageResource>(brickHei);

		umc::Engine::textureRegistry.registerTexture(brickAlb, *alb.get(), gl::GL_SRGB8);
		umc::Engine::textureRegistry.registerTexture(brickNor, *nor.get());
		umc::Engine::textureRegistry.registerTexture(brickRou, *rou.get());
		umc::Engine::textureRegistry.registerTexture(brickHei, *hei.get());
		group = umc::Engine::textureRegistry.registerTextureGroup(brick);
		group->albedo = brickAlb;
		group->normal = brickNor;
		group->roughness = brickRou;
		group->ao = "umc:zero";
		group->metallic = "umc:zero";

		umc::ResourceName wood1Alb{ "umc:assets/textures/Wood1-albedo.png" };
		umc::ResourceName wood1Nor{ "umc:assets/textures/Wood1-normal.png" };
		umc::ResourceName wood1Rou{ "umc:assets/textures/Wood1-roughness.png" };
		umc::ResourceName wood1Hei{ "umc:assets/textures/Wood1-height.png" };

		alb = umc::ResourceManager::get().loadResource<umc::ImageResource>(wood1Alb);
		nor = umc::ResourceManager::get().loadResource<umc::ImageResource>(wood1Nor);
		rou = umc::ResourceManager::get().loadResource<umc::ImageResource>(wood1Rou);
		hei = umc::ResourceManager::get().loadResource<umc::ImageResource>(wood1Hei);

		umc::Engine::textureRegistry.registerTexture(wood1Alb, *alb.get(), gl::GL_SRGB8);
		umc::Engine::textureRegistry.registerTexture(wood1Nor, *nor.get());
		umc::Engine::textureRegistry.registerTexture(wood1Rou, *rou.get());
		umc::Engine::textureRegistry.registerTexture(wood1Hei, *hei.get());
		group = umc::Engine::textureRegistry.registerTextureGroup(wood1);
		group->albedo = wood1Alb;
		group->normal = wood1Nor;
		group->roughness = wood1Rou;
		group->ao = "umc:zero";
		group->metallic = "umc:zero";
	}

	umc::BlocksRenderer::initStatic();

	umc::BlockID id = umc::Engine::blockResistry.registerBlock(stone);
	umc::Engine::blockResistry.setBlockTexture(id, stone);

	id = umc::Engine::blockResistry.registerBlock(brick);
	umc::Engine::blockResistry.setBlockTexture(id, brick);
	
	id = umc::Engine::blockResistry.registerBlock(wood1);
	umc::Engine::blockResistry.setBlockTexture(id, wood1);

	controller placer{ world };
	umc::Engine::inputManager.registerHandler(&placer, GLFW_MOUSE_BUTTON_1, umc::InputLayer::GAMEPLAY);
	umc::Engine::inputManager.registerHandler(&placer, GLFW_MOUSE_BUTTON_2, umc::InputLayer::GAMEPLAY);
	world.publobj.setLogicComponent(&placer);

	const I8 r{ 16 };
	for (I8 x = -r; x < r; x += 2) {
		for (I8 y = -r; y < 0; y += 2) {
			for (I8 z = -r; z < r; z += 2) {
				world.setBlock(umc::ivec3{ x, y, z }, umc::BlockFacing::NORTH, 1);
			}
		}
	}
	umc::Engine::renderManager.registerRenderer(world.getRenderer());

	umc::ResourceManager::get().doGC();

	umc::Engine::window.show();
	/*for (int i = 0; i < 128; i++) {
		glm::vec3 pos{ (rand32() - 0.5f) * 32.f, (rand32() - 1.f) * 16.f, (rand32() - 0.5f) * 32.f };
		glm::vec3 col{ rand32() * 10.f, rand32() * 10.f, rand32() * 10.f };
		umc::Engine::renderManager.getDefaultCamera().createLight(pos, col);
	}*/

	glm::vec3 vel[512];
	for (glm::vec3 &v : vel) {
		v = glm::vec3{ 0 };
	}

	F64 t{ 0 };
	clock_t t0 = clock();
	while (!umc::Engine::window.shouldExit()) {
		clock_t t1 = clock();
		F32 dt = static_cast<F32>(t1 - t0) / static_cast<F32>(CLOCKS_PER_SEC);
		t += dt;
		t0 = t1;

		world.update(dt);

		F32 sx = static_cast<F32>(sin(t * 0.05f));
		F32 sz = static_cast<F32>(cos(t * 0.05f));
		umc::Engine::renderManager.setSunDir(glm::vec3{ sx, -1.f, sz });

		glm::vec3 fwd = umc::Engine::renderManager.getDefaultCamera().getTransform().getForward();
		glm::vec3 ori = umc::Engine::renderManager.getDefaultCamera().getTransform().getPosition();

		umc::RaycastHit hit;
		hit.origin = umc::Engine::renderManager.getDefaultCamera().getTransform().getPosition();
		hit.direction = umc::Engine::renderManager.getDefaultCamera().getTransform().getForward();
		if (world.getCollsion().raycast(hit)) {
			if (hit.type == umc::RaycastHit::HitType::BLOCK) {
				umc::Engine::renderManager.getDebug().drawPoint(hit.hit);
			}
		}

		/*for (int i = 0; i < umc::Camera::lightcnt; i++) {
			glm::vec3 offset{ (rand32() - 0.5f), (rand32() - 0.5f), (rand32() - 0.5f) };
			glm::vec3 &v = vel[i];
			v += offset;

			if (v.x > 1) {
				v.x = 1;
			}
			else if (v.x < -1) {
				v.x = -1;
			}
			if (v.y > 1) {
				v.y = 1;
			}
			else if (v.y < -1) {
				v.y = -1;
			}
			if (v.z > 1) {
				v.z = 1;
			}
			else if (v.z < -1) {
				v.z = -1;
			}

			glm::vec3 npos{ umc::Camera::lcpos[i] + v };

			if (npos.x > 17) {
				npos.x = 17;
			}
			else if (npos.x < -17) {
				npos.x = -17;
			}
			if (npos.y > 1) {
				npos.y = 1;
			}
			else if (npos.y < -17) {
				npos.y = -17;
			}
			if (npos.z > 17) {
				npos.z = 17;
			}
			else if (npos.z < -17) {
				npos.z = -17;
			}

			umc::Camera::lcpos[i] = npos;
		}*/

		umc::Engine::renderManager.renderScene(dt);
		umc::Engine::inputManager.update();
		umc::Engine::window.pollEvents();
	}

	umc::Engine::deinit();
}