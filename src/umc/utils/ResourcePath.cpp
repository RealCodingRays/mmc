#include "umc\utils\ResourcePath.h"

#include "umc\io\ResourceManager.h"
#include "umc\utils\Hash.h"

#include <cstring>
#include <cassert>

namespace umc {

	ResourcePath::ResourcePath() {
	}

	ResourcePath::ResourcePath(const char *name) : ResourceID{ name } {
		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->path.clear();
			return;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		char *path = new char[nlen + 1];
		strncpy_s(path, nlen + 1, pend + 1, nlen);
		path[nlen] = 0;
		this->path = path;
		delete[] path;

		assert(ResourceName::isValidName(this->package));
	}

	ResourcePath::ResourcePath(const char *package, const char *name) : ResourceID{ package, name }, path{ name } {
		size_t len = strlen(package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, package, len);
		(this->package)[len] = 0;

		assert(ResourceName::isValidName(this->package));
	}

	ResourcePath::ResourcePath(const ResourceName &name) : ResourceID{ name }, path{ name.getName() } {
		size_t len = strlen(name.getPackage());
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, name.getPackage(), len);
		(this->package)[len] = 0;
	}

	ResourcePath::~ResourcePath() {
		delete[] this->package;
		this->package = nullptr;
		this->path.clear();
	}

	ResourcePath::ResourcePath(const ResourcePath &other) : ResourceID{ other } {
		if (this->package) {
			delete[] this->package;
		}

		size_t len = strlen(other.package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, other.package, len);
		(this->package)[len] = 0;

		this->path = other.path;
	}

	ResourcePath::ResourcePath(ResourcePath &&other) : ResourceID{ std::move(other) }, path{ std::move(other.path) } {
		if (this->package) {
			delete[] package;
		}

		this->package = other.package;
		other.package = nullptr;
	}

	ResourcePath &ResourcePath::operator =(const char *name) {
		hash = stringHash(name);

		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->path.clear();
			return *this;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		char *path = new char[nlen + 1];
		strncpy_s(path, nlen + 1, pend + 1, nlen);
		path[nlen] = 0;
		this->path = path;
		delete[] path;

		return *this;
	}

	ResourcePath &ResourcePath::operator =(const ResourceName &name) {
		hash = name.getID();

		size_t len = strlen(name.getPackage());
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, name.getPackage(), len);
		(this->package)[len] = 0;

		this->path = name.getName();

		return *this;
	}

	ResourcePath &ResourcePath::operator =(const ResourcePath &other) {
		hash = other.hash;

		if (this->package) {
			delete[] this->package;
		}

		size_t len = strlen(other.package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, other.package, len);
		(this->package)[len] = 0;

		this->path = other.path;

		return *this;
	}

	ResourcePath &ResourcePath::operator =(ResourcePath &&other) {
		hash = other.hash;
		other.hash = 0;

		if (this->package) {
			delete[] this->package;
		}

		this->package = other.package;
		other.package = nullptr;

		this->path = std::move(other.path);

		return *this;
	}

	void ResourcePath::set(const char *name) {
		ResourceID::operator=(name);

		if (this->package) {
			delete[] this->package;
		}

		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->path.clear();
			return;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		char *path = new char[nlen + 1];
		strncpy_s(path, nlen + 1, pend + 1, nlen);
		path[nlen] = 0;
		this->path = path;
		delete[] path;
	}

	void ResourcePath::set(const char *package, const char *name) {
		hash = resourceHash(package, name);

		if (this->package) {
			delete[] this->package;
		}

		size_t len = strlen(package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, package, len);
		(this->package)[len] = 0;

		this->path = name;
	}

	void ResourcePath::setName(const char *name) {
		hash = resourceHash(this->package, name);

		this->path = name;
	}

	const boost::filesystem::path &ResourcePath::getRelativePath() const {
		return path;
	}

	boost::filesystem::path ResourcePath::toAbsolutePath() const {
		boost::filesystem::path ret = ResourceManager::get().getPackage(package);
		ret += path;
		return ret;
	}

	ResourcePath::operator boost::filesystem::path() const {
		boost::filesystem::path ret = ResourceManager::get().getPackage(package);
		ret += path;
		return ret;
	}
}