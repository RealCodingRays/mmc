#include "umc\utils\Properties.h"

#include <iostream>
#include <fstream>
#include <cstring>

#include "umc\io\ResourceManager.h"

namespace umc {

	Properties::Properties() {
	}

	Properties::Properties(const boost::filesystem::path &path) : path{ path } {
		load();
	}

	Properties::~Properties() {
		props.clear();
		for (char *str : strs) {
			delete[] str;
		}
		strs.clear();
	}

	const char *Properties::getoc(const char *name, const char *def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::STRING) {
				std::cout << "[WARNING][Properties.cpp]: requested string value from non string field!" << std::endl;
				return nullptr;
			}
			return res->second.data.str;
		}
		else {
			size_t length = strlen(def) + 1;
			char *data = new char[length];
			strncpy_s(data, length, def, length);
			data[length - 1] = 0;
			strs.push_back(data);

			valueptr &val = props[std::string{ name }];
			val.type = valuetype::STRING;
			val.data.str = data;
			return data;
		}
	}

	bool Properties::getocBool(const char *name, const bool def) {
		auto res = props.find(std::string{ name });
		
		if (res != props.end()) {
			if (res->second.type != valuetype::BOOLEAN) {
				std::cout << "[WARNING][Properties.cpp]: requested boolean value from non boolean field!" << std::endl;
			}
			return res->second.data.b;
		}
		else {
			valueptr &val = props[std::string{ name }];
			val.type = valuetype::BOOLEAN;
			val.data.b = def;
			return def;
		}
	}

	I64 Properties::getocInt(const char *name, const I64 def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::INTEGER) {
				std::cout << "[WARINING][Properties.cpp]: requested signed int from non signed int field!" << std::endl;
			}
			return res->second.data.i64;
		}
		else {
			valueptr &val = props[std::string{ name }];
			val.type = valuetype::INTEGER;
			val.data.i64 = def;
			return def;
		}
	}

	F64 Properties::getocFloat(const char *name, const F64 def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::FLOAT) {
				std::cout << "[WARINING][Properties.cpp]: requested double from non double field!" << std::endl;
			}
			return res->second.data.f64;
		}
		else {
			valueptr &val = props[std::string{ name }];
			val.type = valuetype::FLOAT;
			val.data.f64 = def;
			return def;
		}
	}

	const char *Properties::get(const char *name) const {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::STRING) {
				std::cout << "[WARNING][Properties.cpp]: requested string value from non string field!" << std::endl;
				return nullptr;
			}
			return res->second.data.str;
		}
		return nullptr;
	}

	bool Properties::getBool(const char *name) const {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::BOOLEAN) {
				std::cout << "[WARNING][Properties.cpp]: requested boolean value from non boolean field!" << std::endl;
			}
			return res->second.data.b;
		}
		return false;
	}

	I64 Properties::getInt(const char *name) const {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::INTEGER) {
				std::cout << "[WARNING][Properties.cpp]: requested signed int value from non signed int field!" << std::endl;
			}
			return res->second.data.i64;
		}
		return 0;
	}

	F64 Properties::getFloat(const char *name) const {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type != valuetype::FLOAT) {
				std::cout << "[WARNING][Properties.cpp]: requested double value from non double field!" << std::endl;
			}
			return res->second.data.f64;
		}
		return 0;
	}

	void Properties::set(const char *name, const char *def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type == valuetype::STRING) {
				auto it = strs.begin();
				while (it != strs.end()) {
					if ((*it) == res->second.data.str) {
						strs.erase(it);
						break;
					}
				}
				delete[] res->second.data.str;
			}
		}

		size_t length = strlen(def) + 1;
		char *data = new char[length];
		strncpy_s(data, length, def, length);
		data[length - 1] = 0;
		strs.push_back(data);

		valueptr &val = props[std::string{ name }];
		val.type = valuetype::STRING;
		val.data.str = data;
	}

	void Properties::setBool(const char *name, const bool def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type == valuetype::STRING) {
				auto it = strs.begin();
				while (it != strs.end()) {
					if ((*it) == res->second.data.str) {
						strs.erase(it);
						break;
					}
				}
				delete[] res->second.data.str;
			}
		}

		valueptr &val = props[std::string{ name }];
		val.type = valuetype::BOOLEAN;
		val.data.b = def;
	}

	void Properties::setInt(const char *name, const I64 def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type == valuetype::STRING) {
				auto it = strs.begin();
				while (it != strs.end()) {
					if ((*it) == res->second.data.str) {
						strs.erase(it);
						break;
					}
				}
				delete[] res->second.data.str;
			}
		}

		valueptr &val = props[std::string{ name }];
		val.type = valuetype::INTEGER;
		val.data.i64 = def;
	}

	void Properties::setFloat(const char *name, const F64 def) {
		auto res = props.find(std::string{ name });

		if (res != props.end()) {
			if (res->second.type == valuetype::STRING) {
				auto it = strs.begin();
				while (it != strs.end()) {
					if ((*it) == res->second.data.str) {
						strs.erase(it);
						break;
					}
				}
				delete[] res->second.data.str;
			}
		}

		valueptr &val = props[std::string{ name }];
		val.type = valuetype::FLOAT;
		val.data.f64 = def;
	}

	void Properties::setFile(const boost::filesystem::path &path) {
		this->path = path;
	}

	void Properties::setFile(const ResourcePath &res) {
		this->path = res.toAbsolutePath();
	}

	void Properties::load(const boost::filesystem::path &file) {
		props.clear();
		for (char *str : strs) {
			delete[] str;
		}
		strs.clear();

		std::ifstream in{ file.c_str() };

		if (!in.is_open()) {
			return;
		}

		for (std::string line; !in.eof(); std::getline(in, line)) {
			size_t beg = line.find_first_not_of(" ");
			if (beg == std::string::npos) {
				continue;
			}

			size_t end = line.find_last_not_of(" ");
			line = line.substr(beg, end - beg + 1);

			if (line.size() == 0) {
				continue;
			}
			if (line[0] == '#') {
				continue;
			}

			size_t equ = line.find_first_of("=");
			if (equ == std::string::npos) {
				throw std::runtime_error("[ERROR][Properties.cpp]: Ivalid syntax while reading file!");
			}

			std::string name = line.substr(0, equ + 1);
			std::string valu = line.substr(equ + 1, line.size() - equ - 1);

			end = name.find_last_not_of(" ");
			name = name.substr(0, end);

			beg = valu.find_first_not_of(" ");
			valu = valu.substr(beg, line.size() - beg);

			if (name.find(" ") != std::string::npos) {
				throw std::runtime_error("[ERROR][Properties.cpp]: Ivalid syntax while reading file, no spaces in property names allowed!");
			}
			if (valu.size() == 0) {
				continue;
			}
			
			if (valu[0] == '\"' && valu[valu.size() - 1] == '\"') {
				size_t len = valu.size() - 1;
				char *data = new char[len];
				if (len > 1) {
					for (int i = 0; i < len; i++) {
						data[i] = valu[i + 1];
					}
				}
				data[len - 1] = 0;
				
				strs.push_back(data);

				valueptr &val = props[name];
				val.type = valuetype::STRING;
				val.data.str = data;
			} 
			else if (valu == std::string{ "true" } || valu == std::string{ "True" } || valu == std::string{ "TRUE" }) {
				valueptr &val = props[name];
				val.type = valuetype::BOOLEAN;
				val.data.b = true;
			}
			else if (valu == std::string{ "false" } || valu == std::string{ "False" } || valu == std::string{ "FALSE" }) {
				valueptr &val = props[name];
				val.type = valuetype::BOOLEAN;
				val.data.b = false;
			}
			else if (valu.find(".") != std::string::npos) {
				valueptr &val = props[name];
				val.type = valuetype::FLOAT;
				val.data.f64 = std::atof(valu.c_str());
			}
			else {
				valueptr &val = props[name];
				val.type = valuetype::INTEGER;
				val.data.i64 = std::atoi(valu.c_str());
			}
		}

		in.close();
	}

	void Properties::write(const boost::filesystem::path &file) {
		std::ofstream out{ file.c_str(), std::ios::out | std::ios::trunc };

		for (auto it = props.begin(); it != props.end(); it++) {
			out << it->first << "=";
			switch (it->second.type) {
			case valuetype::STRING:
				out << '\"' << it->second.data.str << '\"' << '\n';
				break;
			case valuetype::BOOLEAN:
				out << (it->second.data.b ? "true" : "false") << '\n';
				break;
			case valuetype::INTEGER:
				out << std::to_string(it->second.data.ui64) << '\n';
				break;
			case valuetype::FLOAT:
				out << std::to_string(it->second.data.f64) << '\n';
			}
		}

		out.close();
	}

	Properties::Properties(const ResourcePath &name) : Properties(name.toAbsolutePath()) {}

	void Properties::load() {
		load(path);
	}
	void Properties::load(const ResourcePath &res) {
		load(res.toAbsolutePath());
	}

	void Properties::write() {
		write(path);
	}
	void Properties::write(const ResourcePath &res) {
		write(res.toAbsolutePath());
	}
}