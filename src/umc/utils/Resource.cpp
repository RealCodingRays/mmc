#include "umc\utils\Resource.h"

#include "umc\utils\Hash.h"

#include <utility>
#include <cstring>
#include <cassert>

namespace umc {

	// ResourceID
	ResourceID::ResourceID() {
	}

	ResourceID::ResourceID(UI64 hash) : hash{ hash } {
	}

	ResourceID::ResourceID(const char *name) : hash{ stringHash(name) } {
	}

	ResourceID::ResourceID(const char *package, const char *name) : hash{ resourceHash(package, name) } {
	}

	ResourceID::~ResourceID() {
		hash = 0;
	}

	ResourceID::ResourceID(const ResourceID &other) : hash{ other.hash } {
	}

	ResourceID::ResourceID(ResourceID &&other) : hash{ other.hash } {
		other.hash = 0;
	}

	ResourceID &ResourceID::operator =(UI64 num) {
		hash = num;

		return *this;
	}

	ResourceID &ResourceID::operator =(const char *name) {
		hash = stringHash(name);

		return *this;
	}

	ResourceID &ResourceID::operator =(const ResourceID &other) {
		hash = other.hash;

		return *this;
	}

	ResourceID &ResourceID::operator =(ResourceID &&other) {
		hash = other.hash;
		other.hash = 0;

		return *this;
	}

	// ResourceName
	ResourceName::ResourceName() : ResourceID{} {
	}

	ResourceName::ResourceName(const char *name) : ResourceID{ name } {
		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->name = nullptr;
			return;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		this->name = new char[nlen + 1];
		strncpy_s(this->name, nlen + 1, pend + 1, nlen);
		(this->name)[nlen] = 0;

		assert(isValidName(this->package));
	}

	ResourceName::ResourceName(const char *package, const char *name) : ResourceID{ package, name } {
		size_t len = strlen(package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, package, len);
		(this->package)[len] = 0;

		len = strlen(name);
		this->name = new char[len + 1];
		strncpy_s(this->name, len + 1, name, len);
		(this->name)[len] = 0;

		assert(isValidName(this->package));
	}

	ResourceName::~ResourceName() {
		delete[] package;
		delete[] name;
		package = nullptr;
		name = nullptr;
	}

	ResourceName::ResourceName(const ResourceName &other) : ResourceID{ other } {
		size_t len = strlen(other.package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, other.package, len);
		(this->package)[len] = 0;

		len = strlen(other.name);
		this->name = new char[len + 1];
		strncpy_s(this->name, len + 1, other.name, len);
		(this->name)[len] = 0;

		assert(isValidName(this->package));
	}

	ResourceName::ResourceName(ResourceName &&other) : ResourceID{ std::move(other) } {
		package = other.package;
		name = other.name;

		other.package = nullptr;
		other.name = nullptr;

		assert(isValidName(this->package));
	}

	ResourceName &ResourceName::operator =(const char *name) {
		ResourceID::operator =(name);

		delete[] this->package;
		delete[] this->name;

		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->name = nullptr;
			return *this;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		this->name = new char[nlen + 1];
		strncpy_s(this->name, nlen + 1, pend + 1, nlen);
		(this->name)[nlen] = 0;

		assert(isValidName(this->package));

		return *this;
	}

	ResourceName &ResourceName::operator =(const ResourceName &other) {
		ResourceID::operator =(other);

		delete[] package;
		delete[] name;

		size_t len = strlen(other.package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, other.package, len);
		(this->package)[len] = 0;

		len = strlen(other.name);
		this->name = new char[len + 1];
		strncpy_s(this->name, len + 1, other.name, len);
		(this->name)[len] = 0;

		assert(isValidName(this->package));

		return *this;
	}

	ResourceName &ResourceName::operator =(ResourceName &&other) {
		ResourceID::operator =(std::move(other));

		delete[] package;
		delete[] name;

		package = other.package;
		name = other.name;

		other.package = nullptr;
		other.name = nullptr;

		assert(isValidName(this->package));

		return *this;
	}

	void ResourceName::set(const char *name) {
		ResourceID::hash = stringHash(name);

		delete[] this->package;
		delete[] this->name;

		const char *pend = strchr(name, ':');
		if (!pend) {
			this->hash = 0;
			this->package = nullptr;
			this->name = nullptr;
			return;
		}
		size_t plen = static_cast<size_t>(pend - name);
		this->package = new char[plen + 1];
		strncpy_s(this->package, plen + 1, name, plen);
		(this->package)[plen] = 0;

		size_t nlen = strlen(name) - plen - 1;
		this->name = new char[nlen + 1];
		strncpy_s(this->name, nlen + 1, pend + 1, nlen);
		(this->name)[nlen] = 0;

		assert(isValidName(this->package));
	}

	void ResourceName::set(const char *package, const char *name) {
		ResourceID::hash = resourceHash(package, name);

		delete[] this->package;
		delete[] this->name;

		size_t len = strlen(package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, package, len);
		(this->package)[len] = 0;

		len = strlen(name);
		this->name = new char[len + 1];
		strncpy_s(this->name, len + 1, name, len);
		(this->name)[len] = 0;

		assert(isValidName(this->package));
	}

	void ResourceName::setPackage(const char *package) {
		ResourceID::hash = resourceHash(package, name);

		delete[] package;

		size_t len = strlen(package);
		this->package = new char[len + 1];
		strncpy_s(this->package, len + 1, package, len);
		this->package[len] = 0;

		assert(isValidName(this->package));
	}

	void ResourceName::setName(const char *name) {
		ResourceID::hash = resourceHash(package, name);

		delete[] name;

		size_t len = strlen(name);
		this->name = new char[len + 1];
		strncpy_s(this->name, len + 1, name, len);
		(this->name)[len] = 0;
	}

	void ResourceName::addSuffix(const char *suffix) {
		size_t slen = strlen(suffix);
		size_t nlen = strlen(name);

		char *nname = new char[slen + nlen + 1];

		strncpy_s(nname, nlen + 1, name, nlen);
		strncpy_s(nname + nlen, slen + 1, name, slen);
		nname[slen + nlen] = 0;

		delete[] name;

		name = nname;

		ResourceID::hash = resourceHash(package, name);
	}

	bool ResourceName::isValid() const {
		if (package && name) {
			return true;
		}
		return false;
	}

	bool ResourceName::isValidName(const char *name) {
		size_t len = strlen(name);
		for (size_t i = 0; i < len; i++) {
			char n = name[i];
			//      --- 0 - 9 ---             --- A - Z ---           --- a - z ---       --- _ ---
			if (!((n >= 48 && n <= 57) || (n >= 65 && n <= 90) || (n >= 97 && n <= 122) || n == 95)) {
				return false;
			}
		}

		return true;
	}
}