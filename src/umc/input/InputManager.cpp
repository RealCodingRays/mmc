#include "umc\input\InputManager.h"

#include "umc\Engine.h"

#include <iostream>

namespace umc {

	InputManager::InputManager() {
	}

	InputManager::~InputManager() {
	}

	void InputManager::registerHandler(InputHandler *cb, int key, InputLayer layer) {
		handlers[key].push_back(handler{ cb, layer });
	}

	UI32 InputManager::registerAxis(int positive, int negative, InputLayer layer) {
		axis &ax = axes[nextAxisId];
		ax.positive = positive;
		ax.negative = negative;
		ax.mask = layer;

		return nextAxisId++;
	}

	F32 InputManager::getAxis(UI32 id) {
		auto it = axes.find(id);

		if (it != axes.end()) {
			if (static_cast<int>(it->second.mask) & static_cast<int>(layermask)) {
				F32 ret{ 0 };

				if (glfwGetKey(Engine::window, it->second.positive) == GLFW_PRESS) {
					ret += 1;
				}
				if (glfwGetKey(Engine::window, it->second.negative) == GLFW_PRESS) {
					ret -= 1;
				}

				return ret;
			}
			else {
				return 0;
			}
		}
		else {
			std::cout << "[WARNING][InputManager.cpp]: Requested non existing axis!" << std::endl;
			return 0;
		}
	}

	F32 InputManager::getCursorX() {
		return cposx;
	}

	F32 InputManager::getCursorY() {
		return cposy;
	}

	F32 InputManager::getCursordX(InputLayer layer) {
		if (static_cast<int>(layermask) & static_cast<int>(layer)) {
			return cdx;
		}
		else {
			return 0;
		}
	}

	F32 InputManager::getCursordY(InputLayer layer) {
		if (static_cast<int>(layermask) & static_cast<int>(layer)) {
			return cdy;
		}
		else {
			return 0;
		}
	}

	void InputManager::lockCursor() {
		glfwSetInputMode(Engine::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}

	void InputManager::unlockCursor() {
		glfwSetInputMode(Engine::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	void InputManager::setCursorLock(bool lock) {
		if (lock) {
			glfwSetInputMode(Engine::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
		else {
			glfwSetInputMode(Engine::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}

	void InputManager::enableLayer(InputLayer layer) {
		layermask = static_cast<InputLayer>(static_cast<int>(layermask) | static_cast<int>(layer));
	}

	void InputManager::disableLayer(InputLayer layer) {
		layermask = static_cast<InputLayer>(static_cast<int>(layermask) & (~static_cast<int>(layer)));
	}

	void InputManager::update() {
		cdx = 0;
		cdy = 0;
	}

	void InputManager::onKeyDown(int key) {
		auto it = handlers.find(key);
		
		if (it != handlers.end()) {
			for (auto hd : it->second) {
				if (static_cast<int>(hd.mask) & static_cast<int>(layermask)) {
					hd.hdlr->onKeyDown(key);
				}
			}
		}
	}

	void InputManager::onKeyUp(int key) {
		auto it = handlers.find(key);

		if (it != handlers.end()) {
			for (auto hd : it->second) {
				if (static_cast<int>(hd.mask) & static_cast<int>(layermask)) {
					hd.hdlr->onKeyUp(key);
				}
			}
		}
	}

	void InputManager::onCursorMove(F32 x, F32 y) {
		cdx = x - cposx;
		cdy = y - cposy;
		cposx = x;
		cposy = y;
	}
}