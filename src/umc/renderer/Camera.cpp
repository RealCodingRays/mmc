#include "umc\renderer\Camera.h"

#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "umc\io\resource\TextResource.h"
#include "umc\io\ResourceManager.h"

namespace umc {

	Camera::Camera() {
	}

	Camera::~Camera() {
	}

	void Camera::configureProjectionPerspective(F32 fov, F32 znear, F32 zfar, F32 aspect) {
		projection = glm::mat4{ 0.f };

		const F32 r = 0.25f;
		F32 zn = r / std::atanf(fov / 2);
		F32 t = (1.f / aspect) * r;

		projection[0][0] = zn / r;
		projection[1][1] = zn / t;	
		projection[2][2] = -(zfar + zn) / (zfar - zn);
		projection[3][2] = -(2 * zfar*zn) / (zfar - zn);
		projection[2][3] = -1;
	}

	void Camera::configureProjectionOrtho(F32 znear, F32 zfar, F32 aspect) {
		F32 w = aspect / 2.f;
		projection = glm::ortho(-w, w, -0.5f, 0.5f, znear, zfar);
	}

	void Camera::configureProjection(glm::mat4 matrix) {
		projection = matrix;
	}
}