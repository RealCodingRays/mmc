#include "umc\renderer\Window.h"

#include <iostream>

#include "umc\Engine.h"

namespace umc {

	Window::Window() {
	}

	Window::~Window() {
	}

	bool Window::init() {
		if (window == nullptr) {
			if (glfwInit() == GLFW_FALSE) {
				std::cout << "[ERROR][Window.cpp]: Failed to initilaize GLFW!";
				return false;
			}

			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, static_cast<int>(Engine::startupConfig.getocInt("GL_VERSION_MAJOR", 4)));
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, static_cast<int>(Engine::startupConfig.getocInt("GL_VERSION_MINOR", 5)));
			glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
			glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

			UI32 fb_width = static_cast<UI32>(Engine::startupConfig.getocInt("WINDOW_WIDTH", 800));
			UI32 fb_height = static_cast<UI32>(Engine::startupConfig.getocInt("WINDOW_HEIGHT", 600));

			if ((window = glfwCreateWindow(fb_width, fb_height, "uMC", nullptr, nullptr)) == nullptr) {
				std::cout << "[ERROR][Window.cpp]: Failed to create window!";
				glfwTerminate();
				return false;
			}

			glfwSetFramebufferSizeCallback(window, Window::framebufferResizeCallback);
			glfwSetKeyCallback(window, Window::keyCallback);
			glfwSetMouseButtonCallback(window, Window::mouseCallback);
			glfwSetCursorPosCallback(window, Window::cursorCallback);

			return true;
		}
		else {
			std::cout << "[WARNING][Window.cpp]: Called Window::init() twice!" << std::endl;
			return false;
		}
	}

	void Window::deinit() {
		if (window) {
			glfwTerminate();
		}
		else {
			std::cout << "[WARNING][Window.cpp]: Called Window::deinit() without having called Window::init()!" << std::endl;
		}
	}

	void Window::setWindowSize(UI32 width, UI32 height) {
		glfwSetWindowSize(window, width, height);
	}

	void Window::getFramebufferSize(UI32 &width, UI32 &height) {
		int w, h;
		glfwGetFramebufferSize(window, &w, &h);
		width = w;
		height = h;
	}

	UI32 Window::getFramebufferWidth() {
		int w, h;
		glfwGetFramebufferSize(window, &w, &h);
		return w;
	}

	UI32 Window::getFramebufferHeight() {
		int w, h;
		glfwGetFramebufferSize(window, &w, &h);
		return h;
	}

	void Window::setTitle(const char *title) {
		glfwSetWindowTitle(window, title);
	}

	void Window::show() {
		glfwShowWindow(window);
	}

	void Window::hide() {
		glfwHideWindow(window);
	}

	void Window::setVisible(bool visible) {
		if (visible) {
			glfwShowWindow(window);
		}
		else {
			glfwHideWindow(window);
		}
	}

	void Window::pollEvents() {
		glfwPollEvents();
	}

	bool Window::shouldExit() {
		return glfwWindowShouldClose(window) == GLFW_TRUE;
	}

	void Window::framebufferResizeCallback(GLFWwindow *window, int width, int height) {
		Engine::renderManager.onFramebufferResize(width, height);
	}

	void Window::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		switch (action) {
		case GLFW_PRESS:
			Engine::inputManager.onKeyDown(key);
			break;
		case GLFW_RELEASE:
			Engine::inputManager.onKeyUp(key);
			break;
		default:
			break;
		}
	}

	void Window::mouseCallback(GLFWwindow *window, int key, int action, int mod) {
		switch (action) {
		case GLFW_PRESS:
			Engine::inputManager.onKeyDown(key);
			break;
		case GLFW_RELEASE:
			Engine::inputManager.onKeyUp(key);
			break;
		default:
			break;
		}
	}

	void Window::cursorCallback(GLFWwindow *window, double x, double y) {
		int w, h;
		glfwGetWindowSize(window, &w, &h);

		F32 nx = static_cast<F32>(std::round(x) / static_cast<double>(w));
		F32 ny = static_cast<F32>(std::round(y) / static_cast<double>(h));

		Engine::inputManager.onCursorMove(nx, ny);
	}
}