#include "umc\renderer\DebugRenderer.h"

#include "umc\io\ResourceManager.h"
#include "umc\io\resource\TextResource.h"

#include <iostream>

namespace umc {

	GLBuffer DebugRenderer::vertecies;
	GLVertexArray DebugRenderer::vao;
	GLProgram DebugRenderer::program;

	void DebugRenderer::draw(const glm::mat4 &wtoc, const glm::mat4 &ctos) {
		program.use();
		vao.bind();

		program.setUniformMatrix4("wtoc", gl::GL_FALSE, wtoc);
		program.setUniformMatrix4("ctos", gl::GL_FALSE, ctos);

		for(glm::vec3 &point : points) {
			program.setUniformValue("pos", point.x, point.y, point.z);
			gl::glDrawArrays(gl::GL_LINES, 0, 6);
		}
		points.clear();

		vao.unbind();
		program.unuse();
	}

	void DebugRenderer::drawPoint(glm::vec3 point) {
		points.push_back(point);
	}

	void DebugRenderer::init() {
		ResourceName vertName{ "umc:assets/shaders/Debug.vert" };
		ResourceName fragName{ "umc:assets/shaders/Debug.frag" };

		ResourceReference<TextResource> vertShad = ResourceManager::get().loadResource<TextResource>(vertName);
		ResourceReference<TextResource> fragShad = ResourceManager::get().loadResource<TextResource>(fragName);

		GLShader vert{ vertShad->get(), static_cast<gl::GLsizei>(vertShad->size()), gl::GL_VERTEX_SHADER };
		GLShader frag{ fragShad->get(), static_cast<gl::GLsizei>(fragShad->size()), gl::GL_FRAGMENT_SHADER };

		program.init();
		program.attach(vert);
		program.attach(frag);
		program.link();

		F32 verts[]{
			-0.5f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.5f, 0.f, 0.f, 1.f, 0.f, 0.f,
			0.f, -0.5f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.5f, 0.f, 0.f, 1.f, 0.f,
			0.f, 0.f, -0.5f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.5f, 0.f, 0.f, 1.f,
		};

		vertecies.init(sizeof(verts) / sizeof(F32), gl::GL_ARRAY_BUFFER, verts);

		vertecies.bind();

		vao.init();

		gl::GLint loc = program.getAttribLocation("vert");
		if (loc >= 0) {
			vao.setupFPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 6, nullptr);
		}

		loc = program.getAttribLocation("color");
		if (loc >= 0) {
			vao.setupFPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 6, reinterpret_cast<void *>(sizeof(F32) * 3));
		}

		vertecies.unbind();
	}

	void DebugRenderer::deinit() {
		vertecies.~GLBuffer();
		vao.~GLVertexArray();
		program.~GLProgram();
	}
}