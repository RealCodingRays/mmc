#include "umc\renderer\GBuffer.h"

#include <iostream>

namespace umc {

	GBuffer::GBuffer() : width{ 0 }, height{ 0 } {
	}

	GBuffer::GBuffer(UI32 width, UI32 height) : width{ width }, height{ height } {
		texdep.init(gl::GL_DEPTH_COMPONENT24, width, height);
		texpos.init2D(width, height, gl::GL_RGB16F, 1);
		texnor.init2D(width, height, gl::GL_RGB16F, 1);
		texalb.init2D(width, height, gl::GL_RGB8, 1);
		texmat.init2D(width, height, gl::GL_RGB8, 1);

		texpos.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texpos.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texpos.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texpos.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texnor.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texnor.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texnor.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texnor.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texalb.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texalb.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texalb.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texalb.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texmat.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texmat.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texmat.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texmat.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		fbuff.init();
		fbuff.attach(gl::GL_DEPTH_ATTACHMENT, texdep);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT0, texpos, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT1, texnor, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT2, texalb, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT3, texmat, 0);
		
		fbuff.bind(gl::GL_FRAMEBUFFER);

		gl::GLenum buffs[]{ gl::GL_COLOR_ATTACHMENT0, gl::GL_COLOR_ATTACHMENT1, gl::GL_COLOR_ATTACHMENT2, gl::GL_COLOR_ATTACHMENT3 };
		gl::glDrawBuffers(4, buffs);

		if (gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) != gl::GL_FRAMEBUFFER_COMPLETE) {
			std::cout << "[ERROR][GBuffer.cpp]: Failed to create GBuffer! " << gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) << std::endl;
		}
		fbuff.unbind(gl::GL_FRAMEBUFFER);
	}

	GBuffer::~GBuffer() {
	}

	void GBuffer::init(UI32 width, UI32 height) {
		texdep.init(gl::GL_DEPTH_COMPONENT24, width, height);
		//testdep.init2D(width, height, gl::GL_DEPTH_COMPONENT24, 1);
		texpos.init2D(width, height, gl::GL_RGB16F, 1);
		texnor.init2D(width, height, gl::GL_RGB16F, 1);
		texalb.init2D(width, height, gl::GL_RGB8, 1);
		texmat.init2D(width, height, gl::GL_RGB8, 1);

		texpos.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texpos.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texpos.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texpos.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texnor.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texnor.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texnor.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texnor.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texalb.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texalb.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texalb.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texalb.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texmat.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texmat.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texmat.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texmat.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		fbuff.init();
		fbuff.attach(gl::GL_DEPTH_ATTACHMENT, texdep);
		//fbuff.attach(gl::GL_DEPTH_ATTACHMENT, testdep, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT0, texpos, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT1, texnor, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT2, texalb, 0);
		fbuff.attach(gl::GL_COLOR_ATTACHMENT3, texmat, 0);

		fbuff.bind(gl::GL_FRAMEBUFFER);

		gl::GLenum buffs[]{ gl::GL_COLOR_ATTACHMENT0, gl::GL_COLOR_ATTACHMENT1, gl::GL_COLOR_ATTACHMENT2, gl::GL_COLOR_ATTACHMENT3 };
		gl::glDrawBuffers(4, buffs);

		if (gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) != gl::GL_FRAMEBUFFER_COMPLETE) {
			std::cout << "[ERROR][GBuffer.cpp]: Failed to create GBuffer! " << gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) << std::endl;
		}
		fbuff.unbind(gl::GL_FRAMEBUFFER);
	}

	void GBuffer::bind() {
		fbuff.bind(gl::GL_FRAMEBUFFER);
	}

	void GBuffer::unbind() {
		fbuff.unbind(gl::GL_FRAMEBUFFER);
	}

	void GBuffer::clear() {
	}

	void GBuffer::configureTextureTargets(gl::GLenum pos, gl::GLenum nor, gl::GLenum alb, gl::GLenum mat) {
		trgpos = pos;
		trgnor = nor;
		trgalb = alb;
		trgmat = mat;
	}

	void GBuffer::bindTextures() {
		gl::glActiveTexture(trgpos);
		texpos.bind();
		gl::glActiveTexture(trgnor);
		texnor.bind();
		gl::glActiveTexture(trgalb);
		texalb.bind();
		gl::glActiveTexture(trgmat);
		texmat.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0);
	}

	void GBuffer::unbindTextures() {
		gl::glActiveTexture(trgpos);
		texpos.unbind();
		gl::glActiveTexture(trgnor);
		texnor.unbind();
		gl::glActiveTexture(trgalb);
		texalb.unbind();
		gl::glActiveTexture(trgmat);
		texmat.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0);
	}
}