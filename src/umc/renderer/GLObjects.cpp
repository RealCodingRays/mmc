
#include <iostream>
#include "umc\renderer\native\OpenGL.h"
#include "umc\renderer\GLObjects.h"


namespace umc {

	GLBuffer::GLBuffer() {
	}

	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target) : bsize{ size }, trgt{ target }, flags{ gl::BufferStorageMask::GL_NONE_BIT } {
		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, nullptr, flags);
		gl::glBindBuffer(target, 0);
	}

	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target, gl::BufferStorageMask hints) : bsize{ size }, trgt{ target }, flags{ hints } {
		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, nullptr, hints);
		gl::glBindBuffer(target, 0);
	}

	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target, const void *data) : bsize{ size }, trgt{ target }, flags{ gl::BufferStorageMask::GL_NONE_BIT } {
		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, data, flags);
		gl::glBindBuffer(target, 0);
	}

	GLBuffer::GLBuffer(gl::GLsizei size, gl::GLenum target, const void *data, gl::BufferStorageMask hints) : bsize{ size }, trgt{ target }, flags{ hints } {
		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, data, hints);
		gl::glBindBuffer(target, 0);
	}

	GLBuffer::GLBuffer(const GLBuffer &other) : bsize{ other.bsize }, trgt{ other.trgt }, flags{ other.flags } {
		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(trgt, id);
		gl::glBufferStorage(trgt, bsize, nullptr, flags);
		gl::glBindBuffer(trgt, 0);

		gl::glCopyNamedBufferSubData(other.id, id, 0, 0, bsize);
	}

	GLBuffer::GLBuffer(GLBuffer &&other) : id{ other.id }, bsize{ other.bsize }, trgt{ other.trgt }, flags{ other.flags } {
		other.id = 0;
	}

	GLBuffer::~GLBuffer() {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
			id = 0;
		}
	}

	GLBuffer &GLBuffer::operator =(const GLBuffer &other) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		bsize = other.bsize;
		trgt = other.trgt;
		flags = other.flags;

		gl::glGenBuffers(1, &id);
		gl::glBindBuffer(trgt, id);
		gl::glBufferStorage(trgt, bsize, nullptr, gl::BufferStorageMask::GL_NONE_BIT);
		gl::glBindBuffer(trgt, 0);

		gl::glCopyNamedBufferSubData(other.id, id, 0, 0, bsize);

		return *this;
	}

	GLBuffer &GLBuffer::operator =(GLBuffer &&other) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		id = other.id;
		other.id = 0;
		bsize = other.bsize;
		trgt = other.trgt;
		flags = other.flags;

		return *this;
	}

	void GLBuffer::init(gl::GLsizei size, gl::GLenum target) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		bsize = size;
		trgt = target;
		flags = gl::BufferStorageMask::GL_NONE_BIT;

		gl::glGenBuffers(1, &id);

		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, nullptr, flags);
		gl::glBindBuffer(target, 0);
	}

	void GLBuffer::init(gl::GLsizei size, gl::GLenum target, gl::BufferStorageMask hints) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		bsize = size;
		trgt = target;
		flags = hints;

		gl::glGenBuffers(1, &id);

		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, nullptr, hints);
		gl::glBindBuffer(target, 0);
	}

	void GLBuffer::init(gl::GLsizei size, gl::GLenum target, const void *data) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		bsize = size;
		trgt = target;
		flags = gl::BufferStorageMask::GL_NONE_BIT;

		gl::glGenBuffers(1, &id);

		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, data, flags);
		gl::glBindBuffer(target, 0);
	}

	void GLBuffer::init(gl::GLsizei size, gl::GLenum target, const void *data, gl::BufferStorageMask hints) {
		if (id != 0) {
			gl::glDeleteBuffers(1, &id);
		}

		bsize = size;
		trgt = target;
		flags = hints;

		gl::glGenBuffers(1, &id);

		gl::glBindBuffer(target, id);
		gl::glBufferStorage(target, size, data, hints);
		gl::glBindBuffer(target, 0);
	}

	void GLBuffer::bind() {
		gl::glBindBuffer(trgt, id);
	}

	void GLBuffer::bind(gl::GLenum target) {
		gl::glBindBuffer(target, id);
	}

	void GLBuffer::unbind() {
		gl::glBindBuffer(trgt, 0);
	}

	void GLBuffer::unbind(gl::GLenum target) {
		gl::glBindBuffer(target, 0);
	}

	void *GLBuffer::map() {
		return gl::glMapNamedBufferRange(id, 0, bsize, gl::BufferAccessMask::GL_MAP_READ_BIT | gl::BufferAccessMask::GL_MAP_WRITE_BIT);
	}

	void GLBuffer::setDataRaw(gl::GLintptr offset, gl::GLsizei length, const void *data) {
		gl::glNamedBufferSubData(id, offset, length, data);
	}

	void GLBuffer::getDataRaw(gl::GLintptr offset, gl::GLsizei length, void *data) const {
		gl::glGetNamedBufferSubData(id, offset, length, data);
	}

	void GLBuffer::resizeRaw(gl::GLsizei size, bool copy) {
		gl::GLuint nbuff;
		gl::glGenBuffers(1, &nbuff);

		gl::glBindBuffer(trgt, nbuff);
		gl::glBufferStorage(trgt, size, nullptr, flags);
		gl::glBindBuffer(trgt, 0);

		if (copy) {
			gl::glCopyNamedBufferSubData(id, nbuff, 0, 0, std::min(size, bsize));
		}

		gl::glDeleteBuffers(1, &id);

		id = nbuff;
		bsize = size;
	}

	void GLBuffer::copyRaw(gl::GLintptr offset, gl::GLsizei length, gl::GLuint src, gl::GLintptr srcoffset) {
		gl::glCopyNamedBufferSubData(src, id, srcoffset, offset, length);
	}

	//
	// GLTexture
	//
	GLTexture::GLTexture() {
	}

	GLTexture::GLTexture(gl::GLsizei width, gl::GLenum format, gl::GLint levels, gl::GLenum type) : width{ width }, height{ 1 }, depth{ 1 }, type{ type } {
		gl::glGenTextures(1, &id);
		gl::glBindTexture(type, id);
		gl::glTextureStorage1D(id, levels, format, width);
		gl::glBindTexture(type, 0);
	}

	GLTexture::GLTexture(gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLint levels, gl::GLenum type) : width{ width }, height{ height }, depth{ 1 }, type{ type } {
		gl::glGenTextures(1, &id);
		gl::glBindTexture(type, id);
		gl::glTextureStorage2D(id, levels, format, width, height);
		gl::glBindTexture(type, 0);
	}

	GLTexture::GLTexture(gl::GLsizei width, gl::GLsizei height, gl::GLsizei depth, gl::GLenum format, gl::GLint levels, gl::GLenum type) : width{ width }, height{ height }, depth{ depth }, type{ type } {
		gl::glGenTextures(1, &id);
		gl::glBindTexture(type, id);
		gl::glTextureStorage3D(id, levels, format, width, height, depth);
		gl::glBindTexture(type, 0);
	}

	GLTexture::GLTexture(GLTexture &&other) : id{ other.id }, width{ other.width }, height{ other.height }, depth{ other.depth }, type{ other.type } {
		other.id = 0;
	}

	GLTexture::~GLTexture() {
		if (id != 0) {
			gl::glDeleteTextures(1, &id);
		}
	}

	void GLTexture::init(ImageResource *res, gl::GLint levels) {
		if (id != 0) {
			gl::glDeleteTextures(1, &id);
		}

		gl::glGenTextures(1, &id);
		gl::glBindTexture(gl::GL_TEXTURE_2D, id);
		gl::GLenum intform;
		gl::GLenum form;
		switch (res->getNumChannels()) {
		case 1:
			if (res->getBitDepth() == 8) {
				intform = gl::GL_R8;
			}
			else {
				intform = gl::GL_R16;
			}
			form = gl::GL_RED;
			break;
		case 2:
			if (res->getBitDepth() == 8) {
				intform = gl::GL_RG8;
			}
			else {
				intform = gl::GL_RG16;
			}
			form = gl::GL_RG;
			break;
		case 3:
			if (res->getBitDepth() == 8) {
				intform = gl::GL_RGB8;
			}
			else {
				intform = gl::GL_RGB16;
			}
			form = gl::GL_RGB;
			break;
		case 4:
			if (res->getBitDepth() == 8) {
				intform = gl::GL_RGBA8;
			}
			else {
				intform = gl::GL_RGBA16;
			}
			form = gl::GL_RGBA;
			break;
		}
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, intform, res->getWidth(), res->getHeight());
		gl::GLenum dtype;
		if (res->getBitDepth() == 8) {
			dtype = gl::GL_UNSIGNED_BYTE;
		}
		else {
			dtype = gl::GL_UNSIGNED_SHORT;
		}
		gl::glTexSubImage2D(gl::GL_TEXTURE_2D, 0, 0, 0, res->getWidth(), res->getHeight(), form, dtype, res->get());
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		
		type = gl::GL_TEXTURE_2D;
	}

	void GLTexture::initCube(ImageResource *fr, ImageResource *ri, ImageResource *ba, ImageResource *le, ImageResource *to, ImageResource *bo) {
		if (id != 0) {
			gl::glDeleteTextures(1, &id);
		}

		gl::glGenTextures(1, &id);
		gl::glBindTexture(gl::GL_TEXTURE_CUBE_MAP, id);
		
		gl::glTexStorage2D(gl::GL_TEXTURE_CUBE_MAP, 1, gl::GL_RGB8, fr->getWidth(), fr->getHeight());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, 0, 0, ri->getWidth(), ri->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, ri->get());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, 0, 0, le->getWidth(), le->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, le->get());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, 0, 0, to->getWidth(), to->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, to->get());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, 0, 0, bo->getWidth(), bo->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, bo->get());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, 0, 0, ba->getWidth(), ba->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, ba->get());
		gl::glTexSubImage2D(gl::GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, 0, 0, fr->getWidth(), fr->getHeight(), gl::GL_RGB, gl::GL_UNSIGNED_BYTE, fr->get());

		type = gl::GL_TEXTURE_CUBE_MAP;
	}

	void GLTexture::init1D(gl::GLsizei width, gl::GLenum format, gl::GLint levels) {
		if (id != 0) {
			gl::glDeleteTextures(1, &id);
		}

		gl::glGenTextures(1, &id);
		gl::glBindTexture(gl::GL_TEXTURE_1D, id);
		gl::glTexStorage1D(gl::GL_TEXTURE_1D, levels, format, width);
		gl::glBindTexture(gl::GL_TEXTURE_1D, 0);
		
		type = gl::GL_TEXTURE_1D;
	}

	void GLTexture::init2D(gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLint levels) {
		if (id != 0) {
			gl::glDeleteTextures(1, &id);
		}

		gl::glGenTextures(1, &id);
		gl::glBindTexture(gl::GL_TEXTURE_2D, id);
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, format, width, height);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);

		type = gl::GL_TEXTURE_2D;
	}

	void GLTexture::setData1D(gl::GLint level, gl::GLint x, gl::GLsizei width, gl::GLenum type, gl::GLenum format, const void *data) {
		gl::glTextureSubImage1D(id, level, x, width, format, type, data);
	}

	void GLTexture::bind() {
		gl::glBindTexture(type, id);
	}

	void GLTexture::unbind() {
		gl::glBindTexture(type, 0);
	}

	void GLTexture::setParameter(gl::GLenum name, gl::GLenum val) {
		gl::glTextureParameteri(id, name, val);
	}


	//
	// GLRenderbuffer
	//
	GLRenderbuffer::GLRenderbuffer() {
	}

	GLRenderbuffer::GLRenderbuffer(gl::GLenum format, gl::GLsizei width, gl::GLsizei height) : format{ format }, width{ width }, height{ height } {
		gl::glGenRenderbuffers(1, &id);

		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, id);
		gl::glRenderbufferStorage(gl::GL_RENDERBUFFER, format, width, height);
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
	}

	GLRenderbuffer::GLRenderbuffer(gl::GLsizei samples, gl::GLenum format, gl::GLsizei width, gl::GLsizei height) : format{ format }, width{ width }, height{ height } {
		gl::glGenRenderbuffers(1, &id);

		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, id);
		gl::glRenderbufferStorageMultisample(gl::GL_RENDERBUFFER, samples, format, width, height);
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
	}

	GLRenderbuffer::GLRenderbuffer(GLRenderbuffer &&other) : id{ other.id }, width{ other.width }, height{ other.height }, format{ other.format } {
		other.id = 0;
	}

	GLRenderbuffer::~GLRenderbuffer() {
		if (id != 0) {
			gl::glDeleteRenderbuffers(1, &id);
			id = 0;
		}
	}

	GLRenderbuffer &GLRenderbuffer::operator =(GLRenderbuffer &&other) {
		if (id != 0) {
			gl::glDeleteRenderbuffers(1, &id);
		}

		id = other.id;
		other.id = 0;
		format = other.format;
		width = other.width;
		height = other.height;

		return *this;
	}

	void GLRenderbuffer::init(gl::GLenum format, gl::GLsizei width, gl::GLsizei height) {
		if (id != 0) {
			gl::glDeleteRenderbuffers(1, &id);
		}

		this->format = format;
		this->width = width;
		this->height = height;

		gl::glGenRenderbuffers(1, &id);

		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, id);
		gl::glRenderbufferStorage(gl::GL_RENDERBUFFER, format, width, height);
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
	}

	void GLRenderbuffer::init(gl::GLsizei samples, gl::GLenum format, gl::GLsizei width, gl::GLsizei height) {
		if (id != 0) {
			gl::glDeleteRenderbuffers(1, &id);
		}

		this->format = format;
		this->width = width;
		this->height = height;

		gl::glGenRenderbuffers(1, &id);

		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, id);
		gl::glRenderbufferStorageMultisample(gl::GL_RENDERBUFFER, samples, format, width, height);
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
	}

	void GLRenderbuffer::bind() {
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, id);
	}

	void GLRenderbuffer::unbind() {
		gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
	}

	//
	// GLFrambuffer
	//
	GLFramebuffer::GLFramebuffer() {
	}

	GLFramebuffer::GLFramebuffer(GLFramebuffer &&other) : id{ other.id } {
		other.id = 0;
	}

	GLFramebuffer::~GLFramebuffer() {
		if (id != 0) {
			gl::glDeleteFramebuffers(1, &id);
			id = 0;
		}
	}

	GLFramebuffer &GLFramebuffer::operator =(GLFramebuffer &&other) {
		if (id != 0) {
			gl::glDeleteFramebuffers(1, &id);
		}

		id = other.id;
		other.id = 0;

		return *this;
	}

	void GLFramebuffer::init() {
		if (id != 0) {
			gl::glDeleteFramebuffers(1, &id);
		}

		gl::glGenFramebuffers(1, &id);
	}

	void GLFramebuffer::attachRenderbuffer(gl::GLenum target, gl::GLuint renderbuffer) {
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, id);
		gl::glFramebufferRenderbuffer(gl::GL_FRAMEBUFFER, target, gl::GL_RENDERBUFFER, renderbuffer);
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
	}

	void GLFramebuffer::attachTexture(gl::GLenum target, gl::GLuint texture, gl::GLint level) {
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, id);
		gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, target, gl::GL_TEXTURE_2D, texture, level);
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
	}

	bool GLFramebuffer::ready() {
		if (id == 0) {
			return false;
		}

		gl::GLenum res = gl::glCheckNamedFramebufferStatus(id, gl::GL_FRAMEBUFFER);
		if (res != gl::GL_FRAMEBUFFER_COMPLETE) {
			return false;
		}

		return true;
	}

	void GLFramebuffer::bind(gl::GLenum target) {
		gl::glBindFramebuffer(target, id);
	}

	void GLFramebuffer::unbind(gl::GLenum target) {
		gl::glBindFramebuffer(target, 0);
	}

	//
	// GLShader
	//
	GLShader::GLShader() {
	}

	GLShader::GLShader(const gl::GLchar *data, gl::GLsizei length, gl::GLenum type) {
		id = gl::glCreateShader(type);

		gl::glShaderSource(id, 1, &data, &length);
		gl::glCompileShader(id);

		gl::GLboolean res;
		gl::glGetShaderiv(id, gl::GL_COMPILE_STATUS, &res);
		if (res == gl::GL_FALSE) {
			gl::GLint len;
			gl::glGetShaderiv(id, gl::GL_INFO_LOG_LENGTH, &len);

			if (len >= 0) {
				gl::GLchar *log = new char[len + 1];
				gl::glGetShaderInfoLog(id, len + 1, &len, log);

				std::cout << "ERROR compiling shader: " << log << std::endl;

				delete[] log;
			}
			else {
				std::cout << "ERROR compiling shader: unable to get info log" << std::endl;
			}

			gl::glDeleteShader(id);
			id = 0;
		}
	}

	GLShader::GLShader(GLShader &&other) : id{ other.id } {
		other.id = 0;
	}

	GLShader::~GLShader() {
		if (id != 0) {
			gl::glDeleteShader(id);
			id = 0;
		}
	}

	GLShader &GLShader::operator =(GLShader &&other) {
		if (id != 0) {
			gl::glDeleteShader(id);
		}

		id = other.id;
		other.id = 0;

		return *this;
	}

	void GLShader::init(const gl::GLchar *data, gl::GLsizei length, gl::GLenum type) {
		if (id != 0) {
			gl::glDeleteShader(id);
		}

		id = gl::glCreateShader(type);

		gl::glShaderSource(id, 1, &data, &length);
		gl::glCompileShader(id);

		gl::GLboolean res;
		gl::glGetShaderiv(id, gl::GL_COMPILE_STATUS, &res);
		if (res == gl::GL_FALSE) {
			gl::GLint len;
			gl::glGetShaderiv(id, gl::GL_INFO_LOG_LENGTH, &len);

			if (len > 0) {
				gl::GLchar *log = new char[len + 1];
				log[len];
				gl::glGetShaderInfoLog(id, len, &len, log);

				std::cout << "ERROR compiling shader: " << log << std::endl;

				delete[] log;
			}
			else {
				std::cout << "ERROR compiling shader: unable to get info log" << std::endl;
			}

			gl::glDeleteShader(id);
			id = 0;
		}
	}

	//
	// GLProgram
	//
	GLProgram::GLProgram() {
	}

	GLProgram::GLProgram(GLProgram &&other) : id{ other.id } {
		other.id = 0;
	}

	GLProgram::~GLProgram() {
		if (id != 0) {
			gl::glDeleteProgram(id);
			id = 0;
		}
	}

	GLProgram &GLProgram::operator =(GLProgram &&other) {
		if (id != 0) {
			gl::glDeleteProgram(id);
		}

		id = other.id;
		other.id = 0;

		return *this;
	}

	void GLProgram::init() {
		if (id != 0) {
			gl::glDeleteProgram(id);
		}

		id = gl::glCreateProgram();
	}

	void GLProgram::attach(gl::GLuint shader) {
		gl::glAttachShader(id, shader);
	}

	bool GLProgram::link() {
		gl::glLinkProgram(id);

		gl::GLboolean res;
		gl::glGetProgramiv(id, gl::GL_LINK_STATUS, &res);
		if (res == gl::GL_FALSE) {
			gl::GLint len;
			gl::glGetProgramiv(id, gl::GL_INFO_LOG_LENGTH, &len);
			std::cout << len;

			if (len > 0) {
				gl::GLchar *log = new gl::GLchar[len + 1];
				log[len] = 0;
				gl::glGetProgramInfoLog(id, len, &len, log);

				std::cout << "ERROR linking program: " << log << std::endl;

				delete[] log;
			}
			else {
				std::cout << "ERROR linking program: unable to get info log" << std::endl;
			}

			gl::glDeleteProgram(id);
			id = 0;
			return false;
		}
		return true;
	}

	bool GLProgram::isReady() const {
		if (id == 0) {
			return false;
		}

		gl::GLboolean res;
		gl::glGetProgramiv(id, gl::GL_LINK_STATUS, &res);
		return res == gl::GL_TRUE;
	}

	void GLProgram::use() {
		gl::glUseProgram(id);
	}

	void GLProgram::unuse() {
		gl::glUseProgram(0);
	}

	void GLProgram::setAttribLocation(const char *name, gl::GLint location) {
		gl::glBindAttribLocation(id, location, name);
	}

	gl::GLint GLProgram::getAttribLocation(const char *name) const {
		return gl::glGetAttribLocation(id, name);
	}

	gl::GLint GLProgram::getUniformLocation(const char *name) const {
		return gl::glGetUniformLocation(id, name);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLfloat v0) {
		gl::glProgramUniform1f(id, location, v0);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1) {
		gl::glProgramUniform2f(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2) {
		gl::glProgramUniform3f(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2, gl::GLfloat v3) {
		gl::glProgramUniform4f(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLint v0) {
		gl::glProgramUniform1i(id, location, v0);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1) {
		gl::glProgramUniform2i(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1, gl::GLint v2) {
		gl::glProgramUniform3i(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLint v0, gl::GLint v1, gl::GLint v2, gl::GLint v3) {
		gl::glProgramUniform4i(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLuint v0) {
		gl::glProgramUniform1ui(id, location, v0);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1) {
		gl::glProgramUniform2ui(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2) {
		gl::glProgramUniform3ui(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(gl::GLint location, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2, gl::GLuint v3) {
		gl::glProgramUniform4ui(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data) {
		gl::glProgramUniform1fv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data) {
		gl::glProgramUniform2fv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data) {
		gl::glProgramUniform3fv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLfloat *data) {
		gl::glProgramUniform4fv(id, location, count, data);
	}

	void GLProgram::setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLint *data) {
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLint *data) {
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLint *data) {
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLint *data) {
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue1(gl::GLint location, gl::GLsizei count, const gl::GLuint *data) {
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(gl::GLint location, gl::GLsizei count, const gl::GLuint *data) {
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(gl::GLint location, gl::GLsizei count, const gl::GLuint *data) {
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(gl::GLint location, gl::GLsizei count, const gl::GLuint *data) {
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformMatrix2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix3fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix2x3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix2x3fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3x2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix3x2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix2x4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix2x4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4x2(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix4x2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3x4(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix3x4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4x3(gl::GLint location, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::glProgramUniformMatrix4x3fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLfloat v0) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1f(id, location, v0);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform2f(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform3f(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLfloat v0, gl::GLfloat v1, gl::GLfloat v2, gl::GLfloat v3) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform4f(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLint v0) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1i(id, location, v0);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform2i(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1, gl::GLint v2) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform3i(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLint v0, gl::GLint v1, gl::GLint v2, gl::GLint v3) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform4i(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLuint v0) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1ui(id, location, v0);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform2ui(id, location, v0, v1);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform3ui(id, location, v0, v1, v2);
	}

	void GLProgram::setUniformValue(const gl::GLchar *name, gl::GLuint v0, gl::GLuint v1, gl::GLuint v2, gl::GLuint v3) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform4ui(id, location, v0, v1, v2, v3);
	}

	void GLProgram::setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1fv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform2fv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform3fv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform4fv(id, location, count, data);
	}

	void GLProgram::setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1iv(id, location, count, data);
	}

	void GLProgram::setUniformValue1(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue2(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue3(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformValue4(const gl::GLchar *name, gl::GLsizei count, const gl::GLuint *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniform1uiv(id, location, count, data);
	}

	void GLProgram::setUniformMatrix2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix3fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix2x3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix2x3fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3x2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix3x2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix2x4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix2x4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4x2(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix4x2fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix3x4(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix3x4fv(id, location, count, transpose, data);
	}

	void GLProgram::setUniformMatrix4x3(const gl::GLchar *name, gl::GLsizei count, gl::GLboolean transpose, const gl::GLfloat *data) {
		gl::GLint location = gl::glGetUniformLocation(id, name);
		gl::glProgramUniformMatrix4x3fv(id, location, count, transpose, data);
	}

	//
	// GLVertexArray
	//
	GLVertexArray::GLVertexArray() {
	}

	GLVertexArray::GLVertexArray(GLVertexArray &&other) : id{ other.id } {
		other.id = 0;
	}

	GLVertexArray::~GLVertexArray() {
		if (id != 0) {
			gl::glDeleteVertexArrays(1, &id);
			id = 0;
		}
	}

	GLVertexArray &GLVertexArray::operator =(GLVertexArray &&other) {
		if (id != 0) {
			gl::glDeleteVertexArrays(1, &id);
		}

		id = other.id;
		other.id = 0;

		return *this;
	}

	void GLVertexArray::init() {
		if (id != 0) {
			gl::glDeleteVertexArrays(1, &id);
		}

		gl::glGenVertexArrays(1, &id);
	}

	void GLVertexArray::setupFPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLboolean normalized, gl::GLsizei stride, const void *offset) {
		gl::glBindVertexArray(id);
		gl::glEnableVertexAttribArray(index);
		gl::glVertexAttribPointer(index, size, type, normalized, stride, offset);
		gl::glBindVertexArray(0);
	}

	void GLVertexArray::setupIPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLsizei stride, const void *offset) {
		gl::glBindVertexArray(id);
		gl::glEnableVertexAttribArray(index);
		gl::glVertexAttribIPointer(index, size, type, stride, offset);
		gl::glBindVertexArray(0);
	}

	void GLVertexArray::setupLPointer(gl::GLuint index, gl::GLint size, gl::GLenum type, gl::GLsizei stride, const void *offset) {
		gl::glBindVertexArray(id);
		gl::glEnableVertexAttribArray(index);
		gl::glVertexAttribLPointer(index, size, type, stride, offset);
		gl::glBindVertexArray(0);
	}

	void GLVertexArray::enableLocation(gl::GLuint index) {
		gl::glEnableVertexArrayAttrib(id, index);
	}

	void GLVertexArray::disableLocation(gl::GLuint index) {
		gl::glDisableVertexArrayAttrib(id, index);
	}

	void GLVertexArray::bind() {
		gl::glBindVertexArray(id);
	}

	void GLVertexArray::unbind() {
		gl::glBindVertexArray(0);
	}
}