#include "umc\renderer\MeshRegistry.h"

#include "umc\renderer\native\OpenGL.h"

#include <cassert>

namespace umc {

	MeshRegistry::MeshRegistry() {
	}

	MeshRegistry::~MeshRegistry() {
	}

	Mesh &MeshRegistry::getMesh(const ResourceID &id) {
		return meshes[id].mesh;
	}

	Mesh &MeshRegistry::cloneMesh(const ResourceID &dest, const ResourceID &src) {
		auto it = meshes.find(src);

		if (it != meshes.end()) {
			meshes[dest].mesh = it->second.mesh;
		}

		assert(false);
		return meshes[dest].mesh;
	}

	gl::GLuint MeshRegistry::getBuffer(const ResourceID &id) {
		auto it = meshes.find(id);

		if (it != meshes.end()) {
			if (it->second.buffer == 0) {
				gl::glGenBuffers(1, &(it->second.buffer));

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, it->second.buffer);

				gl::glBufferStorage(gl::GL_ARRAY_BUFFER, it->second.mesh.getNumVertecies() * sizeof(Vertex), it->second.mesh.getVertecies(), gl::GL_NONE_BIT);

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
			}

			return it->second.buffer;
		}

		assert(false);
		return 0;
	}

	void MeshRegistry::updateBuffer(const ResourceID &id) {
		auto it = meshes.find(id);

		if (it != meshes.end()) {
			if (it->second.buffer == 0) {
				gl::glGenBuffers(1, &(it->second.buffer));

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, it->second.buffer);

				gl::glBufferStorage(gl::GL_ARRAY_BUFFER, it->second.mesh.getNumVertecies() * sizeof(Vertex), it->second.mesh.getVertecies(), gl::GL_NONE_BIT);

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
			}
			else {
				gl::GLuint nbuff;
				gl::glGenBuffers(1, &nbuff);

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, nbuff);

				gl::glBufferStorage(gl::GL_ARRAY_BUFFER, it->second.mesh.getNumVertecies() * sizeof(Vertex), it->second.mesh.getVertecies(), gl::GL_NONE_BIT);

				for (vaodata &vao : it->second.vaos) {
					if (vao.vao) {
						gl::glBindVertexArray(vao.vao);

						if (vao.pos >= 0) {
							gl::glVertexAttribPointer(vao.pos, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, position)));
						}

						if (vao.nor >= 0) {
							gl::glVertexAttribPointer(vao.nor, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, normal)));
						}

						if (vao.tan >= 0) {
							gl::glVertexAttribPointer(vao.tan, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, tangent)));
						}

						if (vao.uv >= 0) {
							gl::glVertexAttribPointer(vao.uv, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, uv)));
						}
					}
				}
				gl::glBindVertexArray(0);

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

				gl::glDeleteBuffers(1, &(it->second.buffer));

				it->second.buffer = nbuff;
			}
		}
		else {
			assert(false);
		}
	}

	void MeshRegistry::linkVertexArray(const ResourceID &id, gl::GLuint vao, gl::GLint position, gl::GLint normal, gl::GLint tangent, gl::GLint uv) {
		assert(vao != 0);

		auto it = meshes.find(id);

		if (it != meshes.end()) {
			if (it->second.buffer == 0) {
				gl::glGenBuffers(1, &(it->second.buffer));

				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, it->second.buffer);

				gl::glBufferStorage(gl::GL_ARRAY_BUFFER, it->second.mesh.getNumVertecies() * sizeof(Vertex), it->second.mesh.getVertecies(), gl::GL_NONE_BIT);
			}
			else {
				gl::glBindBuffer(gl::GL_ARRAY_BUFFER, it->second.buffer);
			}

			vaodata data;
			data.vao = vao;
			data.pos = position;
			data.nor = normal;
			data.tan = tangent;
			data.uv = uv;

			gl::glBindVertexArray(data.vao);

			if (data.pos >= 0) {
				gl::glVertexAttribPointer(data.pos, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, position)));
			}

			if (data.nor >= 0) {
				gl::glVertexAttribPointer(data.nor, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, normal)));
			}

			if (data.tan >= 0) {
				gl::glVertexAttribPointer(data.tan, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, tangent)));
			}

			if (data.uv >= 0) {
				gl::glVertexAttribPointer(data.uv, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), reinterpret_cast<void *>(offsetof(Vertex, uv)));
			}

			gl::glBindVertexArray(0);

			it->second.vaos.push_back(data);
		}
		else {
			assert(false);
		}
	}

	void MeshRegistry::unlinkVertexArray(const ResourceID &id, gl::GLuint vao) {
		auto it = meshes.find(id);

		if (it != meshes.end()) {
			for (auto va = it->second.vaos.begin(); va != it->second.vaos.end(); va++) {
				if (va->vao == vao) {
					it->second.vaos.erase(va);
					return;
				}
			}
			assert(false);
		}
		else {
			assert(false);
		}
	}

	void MeshRegistry::init() {
		ResourceID id{ "umc:XYPlane" };
		Mesh mesh;

		Vertex v1, v2, v3;

		v1.position = glm::vec3{ -0.5f, -0.5f, 0.f };
		v1.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v1.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v1.uv = glm::vec2{ 0.f, 0.f };

		v2.position = glm::vec3{ 0.5f, 0.5f, 0.f };
		v2.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v2.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v2.uv = glm::vec2{ 1.f, 1.f };

		v3.position = glm::vec3{ -0.5f, 0.5f, 0.f };
		v3.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v3.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v3.uv = glm::vec2{ 0.f, 1.f };

		mesh.appendFace(v1, v2, v3);

		v1.position = glm::vec3{ -0.5f, -0.5f, 0.f };
		v1.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v1.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v1.uv = glm::vec2{ 0.f, 0.f };

		v2.position = glm::vec3{ 0.5f, -0.5f, 0.f };
		v2.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v2.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v2.uv = glm::vec2{ 1.f, 0.f };

		v3.position = glm::vec3{ 0.5f, 0.5f, 0.f };
		v3.normal = glm::vec3{ 0.f, 0.f, 1.f };
		v3.tangent = glm::vec3{ 1.f, 0.f, 0.f };
		v3.uv = glm::vec2{ 1.f, 1.f };

		mesh.appendFace(v1, v2, v3);

		getMesh(id) = std::move(mesh);
	}

	void MeshRegistry::deinit() {
		for (auto &data : meshes) {
			if (data.second.buffer != 0) {
				gl::glDeleteBuffers(1, &(data.second.buffer));
				data.second.buffer = 0;
			}
		}
	}
}