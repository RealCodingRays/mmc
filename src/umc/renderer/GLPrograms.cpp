#include "umc\renderer\GLPrograms.h"

#include <iostream>

#include "umc\renderer\native\OpenGL.h"
#include "umc\io\ResourceManager.h"
#include "umc\io\resource\TextResource.h"

namespace umc {

	DefaultPrograms DefaultPrograms::instance;

	void DefaultPrograms::init() {
		GLShader vert, frag;
		ResourceReference<TextResource> vertCode, fragCode;

		//
		// Texture viewer
		//
		ResourceName texviewVert{ "umc:assets/shaders/Texture.vert" };
		ResourceName texviewFrag{ "umc:assets/shaders/Texture.frag" };

		vertCode = ResourceManager::get().loadResource<TextResource>(texviewVert);
		fragCode = ResourceManager::get().loadResource<TextResource>(texviewFrag);

		if (vertCode.ready() && fragCode.ready()) {
			vert.init(vertCode->get(), vertCode->size(), gl::GL_VERTEX_SHADER);
			frag.init(fragCode->get(), fragCode->size(), gl::GL_FRAGMENT_SHADER);

			texview.init();
			texview.attach(vert);
			texview.attach(frag);

			if (!texview.link()) {
				std::cout << "[ERROR][GLPrograms.cpp]: Failed to create TextureViewer program!" << std::endl;
			}
		}
		else {
			std::cout << "[ERROR][GLPrograms.cpp]: Unable to load TextureViewer code!" << std::endl;
		}

		//
		// FontRenderer
		//
		ResourceName fontrenVert{ "umc:assets/shaders/Font.vert" };
		ResourceName fontrenFrag{ "umc:assets/shaders/Font.frag" };

		vertCode = ResourceManager::get().loadResource<TextResource>(fontrenVert);
		fragCode = ResourceManager::get().loadResource<TextResource>(fontrenFrag);

		if (vertCode.ready() && fragCode.ready()) {
			vert.init(vertCode->get(), vertCode->size(), gl::GL_VERTEX_SHADER);
			frag.init(fragCode->get(), fragCode->size(), gl::GL_FRAGMENT_SHADER);

			fontren.init();
			fontren.attach(vert);
			fontren.attach(frag);

			if (!fontren.link()) {
				std::cout << "[ERROR][GLPrograms.cpp]: Failed to create FontRenderer program!" << std::endl;
			}
		}
		else {
			std::cout << "[ERROR][GLPrograms.cpp]: Unable to load FontRenderer code!" << std::endl;
		}

		//
		// Default renderer
		//
		ResourceName defrendVert{ "umc:assets/shaders/G.vert" };
		ResourceName defrendFrag{ "umc:assets/shaders/G.frag" };

		vertCode = ResourceManager::get().loadResource<TextResource>(defrendVert);
		fragCode = ResourceManager::get().loadResource<TextResource>(defrendFrag);

		if (vertCode.ready() && fragCode.ready()) {
			vert.init(vertCode->get(), vertCode->size(), gl::GL_VERTEX_SHADER);
			frag.init(fragCode->get(), fragCode->size(), gl::GL_FRAGMENT_SHADER);

			defrend.init();
			defrend.attach(vert);
			defrend.attach(frag);

			if (!defrend.link()) {
				std::cout << "[ERROR][GLPrograms.cpp]: Failed to create default renderer program!" << std::endl;
			}
		}
		else {
			std::cout << "[ERROR][GLPrograms.cpp]: Unable to load default renderer code!" << std::endl;
		}
	}

	void DefaultPrograms::deinit() {
		texview.~GLProgram();
		fontren.~GLProgram();
	}
}