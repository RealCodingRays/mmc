#include "umc\renderer\RenderManager.h"

#include <iostream>
#include <glm\gtc\matrix_transform.hpp>

#include "umc\Engine.h"
#include "umc\renderer\native\OpenGL.h"

#include "umc\renderer\GLPrograms.h"
#include "umc\renderer\ui\FontRenderer.h"
#include "umc\renderer\DebugCube.h"

#include "umc\utils\ResourcePath.h"
#include "umc\io\ResourceManager.h"
#include "umc\io\resource\TextResource.h"

#define TEXTURE_LOC_G_POSITION		0
#define TEXTURE_LOC_G_NORMAL		1
#define TEXTURE_LOC_G_ALBEDO		2
#define TEXTURE_LOC_G_MATERIAL		3
#define TEXTURE_LOC_SUN_SHADOW		10
#define TEXTURE_LOC_COLOR			0
#define TEXTURE_LOC_BLOOM			1	

namespace umc {

	RenderManager::RenderManager() {
	}

	RenderManager::~RenderManager() {
	}

	void RenderManager::init() {
		// Initialize OpenGL
		glfwMakeContextCurrent(Engine::window);

		glbinding::Binding::initialize();

		gl::glDebugMessageCallback((gl::GLDEBUGPROC)(debugMsgCallback), nullptr);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DONT_CARE, 0, nullptr, gl::GL_FALSE);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DEBUG_SEVERITY_HIGH, 0, nullptr, gl::GL_TRUE);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DEBUG_SEVERITY_MEDIUM, 0, nullptr, gl::GL_TRUE);

		gl::glClearColor(0, 0, 0, 1);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);
		glfwSwapBuffers(Engine::window);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);

		gl::glEnable(gl::GL_CULL_FACE);
		gl::glEnable(gl::GL_TEXTURE_CUBE_MAP_SEAMLESS);

		render_scale = Engine::startupConfig.getocFloat("RENDER_SCALE", 1.0f);
		Engine::window.getFramebufferSize(fb_width, fb_height);
		re_width = static_cast<F32>(fb_width) * render_scale;
		re_height = static_cast<F32>(fb_height) * render_scale;

		shadowres = Engine::startupConfig.getocInt("SHADOW_RESOLUTION", 4096);

		lumfacr = Engine::startupConfig.getocFloat("LUMINANCE_FACTOR_R", 0.299);
		lumfacg = Engine::startupConfig.getocFloat("LUMINANCE_FACTOR_G", 0.587);
		lumfacb = Engine::startupConfig.getocFloat("LUMINANCE_FACTOR_B", 0.114);

		lumfacbloextr = Engine::startupConfig.getocFloat("LUMINANCE_BLOOM_EXTRACTION_FACTOR", 1.);
		lumfacexposure = Engine::startupConfig.getocFloat("LUMINANCE_EXPOSURE_FACTOR", 1.);

		// Initialize framebuffers
		resizeScreenBuffer(fb_width, fb_height);
		resizeShadowBuffer(shadowres);

		// Initialize renderer
		const F32 quad[]{
			-1.f, -1.f, 0.f, 0.f,    1.f,  1.f, 1.f, 1.f,  -1.f, 1.f, 0.f, 1.f,
			-1.f, -1.f, 0.f, 0.f,    1.f, -1.f, 1.f, 0.f,   1.f, 1.f, 1.f, 1.f,
		};

		quadverts.init(sizeof(quad) / sizeof(F32), gl::GL_ARRAY_BUFFER, quad);

		const F32 skybox[]{
			-1.0f,  1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			-1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f
		};

		skybverts.init(sizeof(skybox) / sizeof(F32), gl::GL_ARRAY_BUFFER, skybox);

		ResourcePath vertpath{ "umc:assets/shaders/Post.vert" };
		ResourceReference<TextResource> vertcode = ResourceManager::get().loadResource<TextResource>(vertpath);
		GLShader vertshad{ vertcode->get(), static_cast<gl::GLsizei>(vertcode->size()), gl::GL_VERTEX_SHADER };

		ResourcePath fragpath;
		ResourceReference<TextResource> fragcode;
		GLShader fragshad;

		// Combine program
		fragpath = "umc:assets/shaders/Combine.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgcombine.init();
			prgcombine.attach(vertshad);
			prgcombine.attach(fragshad);
			if (!prgcombine.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Combine shader!" << std::endl;
			}

			prgcombine.setUniformValue("position", TEXTURE_LOC_G_POSITION);
			prgcombine.setUniformValue("normal", TEXTURE_LOC_G_NORMAL);
			prgcombine.setUniformValue("albedo", TEXTURE_LOC_G_ALBEDO);
			prgcombine.setUniformValue("material", TEXTURE_LOC_G_MATERIAL);
			prgcombine.setUniformValue("sun", TEXTURE_LOC_SUN_SHADOW);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Combine shader!" << std::endl;
		}

		// Bloom extract program
		fragpath = "umc:assets/shaders/BloomExtract.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgbloextr.init();
			prgbloextr.attach(vertshad);
			prgbloextr.attach(fragshad);
			if (!prgbloextr.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Bloom extraction shader!" << std::endl;
			}

			prgbloextr.setUniformValue("img", TEXTURE_LOC_COLOR);
			prgbloextr.setUniformValue("lumfac", lumfacr, lumfacg, lumfacb);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Bloom extraction shader!" << std::endl;
		}

		// Bloom pass 1
		fragpath = "umc:assets/shaders/Bloom1.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgblurga1.init();
			prgblurga1.attach(vertshad);
			prgblurga1.attach(fragshad);
			if (!prgblurga1.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Bloom pass 1 shader!" << std::endl;
			}

			prgblurga1.setUniformValue("img", TEXTURE_LOC_COLOR);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Bloom pass 1 shader!" << std::endl;
		}

		// Bloom pass 2
		fragpath = "umc:assets/shaders/Bloom2.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgblurga2.init();
			prgblurga2.attach(vertshad);
			prgblurga2.attach(fragshad);
			if (!prgblurga2.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Bloom pass 2 shader!" << std::endl;
			}

			prgblurga2.setUniformValue("img", TEXTURE_LOC_COLOR);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Bloom pass 2 shader!" << std::endl;
		}

		// Bloom combine
		fragpath = "umc:assets/shaders/BloomCombine.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgblocomb.init();
			prgblocomb.attach(vertshad);
			prgblocomb.attach(fragshad);
			if (!prgblocomb.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Bloom combine shader!" << std::endl;
			}

			prgblocomb.setUniformValue("img", TEXTURE_LOC_COLOR);
			prgblocomb.setUniformValue("bloom", TEXTURE_LOC_BLOOM);
			prgblocomb.setUniformValue("bloomlvls", 4);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Bloom combine shader!" << std::endl;
		}

		// Color mapping
		fragpath = "umc:assets/shaders/Color.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgcolrmap.init();
			prgcolrmap.attach(vertshad);
			prgcolrmap.attach(fragshad);
			if (!prgcolrmap.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Color mapping shader!" << std::endl;
			}

			prgcolrmap.setUniformValue("img", TEXTURE_LOC_COLOR);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Color mapping shader!" << std::endl;
		}

		// Rescale
		fragpath = "umc:assets/shaders/Rescale.frag";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgrescale.init();
			prgrescale.attach(vertshad);
			prgrescale.attach(fragshad);
			if (!prgrescale.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Rescale shader!" << std::endl;
			}

			prgrescale.setUniformValue("img", TEXTURE_LOC_COLOR);
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Rescale shader!" << std::endl;
		}

		// Skybox
		vertpath = "umc:assets/shaders/Skybox.vert";
		fragpath = "umc:assets/shaders/Skybox.frag";
		vertcode = ResourceManager::get().loadResource<TextResource>(vertpath);
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (vertcode && fragcode) {
			vertshad.init(vertcode->get(), vertcode->size(), gl::GL_VERTEX_SHADER);
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_FRAGMENT_SHADER);

			prgskybox.init();
			prgskybox.attach(vertshad);
			prgskybox.attach(fragshad);
			if (!prgskybox.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Skybox shader!" << std::endl;
			}
		}
		else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Skybox shader!" << std::endl;
		}

		// Luminance extraction
		fragpath = "umc:assets/shaders/Luminance.comp";
		fragcode = ResourceManager::get().loadResource<TextResource>(fragpath);
		if (fragcode) {
			fragshad.init(fragcode->get(), fragcode->size(), gl::GL_COMPUTE_SHADER);

			prgluminace.init();
			prgluminace.attach(fragshad);
			if (!prgluminace.link()) {
				std::cout << "[ERROR][RenderManager.cpp]: Unable to link Luminance extraction shader!" << std::endl;
			}

			prgluminace.setUniformValue("src", 0);
			prgluminace.setUniformValue("dest", 1);
			prgluminace.setUniformValue("samplecnt", 8);
			prgluminace.setUniformValue("lumfac", lumfacr, lumfacg, lumfacb);
		} else {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to load Luminance extraction shader!" << std::endl;
		}

		texluminance.init2D(16, 16, gl::GL_R32F, 1);

		// Post Process vertex array
		vaoquad.init();

		const gl::GLuint posloc{ 0 };
		const gl::GLuint uvloc{ 1 };

		quadverts.bind(gl::GL_ARRAY_BUFFER);
		vaoquad.setupFPointer(posloc, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 4, reinterpret_cast<void *>(sizeof(F32) * 0));
		vaoquad.setupFPointer(uvloc, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 4, reinterpret_cast<void *>(sizeof(F32) * 2));
		quadverts.unbind(gl::GL_ARRAY_BUFFER);

		prgcombine.setAttribLocation("pos", posloc);
		prgcombine.setAttribLocation("uv", uvloc);

		prgbloextr.setAttribLocation("pos", posloc);
		prgbloextr.setAttribLocation("uv", uvloc);

		prgblurga1.setAttribLocation("pos", posloc);
		prgblurga1.setAttribLocation("uv", uvloc);

		prgblurga2.setAttribLocation("pos", posloc);
		prgblurga2.setAttribLocation("uv", uvloc);

		prgblocomb.setAttribLocation("pos", posloc);
		prgblocomb.setAttribLocation("uv", uvloc);

		prgcolrmap.setAttribLocation("pos", posloc);
		prgcolrmap.setAttribLocation("uv", uvloc);

		prgrescale.setAttribLocation("pos", posloc);
		prgrescale.setAttribLocation("uv", uvloc);

		// Skybox vertex array
		vaoskyb.init();

		skybverts.bind(gl::GL_ARRAY_BUFFER);
		vaoskyb.setupFPointer(posloc, 3, gl::GL_FLOAT, gl::GL_FALSE, 0, nullptr);
		skybverts.unbind(gl::GL_ARRAY_BUFFER);

		prgskybox.setAttribLocation("pos", posloc);

		// Initialize other stuff
		DefaultPrograms::get().init();
		FontRenderer::init();

		defCam.getTransform().translate(glm::vec3{ 0.f, 0.4f, 4.f });

		debug.init();
	}

	void RenderManager::deinit() {
		debug.deinit();
		defCam.~Camera();
		FontRenderer::deinit();
		DefaultPrograms::get().deinit();

		// Destroy renderer

	}

	void RenderManager::registerRenderer(Renderer *renderer) {
		renderers.push_back(renderer);
	}

	void RenderManager::setSunDir(glm::vec3 dir) {
		sunDir = dir;
	}

	void RenderManager::debugMsgCallback(gl::GLenum source, gl::GLenum type, gl::GLuint id, gl::GLenum severity, gl::GLsizei length, const gl::GLchar *msg, const void *param) {
		std::cout << "[OPENGL] - " << source << ":" << type << "[" << severity << "] (" << id << "):\n    " << msg << std::endl;
	}

	Camera &RenderManager::getDefaultCamera() {
		return defCam;
	}

	UI32 RenderManager::getScreenWidth() {
		return fb_width;
	}

	UI32 RenderManager::getScreenHeight() {
		return fb_height;
	}

	UI32 RenderManager::getRenderWidth() {
		return re_width;
	}

	UI32 RenderManager::getRenderHeight() {
		return re_height;
	}

	F32 RenderManager::getRenderScale() {
		return render_scale;
	}

	void RenderManager::setRenderScale(F32 scale) {
		Engine::startupConfig.setFloat("RENDER_SCALE", scale);

		render_scale = scale;

		resizeRenderBuffer(static_cast<UI32>(static_cast<F32>(fb_width) * render_scale), static_cast<UI32>(static_cast<F32>(fb_height) * render_scale));
	}

	void RenderManager::renderScene(F32 dt) {
		static F32 avrgLuminance{ 1 };

		// Create sun shadow map
		gl::glEnable(gl::GL_DEPTH_TEST);
		buffshad.bind(gl::GL_FRAMEBUFFER);
		gl::glViewport(0, 0, shadowres, shadowres);
		gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
		gl::glCullFace(gl::GL_BACK);

		PipelineInfo shadowInfo;
		shadowInfo.fb_width = shadowres;
		shadowInfo.fb_height = shadowres;

		shadowInfo.world_to_camera = glm::lookAt(glm::vec3{ 0 }, sunDir, glm::vec3{ 0, 1, 0 });
		F32 shadw = std::sqrtf(3) * 16.f;
		shadowInfo.camera_to_screen = glm::mat4{ 0.f };
		shadowInfo.camera_to_screen[0][0] = 1.f / shadw;
		shadowInfo.camera_to_screen[1][1] = 1.f / shadw;
		shadowInfo.camera_to_screen[2][2] = -2.f / (2 * shadw);
		shadowInfo.camera_to_screen[3][3] = 1.f;

		shadowInfo.world_to_camera_norm = glm::transpose(glm::inverse(shadowInfo.world_to_camera));
		shadowInfo.camera_to_screen_norm = glm::transpose(glm::inverse(shadowInfo.camera_to_screen));

		for (Renderer *renderer : renderers) {
			renderer->draw(shadowInfo);
		}
		// Fill GBuffer
		buffg.bind(gl::GL_FRAMEBUFFER);
		gl::glViewport(0, 0, re_width, re_height);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);
		gl::glCullFace(gl::GL_BACK);

		PipelineInfo info;
		info.fb_width = re_width;
		info.fb_height = re_height;

		info.world_to_camera = defCam.getTransform().worldToLocal();
		info.camera_to_screen = defCam.getProjectionMatrix();

		info.world_to_camera_norm = glm::transpose(defCam.getTransform().localToWorld());
		info.camera_to_screen_norm = glm::transpose(glm::inverse(info.camera_to_screen));

		for (Renderer *renderer : renderers) {
			renderer->draw(info);
		}

		// Draw skybox
		gl::glDisable(gl::GL_DEPTH_TEST);
		buffrend.bind(gl::GL_FRAMEBUFFER);
		gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT);

		vaoskyb.bind();

		prgskybox.use();
		glm::mat4 wtoc = glm::mat4{ glm::mat3{info.world_to_camera} };
		prgskybox.setUniformMatrix4("wtoc", gl::GL_FALSE, wtoc);
		prgskybox.setUniformMatrix4("ctos", gl::GL_FALSE, info.camera_to_screen);
		prgskybox.setUniformValue("sunDir", sunDir.x, sunDir.y, sunDir.z);

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 36);

		// Combine GBuffer
		vaoquad.bind();

		prgcombine.use();

		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_POSITION);
		texgpos.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_NORMAL);
		texgnor.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_ALBEDO);
		texgalb.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_MATERIAL);
		texgmat.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_SUN_SHADOW);
		texshadow.bind();

		prgcombine.setUniformValue("sunDir", sunDir.x, sunDir.y, sunDir.z);
		prgcombine.setUniformValue("sunCol", 8 * 0.98f, 8 * 0.97f, 8 * 0.9f);
		prgcombine.setUniformMatrix4("sunwtos", gl::GL_FALSE, shadowInfo.camera_to_screen * shadowInfo.world_to_camera);

		// TODO Set lights
		prgcombine.setUniformValue("lightcnt", 0U);

		glm::vec3 camPos = defCam.getTransform().getPosition();
		prgcombine.setUniformValue("camPos", camPos.x, camPos.y, camPos.z);

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		prgcombine.unuse();

		texshadow.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_MATERIAL);
		texgmat.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_ALBEDO);
		texgalb.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_NORMAL);
		texgnor.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_G_POSITION);
		texgpos.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_COLOR);

		// Extract luminance values
		prgluminace.use();

		gl::glBindImageTexture(0, texcolor1, 0, gl::GL_FALSE, 0, gl::GL_READ_ONLY, gl::GL_RGBA16F);
		gl::glBindImageTexture(1, texluminance, 0, gl::GL_FALSE, 0, gl::GL_WRITE_ONLY, gl::GL_R32F);

		gl::glDispatchCompute(16, 16, 1);
		gl::glMemoryBarrier(gl::GL_TEXTURE_UPDATE_BARRIER_BIT);

		F32 data[16 * 16];
		gl::glGetTextureSubImage(texluminance, 0, 0, 0, 0, 16, 16, 1, gl::GL_RED, gl::GL_FLOAT, sizeof(data), data);
		F32 luminance{ 0 };
		for (F32 d : data) {
			luminance += d;
		}
		luminance = std::expf(luminance / (16.f * 16.f * (8 * 8)));
		F32 dlum = std::min(luminance - avrgLuminance, 1.0f);
		avrgLuminance += dlum * dt;
		//std::cout << "Luminance: " << luminance << std::endl;

		// Extract bloom values
		buffblur0.bind(gl::GL_FRAMEBUFFER);
		gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
		texcolor0.bind();

		prgbloextr.use();
		prgbloextr.setUniformValue("luminance", std::sqrtf(avrgLuminance));

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

		// Create downsampled blur images
		gl::glGenerateTextureMipmap(texblur0);

		// Blur bloom image
		const int blurcnt = 1;
		prgblurga1.setUniformValue("lod", 0);
		prgblurga2.setUniformValue("lod", 0);
		for (int i = 0; i < blurcnt; i++) {
			// Blur horizontal
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT1);
			texblur0.bind();

			prgblurga1.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

			// Blur vertical
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
			texblur1.bind();

			prgblurga2.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		}

		buffblur1.bind(gl::GL_FRAMEBUFFER);
		prgblurga1.setUniformValue("lod", 1);
		prgblurga2.setUniformValue("lod", 1);
		gl::glViewport(0, 0, re_width / 2, re_height / 2);
		for (int i = 0; i < blurcnt; i++) {
			// Blur horizontal
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT1);
			texblur0.bind();

			prgblurga1.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

			// Blur vertical
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
			texblur1.bind();

			prgblurga2.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		}

		buffblur2.bind(gl::GL_FRAMEBUFFER);
		prgblurga1.setUniformValue("lod", 2);
		prgblurga2.setUniformValue("lod", 2);
		gl::glViewport(0, 0, re_width / 4, re_height / 4);
		for (int i = 0; i < blurcnt; i++) {
			// Blur horizontal
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT1);
			texblur0.bind();

			prgblurga1.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

			// Blur vertical
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
			texblur1.bind();

			prgblurga2.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		}

		buffblur3.bind(gl::GL_FRAMEBUFFER);
		prgblurga1.setUniformValue("lod", 3);
		prgblurga2.setUniformValue("lod", 3);
		gl::glViewport(0, 0, re_width / 8, re_height / 8);
		for (int i = 0; i < blurcnt; i++) {
			// Blur horizontal
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT1);
			texblur0.bind();

			prgblurga1.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

			// Blur vertical
			gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
			texblur1.bind();

			prgblurga2.use();
			gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		}

		// Recombine bloom and color
		gl::glViewport(0, 0, re_width, re_height);
		buffrend.bind(gl::GL_FRAMEBUFFER);
		gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT1);

		prgblocomb.use();

		texcolor0.bind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_BLOOM);
		texblur0.bind();

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

		texblur0.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0 + TEXTURE_LOC_COLOR);

		// Do color mapping
		gl::glDrawBuffer(gl::GL_COLOR_ATTACHMENT0);
		texcolor1.bind();

		prgcolrmap.use();
		prgcolrmap.setUniformValue("avrgLum", std::sqrtf(avrgLuminance));

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);

		// Do rescale
		gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
		gl::glViewport(0, 0, fb_width, fb_height);
		
		prgrescale.use();

		texcolor0.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
		texcolor0.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
		texcolor0.bind();

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 6);
		
		texcolor0.unbind();
		texcolor0.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texcolor0.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);

		prgrescale.unuse();

		// Draw debug info
		debug.draw(info.world_to_camera, info.camera_to_screen);

		// TODO Draw UI

		glfwSwapBuffers(Engine::window);
	}

	void RenderManager::onFramebufferResize(UI32 width, UI32 height) {
		resizeScreenBuffer(width, height);
	}

	void RenderManager::resizeScreenBuffer(UI32 width, UI32 height) {
		gl::glViewport(0, 0, width, height);

		fb_width = width;
		fb_height = height;

		resizeRenderBuffer(static_cast<UI32>(static_cast<F32>(fb_width) * render_scale), static_cast<UI32>(static_cast<F32>(fb_height) * render_scale));
	}

	void RenderManager::resizeRenderBuffer(UI32 width, UI32 height) {
		defCam.configureProjectionPerspective(30.f, 0.1f, 100.f, static_cast<F32>(width) / static_cast<F32>(height));

		re_width = width;
		re_height = height;

		// Recreate pipeline
		// Create GBuffer
		rengdepth.init(gl::GL_DEPTH_COMPONENT32, width, height);

		texgpos.init2D(width, height, gl::GL_RGB32F, 1);
		texgpos.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texgpos.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texgpos.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texgpos.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texgnor.init2D(width, height, gl::GL_RGB16F, 1);
		texgnor.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texgnor.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texgnor.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texgnor.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texgalb.init2D(width, height, gl::GL_RGB8, 1);
		texgalb.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texgalb.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texgalb.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texgalb.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texgmat.init2D(width, height, gl::GL_RGB8, 1);
		texgmat.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texgmat.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texgmat.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texgmat.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		buffg.init();
		buffg.attach(gl::GL_DEPTH_ATTACHMENT, rengdepth);
		buffg.attach(gl::GL_COLOR_ATTACHMENT0, texgpos, 0);
		buffg.attach(gl::GL_COLOR_ATTACHMENT1, texgnor, 0);
		buffg.attach(gl::GL_COLOR_ATTACHMENT2, texgalb, 0);
		buffg.attach(gl::GL_COLOR_ATTACHMENT3, texgmat, 0);

		buffg.bind(gl::GL_FRAMEBUFFER);
		const gl::GLenum gdbuffs[]{ gl::GL_COLOR_ATTACHMENT0, gl::GL_COLOR_ATTACHMENT1, gl::GL_COLOR_ATTACHMENT2, gl::GL_COLOR_ATTACHMENT3 };
		gl::glDrawBuffers(4, gdbuffs);
		buffg.unbind(gl::GL_FRAMEBUFFER);

		if (!buffg.ready()) {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to create GBuffer!" << std::endl;
		}

		// Create render buffer
		texcolor0.init2D(width, height, gl::GL_RGBA16F, 1);
		texcolor0.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texcolor0.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texcolor0.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texcolor0.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texcolor1.init2D(width, height, gl::GL_RGBA16F, 1);
		texcolor1.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
		texcolor1.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
		texcolor1.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texcolor1.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		buffrend.init();
		buffrend.attach(gl::GL_COLOR_ATTACHMENT0, texcolor0, 0);
		buffrend.attach(gl::GL_COLOR_ATTACHMENT1, texcolor1, 0);

		if (!buffrend.ready()) {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to create render framebuffer!" << std::endl;
		}

		// Create bloom buffer
		texblur0.init2D(width, height, gl::GL_RGB16F, 4);
		texblur0.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
		texblur0.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
		texblur0.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texblur0.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		texblur1.init2D(width, height, gl::GL_RGB16F, 4);
		texblur1.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
		texblur1.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
		texblur1.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texblur1.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

		buffblur0.init();
		buffblur0.attach(gl::GL_COLOR_ATTACHMENT0, texblur0, 0);
		buffblur0.attach(gl::GL_COLOR_ATTACHMENT1, texblur1, 0);

		buffblur1.init();
		buffblur1.attach(gl::GL_COLOR_ATTACHMENT0, texblur0, 1);
		buffblur1.attach(gl::GL_COLOR_ATTACHMENT1, texblur1, 1);

		buffblur2.init();
		buffblur2.attach(gl::GL_COLOR_ATTACHMENT0, texblur0, 2);
		buffblur2.attach(gl::GL_COLOR_ATTACHMENT1, texblur1, 2);

		buffblur3.init();
		buffblur3.attach(gl::GL_COLOR_ATTACHMENT0, texblur0, 3);
		buffblur3.attach(gl::GL_COLOR_ATTACHMENT1, texblur1, 3);

		if (!buffblur0.ready()) {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to create bloom framebuffer!" << std::endl;
		}
	}

	void RenderManager::resizeShadowBuffer(UI32 size) {
		shadowres = size;

		// Create shadow buffer
		texshadow.init2D(size, size, gl::GL_DEPTH_COMPONENT32, 1);
		texshadow.setParameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
		texshadow.setParameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
		texshadow.setParameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
		texshadow.setParameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
		texshadow.setParameter(gl::GL_TEXTURE_COMPARE_MODE, gl::GL_COMPARE_REF_TO_TEXTURE);

		buffshad.init();
		buffshad.attach(gl::GL_DEPTH_ATTACHMENT, texshadow, 0);

		buffshad.bind(gl::GL_FRAMEBUFFER);
		gl::glDrawBuffer(gl::GL_NONE);
		gl::glReadBuffer(gl::GL_NONE);
		buffshad.unbind(gl::GL_FRAMEBUFFER);

		if (!buffshad.ready()) {
			std::cout << "[ERROR][RenderManager.cpp]: Unable to create shadow framebuffer!" << std::endl;
		}
	}
}