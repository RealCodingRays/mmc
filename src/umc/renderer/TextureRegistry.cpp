#include "umc\renderer\TextureRegistry.h"

#include "umc\renderer\native\OpenGL.h"

#include <iostream>

namespace umc {

	TextureRegistry::TextureRegistry() {
	}

	TextureRegistry::~TextureRegistry() {
	}

	void TextureRegistry::init() {
		UI8 null[]{ 0, 0, 0, 0 };
		registerTexture("umc:zero", &null, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE, 1, 1, gl::GL_RGBA8, 1);
		UI8 one[]{ 0xFF, 0xFF, 0xFF, 0xFF };
		registerTexture("umc:one", &one, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE, 1, 1, gl::GL_RGBA8, 1);
		UI8 norm[]{ 0, 0, 0xFF, 0 };
		registerTexture("umc:nonormal", &norm, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE, 1, 1, gl::GL_RGBA8, 1);
	}

	void TextureRegistry::deinit() {
	}

	void TextureRegistry::registerTexture(ResourceID name, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to reregister texture" << std::endl;
			return;
		}

		if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to register non power of 2 sized texture" << std::endl;
			return;
		}

		if (levels == 0) {
			levels = 1;
			gl::GLsizei m = std::max(width, height);
			while (m != 1) {
				m /= 2;
				levels++;
			}
		}

		texinfo &tex = textures[name];
		tex.format = format;
		tex.width = width;
		tex.height = height;
		tex.levels = levels;

		gl::glGenTextures(1, &tex.texid);
		gl::glBindTexture(gl::GL_TEXTURE_2D, tex.texid);
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, format, width, height);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
	}

	void TextureRegistry::registerTexture(ResourceID name, void *data, gl::GLenum dform, gl::GLenum dtype, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to reregister texture" << std::endl;
			return;
		}

		if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to register non power of 2 sized texture" << std::endl;
			return;
		}

		if (levels == 0) {
			levels = 1;
			gl::GLsizei m = std::max(width, height);
			while (m != 1) {
				m /= 2;
				levels++;
			}
		}

		texinfo &tex = textures[name];
		tex.format = format;
		tex.width = width;
		tex.height = height;
		tex.levels = levels;

		gl::glGenTextures(1, &tex.texid);
		gl::glBindTexture(gl::GL_TEXTURE_2D, tex.texid);
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, format, width, height);
		gl::glTexSubImage2D(gl::GL_TEXTURE_2D, 0, 0, 0, width, height, dform, dtype, data);
		gl::glGenerateMipmap(gl::GL_TEXTURE_2D);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
	}

	void TextureRegistry::registerTexture(ResourceID name, const ImageResource &img, gl::GLsizei levels) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to reregister texture" << std::endl;
			return;
		}
		
		gl::GLsizei width = static_cast<gl::GLsizei>(img.getWidth());
		gl::GLsizei height = static_cast<gl::GLsizei>(img.getHeight());

		if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to register non power of 2 sized texture" << std::endl;
			return;
		}

		if (levels == 0) {
			levels = 1;
			gl::GLsizei m = std::max(width, height);
			while (m != 1) {
				m /= 2;
				levels++;
			}
		}

		texinfo &tex = textures[name];
		tex.format = infoToType(img.getBitDepth(), img.getNumChannels());
		tex.width = width;
		tex.height = height;
		tex.levels = levels;

		gl::glGenTextures(1, &tex.texid);
		gl::glBindTexture(gl::GL_TEXTURE_2D, tex.texid);
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, tex.format, width, height);
		gl::glTexSubImage2D(gl::GL_TEXTURE_2D, 0, 0, 0, width, height, channelsToType(img.getNumChannels()), bitDepthToType(img.getBitDepth()), img.get());
		gl::glGenerateMipmap(gl::GL_TEXTURE_2D);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
	}

	void TextureRegistry::registerTexture(ResourceID name, const ImageResource &img, gl::GLenum format, gl::GLsizei levels) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to reregister texture" << std::endl;
			return;
		}

		gl::GLsizei width = static_cast<gl::GLsizei>(img.getWidth());
		gl::GLsizei height = static_cast<gl::GLsizei>(img.getHeight());

		if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to register non power of 2 sized texture" << std::endl;
			return;
		}

		if (levels == 0) {
			levels = 1;
			gl::GLsizei m = std::max(width, height);
			while (m != 1) {
				m /= 2;
				levels++;
			}
		}

		texinfo &tex = textures[name];
		tex.format = format;
		tex.width = width;
		tex.height = height;
		tex.levels = levels;

		gl::glGenTextures(1, &tex.texid);
		gl::glBindTexture(gl::GL_TEXTURE_2D, tex.texid);
		gl::glTexStorage2D(gl::GL_TEXTURE_2D, levels, format, width, height);
		gl::glTexSubImage2D(gl::GL_TEXTURE_2D, 0, 0, 0, width, height, channelsToType(img.getNumChannels()), bitDepthToType(img.getBitDepth()), img.get());
		gl::glGenerateMipmap(gl::GL_TEXTURE_2D);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
	}

	void TextureRegistry::registerTexture(ResourceID name, gl::GLuint texid, gl::GLsizei width, gl::GLsizei height, gl::GLenum format, gl::GLsizei levels) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to reregister texture" << std::endl;
			return;
		}

		if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
			std::cout << "[ERROR][TextureRegistry.cpp]: Tried to register non power of 2 sized texture" << std::endl;
			return;
		}

		texinfo &tex = textures[name];
		tex.format = format;
		tex.width = width;
		tex.height = height;
		tex.levels = levels;
		tex.texid = texid;
	}

	void TextureRegistry::removeTexture(ResourceID name, bool del) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			if (it->second.texid > 0) {
				gl::glDeleteTextures(1, &(it->second.texid));
			}

			textures.erase(it);
		}
	}

	void TextureRegistry::getTextureSize(ResourceID name, gl::GLsizei &width, gl::GLsizei &height) {
		auto it = textures.find(name);
		
		if (it != textures.end()) {
			width = it->second.width;
			height = it->second.height;
		}
	}

	gl::GLsizei TextureRegistry::getTextureLevels(ResourceID name) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			return it->second.levels;
		}
		return 0;
	}

	gl::GLuint TextureRegistry::getGLTexture(ResourceID name) {
		auto it = textures.find(name);

		if (it != textures.end()) {
			return it->second.texid;
		}
		return 0;
	}

	void TextureRegistry::bindTexture(ResourceID name, gl::GLint target) {
		auto it = textures.find(name);

		gl::glActiveTexture(gl::GL_TEXTURE0 + target);
		if (it != textures.end()) {
			gl::glBindTexture(gl::GL_TEXTURE_2D, it->second.texid);
		}
		else {
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
		}
	}

	TextureGroup *TextureRegistry::registerTextureGroup(ResourceID name) {
		return &(groups[name]);
	}

	TextureGroup *TextureRegistry::getTextureGroup(ResourceID name) {
		auto it = groups.find(name);

		if (it != groups.end()) {
			return &(it->second);
		}
		return nullptr;
	}

	void TextureRegistry::bindTextureGroup(ResourceID name, gl::GLint alloc, gl::GLint noloc, gl::GLint roloc, gl::GLint aoloc, gl::GLint meloc) {
		auto it = groups.find(name);

		if (it != groups.end()) {
			bindTexture(it->second.albedo, alloc);
			bindTexture(it->second.normal, noloc);
			bindTexture(it->second.roughness, roloc);
			bindTexture(it->second.ao, aoloc);
			bindTexture(it->second.metallic, meloc);
		}
		else {
			gl::glActiveTexture(gl::GL_TEXTURE0 + alloc);
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
			gl::glActiveTexture(gl::GL_TEXTURE0 + noloc);
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
			gl::glActiveTexture(gl::GL_TEXTURE0 + roloc);
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
			gl::glActiveTexture(gl::GL_TEXTURE0 + aoloc);
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
			gl::glActiveTexture(gl::GL_TEXTURE0 + meloc);
			gl::glBindTexture(gl::GL_TEXTURE_2D, errortex);
		}
	}

	gl::GLenum TextureRegistry::bitDepthToType(int depth) {
		switch (depth) {
		case 8:
			return gl::GL_UNSIGNED_BYTE;
		case 16:
			return gl::GL_UNSIGNED_SHORT;
		case 32:
			return gl::GL_UNSIGNED_INT;
		default:
			std::cout << "[ERROR][TextureRegistry.cpp]: Unknown bit depth!" << std::endl;
			return gl::GL_UNSIGNED_BYTE;
		}
	}

	gl::GLenum TextureRegistry::channelsToType(int chnls) {
		switch (chnls) {
		case 1:
			return gl::GL_RED;
		case 2:
			return gl::GL_RG;
		case 3:
			return gl::GL_RGB;
		case 4:
			return gl::GL_RGBA;
		default:
			std::cout << "[ERROR][TextureRegistry.cpp]: Unknown channel count!" << std::endl;
			return gl::GL_RED;
		}
	}
	gl::GLenum TextureRegistry::infoToType(int depth, int chnls) {
		switch (chnls) {
		case 1:
			switch (depth) {
			case 8:
				return gl::GL_R8;
			case 16:
				return gl::GL_R16;
			case 32:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unsupported combination R32!" << std::endl;
				return gl::GL_R8;
			default:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unknown bit depth!" << std::endl;
				return gl::GL_R8;
			}
		case 2:
			switch (depth) {
			case 8:
				return gl::GL_RG8;
			case 16:
				return gl::GL_RG16;
			case 32:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unsupported combination RG32!" << std::endl;
				return gl::GL_RG8;
			default:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unknown bit depth!" << std::endl;
				return gl::GL_RG8;
			}
		case 3:
			switch (depth) {
			case 8:
				return gl::GL_RGB8;
			case 16:
				return gl::GL_RGB16;
			case 32:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unsupported combination RGB32!" << std::endl;
				return gl::GL_RGB8;
			default:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unknown bit depth!" << std::endl;
				return gl::GL_RGB8;
			}
		case 4:
			switch (depth) {
			case 8:
				return gl::GL_RGBA8;
			case 16:
				return gl::GL_RGBA16;
			case 32:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unsupported combination RGBA32!" << std::endl;
				return gl::GL_RGBA8;
			default:
				std::cout << "[ERROR][TextureRegistry.cpp]: Unknown bit depth!" << std::endl;
				return gl::GL_RGBA8;
			}
		default:
			std::cout << "[ERROR][TextureRegistry.cpp]: Unknown channel count!" << std::endl;
			return gl::GL_R8;
		}
	}
}