#include "umc\renderer\ui\FontRenderer.h"

#include "umc\renderer\native\OpenGL.h"
#include "umc\renderer\GLPrograms.h"
#include <limits>

#include <iostream>

namespace umc {

	Font::Font() {
	}

	Font::Font(ResourceReference<ImageResource> &texture, UI32 size, UI32 baseoff) : texture{ texture }, size{ size }, baseoffset{ static_cast<F32>(baseoff) / static_cast<F32>(size) } {
	}

	Font::~Font() {
	}

	void Font::setTexture(ResourceReference<ImageResource> &texture, UI32 size, UI32 baseoff) {
		this->texture = texture;
		this->size = size;
		this->baseoffset = static_cast<F32>(baseoff) / static_cast<F32>(size);
	}

	void Font::addChar(UI32 chr, UI32 x, UI32 y, UI32 width, UI32 height, I32 xoff, I32 yoff, UI32 advance, UI8 chnl) {
		CharDims &dims = characters[chr];

		dims.chr = chr;
		dims.x = x;
		dims.y = y;
		dims.width = width;
		dims.height = height;
		dims.xoffset = xoff;
		dims.yoffset = yoff;
		dims.advance = advance;
		dims.chnl = chnl;
	}

	const Font::CharDims *Font::getChar(UI32 chr) const {
		auto it = characters.find(chr);

		if (it != characters.end()) {
			return &(it->second);
		}
		else {
			return nullptr;
		}
	}

	UI32 Font::getBaseSize() const {
		return size;
	}

	F32 Font::getBaseOffset() const {
		return baseoffset;
	}

	ImageResource *Font::getTexture() {
		if (texture.ready()) {
			return texture.get();
		}
		else {
			return nullptr;
		}
	}


	GLBuffer FontRenderer::quad{};
	GLVertexArray FontRenderer::vao{};
	int FontRenderer::p_base_size;
	int FontRenderer::p_base_pos;
	int FontRenderer::p_uv_scale;
	int FontRenderer::p_frag_color;
	int FontRenderer::p_font;
	int FontRenderer::p_uvs;
	int FontRenderer::p_dim;

	FontRenderer::FontRenderer() {
	}

	FontRenderer::FontRenderer(Font &font) : font{ &font } {
		texture.init(font.getTexture(), 1);
	}

	FontRenderer::~FontRenderer() {
	}

	void FontRenderer::setFont(Font &font) {
		this->font = &font;
		texture.init(font.getTexture(), 1);
	}

	void FontRenderer::setText(const char *text) {
		size_t length = strlen(text);

		size_t alen = 2;
		while (alen < length) {
			alen *= 2;
		}

		F32 *dims_a = new F32[alen * 4];
		F32 *uvs_a = new F32[alen * 3];

		I16 xpos = 0;
		for (size_t i = 0; i < length; i++) {
			const Font::CharDims *dim = font->getChar(static_cast<unsigned char>(text[i]));
			
			uvs_a[(i * 3)] = dim->x;
			uvs_a[(i * 3) + 1] = dim->y;
			uvs_a[(i * 3) + 2] = dim->chnl;

			dims_a[(i * 4)] = xpos + dim->xoffset;
			dims_a[(i * 4) + 1] = 63 - (dim->yoffset + dim->height);
			dims_a[(i * 4) + 2] = dim->width;
			dims_a[(i * 4) + 3] = dim->height;

			xpos += dim->advance;
		}

		dims.init1D(alen, gl::GL_RGBA32F, 1);
		dims.setData1D(0, 0, alen, gl::GL_FLOAT, gl::GL_RGBA, dims_a);

		uvs.init1D(alen, gl::GL_RGB32F, 1);
		uvs.setData1D(0, 0, alen, gl::GL_FLOAT, gl::GL_RGB, uvs_a);

		tlen = length;

		delete[] dims_a;
		delete[] uvs_a;
	}

	void FontRenderer::setPosition(F32 x, F32 y) {
		this->x = x;
		this->y = y;
	}

	void FontRenderer::setSize(F32 size) {
		this->size = size;
	}

	void FontRenderer::setColor(F32 r, F32 g, F32 b, F32 a) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	void FontRenderer::render(UI32 width, UI32 height) {
		GLProgram &prog = DefaultPrograms::get().fontRenderer();

		prog.use();
		vao.bind();

		gl::glActiveTexture(gl::GL_TEXTURE0);
		texture.bind();
		gl::glActiveTexture(gl::GL_TEXTURE1);
		uvs.bind();
		gl::glActiveTexture(gl::GL_TEXTURE2);
		dims.bind();

		prog.setUniformValue(p_base_size, (2.f / 63.f) * size, (2.f / 63.f) * size * (static_cast<F32>(width) / static_cast<F32>(height)));
		prog.setUniformValue(p_base_pos, (x * 2.f) - 1.f, (y * 2.f) - 1.f);
		prog.setUniformValue(p_uv_scale, 1.f / static_cast<F32>(512));
		prog.setUniformValue(p_frag_color, r, g, b, a);
		prog.setUniformValue(p_font, static_cast<gl::GLint>(0));
		prog.setUniformValue(p_uvs, static_cast<gl::GLint>(1));
		prog.setUniformValue(p_dim, static_cast<gl::GLint>(2));

		gl::glDrawArraysInstanced(gl::GL_TRIANGLES, 0, 6, tlen);

		gl::glActiveTexture(gl::GL_TEXTURE0);
		texture.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE1);
		uvs.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE2);
		dims.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0);

		vao.unbind();
		prog.unuse();
	}

	void FontRenderer::init() {
		F32 verts[]{
			0, 0,  0, 1,  1, 1,
			0, 0,  1, 1,  1, 0,
		};

		quad.init(12, gl::GL_ARRAY_BUFFER, verts);
		if (!quad.isReady()) {
			std::cout << "[ERROR][FontRenderer.cpp]: Failed to initialize quad buffer!" << std::endl;
			return;
		}

		GLProgram &prog = DefaultPrograms::get().fontRenderer();

		p_base_size = prog.getUniformLocation("base_size");
		p_base_pos = prog.getUniformLocation("base_pos");
		p_uv_scale = prog.getUniformLocation("uv_scale");
		p_frag_color = prog.getUniformLocation("frag_color");
		p_font = prog.getUniformLocation("font");
		p_uvs = prog.getUniformLocation("uvs");
		p_dim = prog.getUniformLocation("dim");

		vao.init();
		gl::GLint pos = prog.getAttribLocation("pos");
		if(pos < 0) {
			std::cout << "[ERROR][FontRenderer.cpp]: Failed to find position attribute!" << std::endl;
			return;
		}
		quad.bind();

		vao.enableLocation(pos);
		vao.setupFPointer(pos, 2, gl::GL_FLOAT, gl::GL_FALSE, 0, nullptr);

		quad.unbind();
	}

	void FontRenderer::deinit() {
		quad.~GLBuffer();
		vao.~GLVertexArray();
	}
}