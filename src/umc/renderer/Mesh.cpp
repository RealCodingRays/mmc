#include "umc\renderer\Mesh.h"

namespace umc {

	glm::vec3 Vertex::generateNormal(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		return glm::normalize(glm::cross(v2.position - v1.position, v3.position - v1.position));
	}

	glm::vec3 Vertex::generateTangent(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		glm::vec3 e1 = v2.position - v1.position;
		glm::vec3 e2 = v3.position - v1.position;
		glm::vec2 u1 = v2.uv - v1.uv;
		glm::vec2 u2 = v3.uv - v1.uv;

		F32 f = 1.f / (u1.x * u2.y - u2.x * u2.y);

		glm::vec3 tangent{};
		tangent.x = f * (u2.y * e1.x - u1.y * e1.x);
		tangent.y = f * (u2.y * e1.y - u1.y * e2.y);
		tangent.z = f * (u2.y * e1.z - u1.y * e2.z);

		return glm::normalize(tangent);
	}

	Mesh::Mesh() {
	}

	Mesh::Mesh(size_t facecnt) : mem{ facecnt * 3 }, size{ 0 } {
		vertecies = new Vertex[mem];
	}

	Mesh::~Mesh() {
		if (vertecies != nullptr) {
			delete[] vertecies;
			vertecies = nullptr;
		}
	}

	Mesh::Mesh(const Mesh &other) {
		if (other.vertecies != nullptr) {
			mem = other.size;
			size = other.size;

			vertecies = new Vertex[mem];
			memcpy_s(vertecies, size * sizeof(Vertex), other.vertecies, other.size * sizeof(Vertex));
		}
	}

	Mesh::Mesh(Mesh &&other) : vertecies{ other.vertecies }, mem{ other.mem }, size{ other.size } {
		other.vertecies = nullptr;
		other.mem = 0;
		other.size = 0;
	}

	Mesh &Mesh::operator =(const Mesh &other) {
		if (vertecies != nullptr) {
			delete[] vertecies;
		}

		if (other.vertecies != nullptr) {
			mem = other.size;
			size = other.size;

			vertecies = new Vertex[mem];
			memcpy_s(vertecies, size * sizeof(Vertex), other.vertecies, other.size * sizeof(Vertex));
		}
		else {
			vertecies = nullptr;
			mem = 0;
			size = 0;
		}

		return *this;
	}

	Mesh &Mesh::operator =(Mesh &&other) {
		if (vertecies != nullptr) {
			delete[] vertecies;
		}

		mem = other.mem;
		size = other.size;
		vertecies = other.vertecies;

		other.mem = 0;
		other.size = 0;
		other.vertecies = nullptr;

		return *this;
	}

	void Mesh::appendFace(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		if (size + 3 > mem) {
			resize(mem + 48);
		}

		vertecies[size] = v1;
		size++;
		vertecies[size] = v2;
		size++;
		vertecies[size] = v3;
		size++;
	}

	void Mesh::appendFaceCT(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		glm::vec3 tangent = Vertex::generateTangent(v1, v2, v3);

		if (size + 3 > mem) {
			resize(mem + 48);
		}

		vertecies[size] = v1;
		vertecies[size].tangent = tangent;
		size++;
		vertecies[size] = v2;
		vertecies[size].tangent = tangent;
		size++;
		vertecies[size] = v3;
		vertecies[size].tangent = tangent;
		size++;
	}

	void Mesh::appendFaceCN(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		glm::vec3 normal = Vertex::generateNormal(v1, v2, v3);

		if (size + 3 > mem) {
			resize(mem + 48);
		}

		vertecies[size] = v1;
		vertecies[size].normal = normal;
		size++;
		vertecies[size] = v2;
		vertecies[size].normal = normal;
		size++;
		vertecies[size] = v3;
		vertecies[size].normal = normal;
		size++;
	}

	void Mesh::appendFaceCNT(const Vertex &v1, const Vertex &v2, const Vertex &v3) {
		glm::vec3 normal = Vertex::generateNormal(v1, v2, v3);
		glm::vec3 tangent = Vertex::generateTangent(v1, v2, v3);

		if (size + 3 > mem) {
			resize(mem + 48);
		}

		vertecies[size] = v1;
		vertecies[size].normal = normal;
		vertecies[size].tangent = tangent;
		size++;
		vertecies[size] = v2;
		vertecies[size].normal = normal;
		vertecies[size].tangent = tangent;
		size++;
		vertecies[size] = v3;
		vertecies[size].normal = normal;
		vertecies[size].tangent = tangent;
		size++;
	}

	Vertex &Mesh::getVertex(size_t face, int vertex) {
		return vertecies[face * 3 + vertex];
	}

	Vertex &Mesh::getVertex(size_t vertex) {
		return vertecies[vertex];
	}

	Vertex *Mesh::getVertecies() {
		return vertecies;
	}

	size_t Mesh::getNumVertecies() {
		return size;
	}

	void Mesh::generateNormals() {
		for (int i = 0; i < size; i += 3) {
			glm::vec3 normal = Vertex::generateNormal(vertecies[i], vertecies[i + 1], vertecies[i + 2]);

			vertecies[i].normal = normal;
			vertecies[i + 1].normal = normal;
			vertecies[i + 2].normal = normal;
		}
	}

	void Mesh::generateTangents() {
		for (int i = 0; i < size; i += 3) {
			glm::vec3 tangent = Vertex::generateTangent(vertecies[i], vertecies[i + 1], vertecies[i + 2]);

			vertecies[i].tangent = tangent;
			vertecies[i + 1].tangent = tangent;
			vertecies[i + 2].tangent = tangent;
		}
	}

	AABB Mesh::generateAABB() {
		if (size > 0) {
			glm::vec3 min{ vertecies[0].position };
			glm::vec3 max{ vertecies[0].position };

			for (int i = 1; i < size; i++) {
				min = glm::min(min, vertecies[i].position);
				max = glm::max(max, vertecies[i].position);
			}

			return AABB{ min, max };
		}
		else {
			return AABB{};
		}
	}

	void Mesh::resize(size_t nsize) {
		Vertex *ndata = new Vertex[nsize];

		memcpy_s(ndata, nsize * sizeof(Vertex), vertecies, size * sizeof(Vertex));

		delete[] vertecies;
		
		vertecies = ndata;
		mem = nsize;
		size = size > nsize ? nsize : size;
	}
}