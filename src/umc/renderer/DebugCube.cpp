#include "umc\renderer\DebugCube.h"

#include "umc\renderer\native\OpenGL.h"
#include "umc\renderer\GLPrograms.h"

namespace umc {

	GLBuffer DebugCube::vertecies;
	GLVertexArray DebugCube::vao;

	void DebugCube::draw(const PipelineInfo &pipe) {
		DefaultPrograms::get().defaultRenderer().use();
		vao.bind();

		glm::mat4 mtow{ trans.localToWorld() };
		glm::mat4 mtown{ glm::transpose(trans.worldToLocal()) };

		gl::glActiveTexture(gl::GL_TEXTURE0);
		albedo.bind();
		gl::glActiveTexture(gl::GL_TEXTURE1);
		normal.bind();
		gl::glActiveTexture(gl::GL_TEXTURE2);
		roughn.bind();

		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("mtow", gl::GL_FALSE, mtow);
		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("wtoc", gl::GL_FALSE, pipe.world_to_camera);
		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("ctos", gl::GL_FALSE, pipe.camera_to_screen);
		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("mtown", gl::GL_FALSE, mtown);
		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("wtocn", gl::GL_FALSE, pipe.world_to_camera_norm);
		DefaultPrograms::get().defaultRenderer().setUniformMatrix4("ctosn", gl::GL_FALSE, pipe.camera_to_screen_norm);

		DefaultPrograms::get().defaultRenderer().setUniformValue("albedo", 0);
		DefaultPrograms::get().defaultRenderer().setUniformValue("normal", 1);
		DefaultPrograms::get().defaultRenderer().setUniformValue("metalic", 2);

		gl::glDrawArrays(gl::GL_TRIANGLES, 0, 36);

		roughn.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE1);
		normal.unbind();
		gl::glActiveTexture(gl::GL_TEXTURE0);
		albedo.unbind();

		vao.unbind();
		DefaultPrograms::get().defaultRenderer().unuse();
	}

	Transform &DebugCube::getTransform() {
		return trans;
	}

	void DebugCube::init() {
		const F32 verts[]{
			// --position--        --normal--          --uv--       --tangent--
			// Front face
			-1.f, -1.f,  1.f,    0.f,  0.f,  1.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  0.f,  1.f,    1.f,  0.f,    1.f,  0.f,  0.f,
			-1.f,  1.f,  1.f,    0.f,  0.f,  1.f,    0.f,  0.f,    1.f,  0.f,  0.f,

			-1.f, -1.f,  1.f,    0.f,  0.f,  1.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f, -1.f,  1.f,    0.f,  0.f,  1.f,    1.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  0.f,  1.f,    1.f,  0.f,    1.f,  0.f,  0.f,

			// Right face
			 1.f, -1.f,  1.f,    1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f, -1.f,
			 1.f,  1.f, -1.f,    1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f, -1.f,
			 1.f,  1.f,  1.f,    1.f,  0.f,  0.f,    0.f,  0.f,    0.f,  0.f, -1.f,

			 1.f, -1.f,  1.f,    1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f, -1.f,
			 1.f, -1.f, -1.f,    1.f,  0.f,  0.f,    1.f,  1.f,    0.f,  0.f, -1.f,
			 1.f,  1.f, -1.f,    1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f, -1.f,

			// Back face
			 1.f, -1.f, -1.f,    0.f,  0.f, -1.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			-1.f,  1.f, -1.f,    0.f,  0.f, -1.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
			 1.f,  1.f, -1.f,    0.f,  0.f, -1.f,    0.f,  0.f,   -1.f,  0.f,  0.f,

			 1.f, -1.f, -1.f,    0.f,  0.f, -1.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			-1.f, -1.f, -1.f,    0.f,  0.f, -1.f,    1.f,  1.f,   -1.f,  0.f,  0.f,
			-1.f,  1.f, -1.f,    0.f,  0.f, -1.f,    1.f,  0.f,   -1.f,  0.f,  0.f,

			// Left face
			-1.f, -1.f, -1.f,   -1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f,  1.f,
			-1.f,  1.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f,  1.f,
			-1.f,  1.f, -1.f,   -1.f,  0.f,  0.f,    0.f,  0.f,    0.f,  0.f,  1.f,

			-1.f, -1.f, -1.f,   -1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f,  1.f,
			-1.f, -1.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  1.f,    0.f,  0.f,  1.f,
			-1.f,  1.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f,  1.f,

			// Top face
			-1.f,  1.f,  1.f,    0.f,  1.f,  0.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f, -1.f,    0.f,  1.f,  0.f,    1.f,  0.f,    1.f,  0.f,  0.f,
			-1.f,  1.f, -1.f,    0.f,  1.f,  0.f,    0.f,  0.f,    1.f,  0.f,  0.f,

			-1.f,  1.f,  1.f,    0.f,  1.f,  0.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  1.f,  0.f,    1.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f, -1.f,    0.f,  1.f,  0.f,    1.f,  0.f,    1.f,  0.f,  0.f,

			// Bottom face
			-1.f, -1.f, -1.f,    0.f, -1.f,  0.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f, -1.f,  1.f,    0.f, -1.f,  0.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
			-1.f, -1.f,  1.f,    0.f, -1.f,  0.f,    0.f,  0.f,   -1.f,  0.f,  0.f,

			-1.f, -1.f, -1.f,    0.f, -1.f,  0.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f, -1.f, -1.f,    0.f, -1.f,  0.f,    1.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f, -1.f,  1.f,    0.f, -1.f,  0.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
		};

		vertecies.init(sizeof(verts) / sizeof(F32), gl::GL_ARRAY_BUFFER, verts);

		vao.init();
		GLProgram &prog = DefaultPrograms::get().defaultRenderer();
		vertecies.bind();

		gl::GLint loc;
		loc = prog.getAttribLocation("pos");
		if (loc >= 0) {
			vao.enableLocation(loc);
			vao.setupFPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, 11 * sizeof(F32), nullptr);
		}

		loc = prog.getAttribLocation("normal");
		if (loc >= 0) {
			vao.enableLocation(loc);
			vao.setupFPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, 11 * sizeof(F32), reinterpret_cast<void *>(sizeof(F32) * 3));
		}

		loc = prog.getAttribLocation("uv");
		if (loc >= 0) {
			vao.enableLocation(loc);
			vao.setupFPointer(loc, 2, gl::GL_FLOAT, gl::GL_FALSE, 11 * sizeof(F32), reinterpret_cast<void *>(sizeof(F32) * 6));
		}

		loc = prog.getAttribLocation("tangent");
		if (loc >= 0) {
			vao.enableLocation(loc);
			vao.setupFPointer(loc, 2, gl::GL_FLOAT, gl::GL_FALSE, 11 * sizeof(F32), reinterpret_cast<void *>(sizeof(F32) * 8));
		}

		vertecies.unbind();
	}

	void DebugCube::deint() {
		vertecies.~GLBuffer();
		vao.~GLVertexArray();
	}
}