#include "umc\world\BlockRegistry.h"

#include <iostream>

namespace umc {

	BlockRegistry::BlockRegistry() {
		registerBlock(ResourceName{ "umc:Air" });
	}

	BlockRegistry::~BlockRegistry() {
	}

	BlockID BlockRegistry::registerBlock(ResourceName name) {
		auto it = names.find(name);
		
		if (it != names.end()) {
			std::cout << "[WARNING][BlockRegistry.cpp]: Tried to re register block " << name.getPackage() << ":" << name.getName() << std::endl;
			return it->second;
		}

		BlockConfig nblock;
		blocks.push_back(nblock);

		BlockID nid = static_cast<BlockID>(blocks.size() - 1);
		names[name] = nid;
		return nid;
	}

	BlockID BlockRegistry::getBlockID(ResourceID name) {
		auto it = names.find(name);

		if (it != names.end()) {
			return it->second;
		}
		else {
			return 0;
		}
	}

	void BlockRegistry::setBlockFactory(BlockID id, BlockFactory *factory) {
		blocks.at(id).factory = factory;
	}

	void BlockRegistry::setBlockTexture(BlockID id, UI8 side, ResourceID texture) {
		blocks.at(id).textures[side] = texture;
	}

	void BlockRegistry::setBlockTexture(BlockID id, ResourceID texture) {
		BlockConfig &cfg = blocks.at(id);

		for (ResourceID &tex : cfg.textures) {
			tex = texture;
		}
	}

	void BlockRegistry::setBlockTexture(BlockID id, ResourceID side, ResourceID up, ResourceID down) {
		BlockConfig &cfg = blocks.at(id);

		cfg.textures[BlockSide::FRONT] = side;
		cfg.textures[BlockSide::RIGHT] = side;
		cfg.textures[BlockSide::BACK] = side;
		cfg.textures[BlockSide::LEFT] = side;
		cfg.textures[BlockSide::TOP] = up;
		cfg.textures[BlockSide::BOTTOM] = down;
	}

	void BlockRegistry::setBlockTexture(BlockID id, ResourceID front, ResourceID right, ResourceID back, ResourceID left, ResourceID up, ResourceID down) {
		BlockConfig &cfg = blocks.at(id);

		cfg.textures[BlockSide::FRONT] = front;
		cfg.textures[BlockSide::RIGHT] = right;
		cfg.textures[BlockSide::BACK] = back;
		cfg.textures[BlockSide::LEFT] = left;
		cfg.textures[BlockSide::TOP] = up;
		cfg.textures[BlockSide::BOTTOM] = down;
	}

	ResourceID BlockRegistry::getBlockTexture(BlockID id, UI8 side) {
		BlockConfig &cfg = blocks[id];

		return cfg.textures[side];
	}

	void BlockRegistry::createBlock(BlockID id, Block &block) {
		new (&block) Block{};

		BlockConfig &cfg = blocks[id];
		
		if (cfg.factory) {
			cfg.factory->initBlock(block);
		}
	}

	Block BlockRegistry::createBlock(BlockID id) {
		Block block;

		BlockConfig &cfg = blocks[id];

		if (cfg.factory) {
			cfg.factory->initBlock(block);
		}

		return block;
	}
}