#include "umc\world\Transform.h"

#include <iostream>
#include <glm\gtx\string_cast.hpp>

namespace umc{

	//
	// Transform
	//
	Transform::Transform() {
	}

	Transform::Transform(const glm::quat &rotation, const glm::vec3 &scale, const glm::vec3 &position) : rotation{ rotation }, fscale{ scale }, position{ position } {
	}

	Transform::~Transform() {
		if (parent) {
			parent->removeChild(this);
			parent = nullptr;
		}
		for (Transform *child : children) {
			child->removeParent();
		}
		children.clear();
	}

	Transform::Transform(const Transform &other) : rotation{ other.rotation }, fscale{ other.fscale }, position{ other.position } {
	}

	Transform::Transform(Transform &&other) : rotation{ other.rotation }, fscale{ other.fscale }, position{ other.position } {
	}

	Transform &Transform::operator =(const Transform &other) {
		rotation = other.rotation;
		fscale = other.fscale;
		position = other.position;

		return *this;
	}

	Transform &Transform::operator =(Transform &&other) {
		rotation = other.rotation;
		fscale = other.fscale;
		position = other.position;

		return *this;
	}

	void Transform::rotate(F32 rad, glm::vec3 axis) {
		rotation = glm::rotate(rotation, rad, axis);
	}

	void Transform::rotateX(F32 rad) {
		rotation = glm::rotate(rotation, rad, glm::vec3{ 1.f, 0.f, 0.f });
	}

	void Transform::rotateY(F32 rad) {
		rotation = glm::rotate(rotation, rad, glm::vec3{ 0.f, 1.f, 0.f });
	}

	void Transform::rotateZ(F32 rad) {
		rotation = glm::rotate(rotation, rad, glm::vec3{ 0.f, 0.f, 1.f });
	}

	glm::quat Transform::getRotation() const {
		return rotation;
	}

	void Transform::setRotation(glm::quat rot) {
		rotation = rot;
	}

	void Transform::scale(glm::vec3 factor) {
		fscale *= factor;
	}

	void Transform::scaleX(F32 factor) {
		fscale.x *= factor;
	}

	void Transform::scaleY(F32 factor) {
		fscale.y *= factor;
	}

	void Transform::scaleZ(F32 factor) {
		fscale.z *= factor;
	}

	glm::vec3 Transform::getScale() const {
		return fscale;
	}

	void Transform::setScale(glm::vec3 scale) {
		fscale = scale;
	}

	void Transform::translate(glm::vec3 trans) {
		position += trans;
	}

	void Transform::translateForward(glm::vec3 trans) {
		glm::vec4 trans4{ trans, 0.f };

		trans4 = trans4 * worldToLocal();

		translate(glm::vec3{ trans4 });
	}

	glm::vec3 Transform::getPosition() const {
		return position;
	}

	void Transform::setPosition(glm::vec3 pos) {
		position = pos;
	}

	void Transform::setParent(Transform *parent) {
		if (this->parent == parent) {
			return;
		}

		if (this->parent) {
			this->parent->removeChild(this);
		}

		this->parent = parent;
		if (this->parent) {
			this->parent->addChild(this);
		}
	}

	glm::mat4 Transform::worldToLocal() const {
		return glm::inverse(localToWorld());
	}

	glm::mat4 Transform::localToWorld() const {
		glm::mat4 rot{ glm::mat4_cast(rotation) };

		glm::mat4 scale{ 1.f };

		scale[0][0] = fscale.x;
		scale[1][1] = fscale.y;
		scale[2][2] = fscale.z;

		glm::mat4 pos{ 1.f };

		pos[3][0] = position.x;
		pos[3][1] = position.y;
		pos[3][2] = position.z;

		return scale * rot * pos;
	}

	void Transform::addChild(Transform *child) {
		for (const Transform *chil : children) {
			if (chil == child) {
				return;
			}
		}
		children.push_back(child);
	}

	void Transform::removeChild(Transform *child) {
		for (auto it = children.begin(); it != children.end(); it++) {
			if (*it == child) {
				children.erase(it);
				return;
			}
		}
	}

	void Transform::removeParent() {
		parent = nullptr;
	}

	//
	// CameraTransform
	//
	CameraTransform::CameraTransform() {
	}

	CameraTransform::CameraTransform(F32 vlim) : vlim{ vlim } {
	}

	CameraTransform::CameraTransform(const CameraTransform &other) : vlim{ other.vlim }, rotv{ other.rotv }, roth{ other.roth }, position{ other.position } {
	}

	CameraTransform::CameraTransform(CameraTransform &&other) : vlim{ other.vlim }, rotv{ other.rotv }, roth{ other.roth }, position{ other.position } {
	}

	CameraTransform &CameraTransform::operator =(const CameraTransform &other) {
		vlim = other.vlim;
		rotv = other.rotv;
		roth = other.roth;
		position = other.position;

		return *this;
	}

	CameraTransform &CameraTransform::operator =(CameraTransform &&other) {
		vlim = other.vlim;
		rotv = other.rotv;
		roth = other.roth;
		position = other.position;

		return *this;
	}

	void CameraTransform::setLimits(F32 rad) {
		vlim = rad;
	}

	F32 CameraTransform::getLimits() const {
		return vlim;
	}

	void CameraTransform::rotateVertical(F32 rad) {
		rotv += rad;
		if (rotv > vlim) {
			rotv = vlim;
		}
		else if (rotv < -vlim) {
			rotv = -vlim;
		}
	}

	F32 CameraTransform::getRotationVertical() const {
		return rotv;
	}

	void CameraTransform::setRotationVertical(F32 rad) {
		rotv = rad;
		if (rotv > vlim) {
			rotv = vlim;
		}
		else if (rotv < -vlim) {
			rotv = -vlim;
		}
	}

	void CameraTransform::rotateHorizontal(F32 rad) {
		roth += rad;
	}

	F32 CameraTransform::getRotationHorizontal() const {
		return roth;
	}

	void CameraTransform::setRotationHorizontal(F32 rad) {
		roth = rad;
	}

	glm::vec3 CameraTransform::getForward() {
		return glm::vec3{ glm::vec4{0, 0, -1, 0} *worldToLocal() };
	}

	void CameraTransform::translate(F32 x, F32 y, F32 z) {
		position += glm::vec3{ x, y, z };
	}

	void CameraTransform::translate(glm::vec3 trans) {
		position += trans;
	}

	void CameraTransform::translateForward(F32 x, F32 y, F32 z) {
		translateForward(glm::vec3{ x, y, z });
	}

	void CameraTransform::translateForward(glm::vec3 trans) {
		glm::vec4 trans4{ trans, 0.f };

		trans4 = trans4 * worldToLocal();

		translate(glm::vec3{ trans4 });
	}

	F32 CameraTransform::getPositionX() const {
		return position.x;
	}

	F32 CameraTransform::getPositionY() const {
		return position.y;
	}

	F32 CameraTransform::getPositionZ() const {
		return position.z;
	}

	glm::vec3 CameraTransform::getPosition() const {
		return position;
	}

	void CameraTransform::setPosition(F32 x, F32 y, F32 z) {
		position = glm::vec3{ x, y, z };
	}

	void CameraTransform::setPosition(glm::vec3 pos) {
		position = pos;
	}

	glm::mat4 CameraTransform::worldToLocal() const {
		glm::mat4 mv{ 1.f };

		mv[1][1] = std::cosf(-rotv);
		mv[2][1] = -std::sinf(-rotv);
		mv[1][2] = std::sinf(-rotv);
		mv[2][2] = std::cosf(-rotv);

		glm::mat4 mh{ 1.f };

		mh[0][0] = std::cosf(roth);
		mh[2][0] = std::sinf(roth);
		mh[0][2] = -std::sinf(roth);
		mh[2][2] = std::cosf(roth);

		glm::mat4 pos{ 1.f };

		pos[3][0] = -position.x;
		pos[3][1] = -position.y;
		pos[3][2] = -position.z;

		glm::mat4 t = mv * mh * pos;

		if (parent) {
			return t * parent->worldToLocal();
		}
		else {
			return t;
		}
	}

	glm::mat4 CameraTransform::localToWorld() const {
		return glm::inverse(worldToLocal());
	}
}