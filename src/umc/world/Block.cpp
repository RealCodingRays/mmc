#include "umc\world\Block.h"

#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtx\string_cast.hpp>

#define FACING_NORTH 0
#define FACING_EAST 1
#define FACING_SOUTH 2
#define FACING_WEST 3
#define FACING_UP 4
#define FACING_DOWN 5

#define GET_FACING_FRONT(dir)			(dir & 0x0F)
#define GET_FACING_UP(dir)				((dir & 0xF0) >> 4)

#define TRL_FACING_FRONT(front)			(front)
#define TRL_FACING_UP(up)				(up << 4)

#define SET_FACING_FRONT(dir, front)	((dir & 0xF0) | (front))
#define SET_FACING_UP(dir, up)			((dir & 0x0F) | (up << 4))

namespace umc {

	const ivec3 WorldDirection::NORTH{ 0, 0, 1 };
	const ivec3 WorldDirection::EAST{ 1, 0, 0 };
	const ivec3 WorldDirection::SOUTH{ 0, 0, -1 };
	const ivec3 WorldDirection::WEST{ -1, 0, 0 };
	const ivec3 WorldDirection::UP{ 0, 1, 0 };
	const ivec3 WorldDirection::DOWN{ 0, -1, 0 };

	const UI8 BlockSide::FRONT;
	const UI8 BlockSide::RIGHT;
	const UI8 BlockSide::BACK;
	const UI8 BlockSide::LEFT;
	const UI8 BlockSide::TOP;
	const UI8 BlockSide::BOTTOM;

	const BlockFacing BlockFacing::NORTH{ WorldDirection::NORTH };
	const BlockFacing BlockFacing::EAST{ WorldDirection::EAST };
	const BlockFacing BlockFacing::SOUTH{ WorldDirection::SOUTH };
	const BlockFacing BlockFacing::WEST{ WorldDirection::WEST };

	inline ivec3 cross(ivec3 a, ivec3 b) {
		return ivec3{ a.y * b.z - a.z * b.y,
					  a.z * b.x - a.x * b.z,
					  a.x * b.y - a.y * b.x };
	}

	inline I8 dot(ivec3 a, ivec3 b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	inline imat3 transpose(imat3 mat) {
		imat3 ret{ glm::uninitialize };
		ret[0][0] = mat[0][0];
		ret[0][1] = mat[1][0];
		ret[0][2] = mat[2][0];

		ret[1][0] = mat[0][1];
		ret[1][1] = mat[1][1];
		ret[1][2] = mat[2][1];

		ret[2][0] = mat[0][2];
		ret[2][1] = mat[1][2];
		ret[2][2] = mat[2][2];
		return ret;
	}

	ivec3 BlockSide::sideToDir(UI8 side) {
		switch (side) {
		case FACING_NORTH:
			return WorldDirection::NORTH;
		case FACING_EAST:
			return WorldDirection::EAST;
		case FACING_SOUTH:
			return WorldDirection::SOUTH;
		case FACING_WEST:
			return WorldDirection::WEST;
		case FACING_UP:
			return WorldDirection::UP;
		case FACING_DOWN:
			return WorldDirection::DOWN;
		default:
			std::cout << "[ERROR][Block.cpp]: Tried to convert invalid side to dir!" << std::endl;
			return WorldDirection::NORTH;
		}
	}

	UI8 BlockSide::dirToSide(ivec3 dir) {
		if (dir == WorldDirection::NORTH) {
			return FACING_NORTH;
		}
		else if (dir == WorldDirection::EAST) {
			return FACING_EAST;
		}
		else if (dir == WorldDirection::SOUTH) {
			return FACING_SOUTH;
		}
		else if (dir == WorldDirection::WEST) {
			return FACING_WEST;
		}
		else if (dir == WorldDirection::UP) {
			return FACING_UP;
		}
		else if (dir == WorldDirection::DOWN) {
			return FACING_DOWN;
		}
		else {
			std::cout << "[ERROR][Block.cpp]: Tried to convert invalid dir to side!" << std::endl;
			return FACING_NORTH;
		}
	}

	BlockFacing::BlockFacing() : facing{ TRL_FACING_FRONT(FACING_NORTH) | TRL_FACING_UP(FACING_UP) } {
	}

	BlockFacing::BlockFacing(ivec3 forward, ivec3 upward) : facing{ 0 } {
		if (!isValidDir(forward, upward)) {
			std::cout << "[WARINING][Block.cpp]: BlockFacing dir is invalid!" << std::endl;
			facing = TRL_FACING_FRONT(FACING_NORTH) | TRL_FACING_UP(FACING_UP);
			return;
		}

		facing = TRL_FACING_FRONT(BlockSide::dirToSide(forward)) | TRL_FACING_UP(BlockSide::dirToSide(upward));
	}

	BlockFacing::BlockFacing(const BlockFacing &other) : facing{ other.facing } {
	}

	BlockFacing::BlockFacing(BlockFacing &&other) : facing{ other.facing } {
	}

	BlockFacing &BlockFacing::operator =(const BlockFacing &other) {
		facing = other.facing;

		return *this;
	}

	BlockFacing &BlockFacing::operator =(BlockFacing &&other) {
		facing = other.facing;

		return *this;
	}

	bool BlockFacing::operator ==(const BlockFacing &other) const {
		return facing == other.facing;
	}

	void BlockFacing::setForward(ivec3 forward) {
		facing = SET_FACING_FRONT(facing, BlockSide::dirToSide(forward));
	}

	void BlockFacing::setUpward(ivec3 upward) {
		facing = SET_FACING_UP(facing, BlockSide::dirToSide(upward));
	}

	void BlockFacing::rotate(I8 steps) {
		steps = steps % 4;

		if (steps != 0) {
			ivec3 front = BlockSide::sideToDir(GET_FACING_FRONT(facing));
			const ivec3 up = BlockSide::sideToDir(GET_FACING_UP(facing));

			switch (steps) {
			case -3:
				front = cross(front, up);
				break;
			case -2:
				front = -front;
				break;
			case -1:
				front = cross(up, front);
				break;
			case 1:
				front = cross(front, up);
				break;
			case 2:
				front = -front;
				break;
			case 3:
				front = cross(up, front);
				break;
			}

			facing = SET_FACING_FRONT(facing, BlockSide::dirToSide(front));
		}
	}

	void BlockFacing::roll(I8 steps) {
		steps = steps % 4;

		if (steps != 0) {
			const ivec3 front = BlockSide::sideToDir(GET_FACING_FRONT(facing));
			ivec3 up = BlockSide::sideToDir(GET_FACING_UP(facing));

			switch (steps) {
			case -3:
				up = cross(front, up);
				break;
			case -2:
				up = -up;
				break;
			case -1:
				up = cross(up, front);
				break;
			case 1:
				up = cross(front, up);
				break;
			case 2:
				up = -up;
				break;
			case 3:
				up = cross(up, front);
				break;
			}

			facing = SET_FACING_UP(facing, BlockSide::dirToSide(up));
		}
	}

	ivec3 BlockFacing::front() const {
		return BlockSide::sideToDir(GET_FACING_FRONT(facing));
	}

	ivec3 BlockFacing::back() const {
		return -BlockSide::sideToDir(GET_FACING_FRONT(facing));
	}

	ivec3 BlockFacing::right() const {
		ivec3 f = front();
		ivec3 t = up();
		return cross(f, t);
	}

	ivec3 BlockFacing::left() const {
		ivec3 f = front();
		ivec3 t = up();
		return cross(t, f);
	}

	ivec3 BlockFacing::up() const {
		return BlockSide::sideToDir(GET_FACING_UP(facing));
	}

	ivec3 BlockFacing::down() const {
		return -BlockSide::sideToDir(GET_FACING_UP(facing));
	}

	imat3 BlockFacing::toMatrixLTW() const {
		ivec3 f{ BlockSide::sideToDir(GET_FACING_FRONT(facing)) };
		ivec3 t{ BlockSide::sideToDir(GET_FACING_UP(facing)) };

		return imat3{ cross(f, t), t, f };
	}

	imat3 BlockFacing::toMatrixWTL() const {
		ivec3 f{ BlockSide::sideToDir(GET_FACING_FRONT(facing)) };
		ivec3 t{ BlockSide::sideToDir(GET_FACING_UP(facing)) };

		return transpose(imat3{ cross(f, t), t, f });
	}

	ivec3 BlockFacing::translateLTW(ivec3 dir) const {
		ivec3 f{ BlockSide::sideToDir(GET_FACING_FRONT(facing)) };
		ivec3 t{ BlockSide::sideToDir(GET_FACING_UP(facing)) };

		imat3 trans{ cross(f, t), t, f };

		return trans * dir;
	}

	ivec3 BlockFacing::translateWTL(ivec3 dir) const {
		ivec3 f{ BlockSide::sideToDir(GET_FACING_FRONT(facing)) };
		ivec3 t{ BlockSide::sideToDir(GET_FACING_UP(facing)) };

		imat3 trans{ cross(f, t), t, f };

		return dir * trans;
	}

	UI8 BlockFacing::getSideLTW(ivec3 dir) const {
		int cnt = 0;
		if (dir.x == 0) {
			cnt++;
		}
		if (dir.y == 0) {
			cnt++;
		}
		if (dir.z == 0) {
			cnt++;
		}
		if (cnt != 2) {
			std::cout << "[ERROR][Block.cpp]: Tried to translate invalid dir to side!" << std::endl;
			return BlockSide::FRONT;
		}

		I8 len = dir.x + dir.y + dir.z;
		if (len < 0) {
			len = -len;
		}
		dir = ivec3{ dir.x / len, dir.y / len, dir.z / len };

		return BlockSide::dirToSide(translateLTW(dir));
	}

	UI8 BlockFacing::getSideWTL(ivec3 dir) const {
		int cnt = 0;
		if (dir.x == 0) {
			cnt++;
		}
		if (dir.y == 0) {
			cnt++;
		}
		if (dir.z == 0) {
			cnt++;
		}
		if (cnt != 2) {
			std::cout << "[ERROR][Block.cpp]: Tried to translate invalid dir to side!" << std::endl;
			return BlockSide::FRONT;
		}

		I8 len = dir.x + dir.y + dir.z;
		if (len < 0) {
			len = -len;
		}
		dir = ivec3{ dir.x / len, dir.y / len, dir.z / len };

		return BlockSide::dirToSide(translateWTL(dir));
	}

	bool BlockFacing::isValidDir(ivec3 forward, ivec3 upward) {
		if (dot(forward, forward) != 1) {
			return false;
		}
		if (dot(upward, upward) != 1) {
			return false;
		}
		if (dot(forward, upward) != 0) {
			return false;
		}
		return true;
	}
}