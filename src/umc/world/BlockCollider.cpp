#include "umc\world\BlockCollider.h"

namespace umc {

	BlockCollider::BlockCollider(ivec3 pos) : box{ glm::vec3{pos} -0.5f, glm::vec3{pos} +0.5f } {
	}

	BlockCollider::~BlockCollider() {
	}

	bool BlockCollider::isInRange(glm::vec3 origin, glm::vec3 dir) const {
		return true;
	}

	bool BlockCollider::raycast(glm::vec3 origin, glm::vec3 dir, glm::vec3 &hit, glm::vec3 &norm) const {
		return box.raycast(origin, dir, hit, norm);
	}

	const AABB &BlockCollider::getDomain() const {
		return box;
	}

	WorldObject *BlockCollider::getParentObject() {
		return nullptr;
	}

	WorldObject *BlockCollider::getControllObject() {
		return nullptr;
	}
}