#include "umc\world\Collision.h"

#include "umc\world\World.h"
#include "umc\world\BlockCollider.h"

#include <glm\glm.hpp>
#include <iostream>

namespace umc {

	// WorldCollision

	WorldCollision::WorldCollision() : root{ glm::vec3{World::WORLD_CENTER}, World::WORLD_DIAMETER / 2.f } {
	}

	WorldCollision::~WorldCollision() {
		for (auto it : blocks) {
			delete it.second;
		}
	}

	void WorldCollision::registerBlock(ivec3 pos) {
		auto it = blocks.find(ivec3ToHash(pos));

		if (it != blocks.end()) {
			std::cout << "[WARNING][Collider.cpp]: Inserting already existing block!" << std::endl;
			removeCollider(it->second);
			delete it->second;
			it->second = new BlockCollider(pos);
			registerBlockCollider(it->second, pos);
		}
		else {
			Collider *&coll = blocks[ivec3ToHash(pos)];
			coll = new BlockCollider(pos);
			registerBlockCollider(coll, pos);
		}
	}

	void WorldCollision::registerBlockCollider(Collider *collider, ivec3 pos) {
		root.insertCollider(collider, pos, 8);
	}

	void WorldCollision::registerCollider(Collider *collider) {
		root.insertCollider(collider, ivec3{ CHAR_MIN }, 8);
	}

	void WorldCollision::removeBlock(ivec3 pos) {
		auto it = blocks.find(ivec3ToHash(pos));

		if (it != blocks.end()) {
			removeCollider(it->second);
			blocks.erase(it);
		}
		else {
			//std::cout << "[WARNING][Collider.cpp]: Tried to remove non existing block!" << std::endl;
		}
	}

	void WorldCollision::removeCollider(Collider *collider) {
		if (root.removeCollider(collider)) {
			return;
		}

		std::cout << "[WARNING][Collision.cpp]: Unable to find collider!... trying brute force" << std::endl;
		if (root.removeColliderAll(collider)) {
			return;
		}

		std::cout << "[WARNING][Collision.cpp]: Unable to find collider!" << std::endl;
	}

	bool WorldCollision::raycast(RaycastHit &hit) {
		hit.type = RaycastHit::HitType::NONE;
		hit.direction = glm::normalize(hit.direction);

		F32 dist2{ FLT_MAX };
		root.raycast(hit, dist2);
		return hit.type != RaycastHit::HitType::NONE;
	}

	// WorldCollision::TreeNode

	WorldCollision::TreeNode::TreeNode(glm::vec3 mid, F32 size) : mid{ mid }, radius{ size } {
	}

	WorldCollision::TreeNode::~TreeNode() {
		for (TreeNode *&n : next) {
			if (n) {
				nodeAllocator.destroy(n);
				nodeAllocator.deallocate(n, 1);
			}
		}
		colliders.clear();
	}

	void WorldCollision::TreeNode::insertCollider(Collider *collider, ivec3 position, int level) {
		// If last node insert collider
		if (level == 1) {
			collref ref;
			ref.collider = collider;
			ref.position = position;
			colliders.push_back(ref);
			return;
		}

		// Test if object fits in child nodes
		const AABB &domain = collider->getDomain();
		for (int i = 0; i < 8; i++) {
			if (next[i]) {
				if (next[i]->isInside(domain)) {
					next[i]->insertCollider(collider, position, level - 1);
					return;
				}
			}
			else {
				// Test if new node has to be created
				F32 nrad = radius / 2.f;
				glm::vec3 dir;
				switch (i) {
				case 0: dir = glm::vec3{ -1, -1, -1 }; break;
				case 1: dir = glm::vec3{ -1, -1,  1 }; break;
				case 2: dir = glm::vec3{ -1,  1, -1 }; break;
				case 3: dir = glm::vec3{ -1,  1,  1 }; break;
				case 4: dir = glm::vec3{ 1, -1, -1 }; break;
				case 5: dir = glm::vec3{ 1, -1,  1 }; break;
				case 6: dir = glm::vec3{ 1,  1, -1 }; break;
				case 7: dir = glm::vec3{ 1,  1,  1 }; break;
				}
				dir = dir * nrad;
				glm::vec3 nmid = mid + dir;

				AABB naabb{ nmid, nrad };

				if (naabb.isInside(domain)) {
					next[i] = nodeAllocator.allocate(1);
					nodeAllocator.construct(next[i], TreeNode{ nmid, nrad });
					next[i]->insertCollider(collider, position, level - 1);
					return;
				}
			}
		}

		// Insert collider
		collref ref;
		ref.collider = collider;
		ref.position = position;
		colliders.push_back(ref);
		return;
	}

	bool WorldCollision::TreeNode::removeCollider(Collider *collider) {
		const AABB &domain = collider->getDomain();
		for (TreeNode *n : next) {
			if (n) {
				if (n->isInside(domain)) {
					return n->removeCollider(collider);
				}
			}
		}

		for (auto it = colliders.begin(); it != colliders.end(); it++) {
			if (it->collider == collider) {
				colliders.erase(it);
				return true;
			}
		}
		return false;
	}

	bool WorldCollision::TreeNode::removeColliderAll(Collider *collider) {
		for (TreeNode *n : next) {
			if (n) {
				if (n->removeColliderAll(collider)) {
					return true;
				}
			}
		}

		for (auto it = colliders.begin(); it != colliders.end(); it++) {
			if (it->collider == collider) {
				colliders.erase(it);
				return true;
			}
		}

		return false;
	}

	bool WorldCollision::TreeNode::isInside(glm::vec3 pos) {
		return glm::length(mid - pos) <= radius + F32_MOE;
	}

	bool WorldCollision::TreeNode::isInside(const AABB &aabb) {
		AABB b1{ mid, radius };
		return b1.isInside(aabb);
	}

	void WorldCollision::TreeNode::raycast(RaycastHit &ray, F32 &dist2) {
		// Do early elimination
		glm::vec3 t1 = mid - ray.origin;
		if (glm::length(glm::cross(ray.direction, t1)) > radius * std::sqrt(3) + F32_MOE) {
			return;
		}

		// Test child nodes
		for (TreeNode *n : next) {
			if (n) {
				n->raycast(ray, dist2);
			}
		}

		// Find closest collider
		for (collref &coll : colliders) {
			if (coll.collider->isInRange(ray.origin, ray.direction)) {
				glm::vec3 hit, nor;
				if (coll.collider->raycast(ray.origin, ray.direction, hit, nor)) {

					glm::vec3 oh = hit - ray.origin;
					F32 d2 = glm::dot(oh, oh);
					if (d2 < dist2) {
						dist2 = d2;
						ray.hit = hit;
						ray.normal = nor;
						ray.object = coll.collider;
						if (coll.position.x != CHAR_MIN) {
							ray.type = RaycastHit::HitType::BLOCK;
							ray.blockpos = coll.position;
						}
						else {
							ray.type = RaycastHit::HitType::ENTITY;
						}
					}
				}
			}
		}
	}

	bool WorldCollision::TreeNode::isEmpty() {
		if (!colliders.empty()) {
			return false;
		}
		for (TreeNode *n : next) {
			if (n) {
				return false;
			}
		}
		return true;;
	}
}