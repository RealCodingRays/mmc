#include "umc\world\World.h"

#include "umc\Engine.h"
#include "umc\renderer\native\OpenGL.h"

#include "umc\io\ResourceManager.h"
#include "umc\io\resource\TextResource.h"


#include <iostream>

namespace umc {

	World::BlockUnion::BlockUnion() {
		air.flow = glm::vec3{ 1.f, 0.f, 0.f };
		air.pressure = 1.f;
	}

	World::World() {
		/*for (BlockData &b : blocks) {
			b.id = 0;
			b.data.air.pressure = 1;
			b.data.air.flow = glm::vec3{ 1.f, 0.f, 0.f };
		}*/
	}

	World::~World() {
	}

	void World::fill(BlockDomain &domain, BlockCreationRule &rule) {

	}

	void World::setBlock(ivec3 pos, BlockFacing dir, BlockID id) {
		BlockData &data = findBlock(pos);
		if (data.id != 0) {
			renderer.remove(pos);
			collision.removeBlock(pos);
		}
		data.id = id;
		data.data.solid.dir = dir;

		updateBlock(pos);
		updateBlock(north(pos));
		updateBlock(east(pos));
		updateBlock(south(pos));
		updateBlock(west(pos));
		updateBlock(up(pos));
		updateBlock(down(pos));
	}

	BlockID World::getBlockID(ivec3 pos) {
		return findBlock(pos).id;
	}

	bool World::isSolidBlock(ivec3 pos) {
		return findBlock(pos).id != 0;
	}

	bool World::isVisible(ivec3 pos) {
		for (UI8 i = 0; i < 6; i++) {
			ivec3 dir = BlockSide::sideToDir(i);

			if (getBlockID(pos + dir) == 0) {
				return true;
			}
		}
		return false;
	}

	void World::updateBlock(ivec3 pos) {
		if (glm::any(glm::lessThan(pos, ivec3{ BLOCK_MIN })) || glm::any(glm::greaterThan(pos, ivec3{ BLOCK_MAX }))) {
			return;
		}

		if (findBlock(pos).id != 0) {
			renderer.remove(pos);
			collision.removeBlock(pos);

			bool visible{ false };
			for (UI8 i = 0; i < 6; i++) {
				ivec3 dir = BlockSide::sideToDir(i);

				if (getBlockID(pos + dir) == 0) {
					UI8 side = findBlock(pos).data.solid.dir.getSideWTL(dir);
					renderer.insert(&pos, &(findBlock(pos).data.solid.dir), &(side), Engine::blockResistry.getBlockTexture(findBlock(pos).id, side));
					visible = true;
				}
			}

			if (visible) {
				collision.registerBlock(pos);
			}
		}
	}

	Renderer *World::getRenderer() {
		return &renderer;
	}

	inline World::BlockData &World::findBlock(ivec3 pos) {
		return blocks[(static_cast<int>(pos.x) + 64) * 128 * 128
					+ (static_cast<int>(pos.y) + 64) * 128
					+ (static_cast<int>(pos.z) + 64)];
	}

	void World::update(F32 dt) {
		publobj.update(dt);
	}
}