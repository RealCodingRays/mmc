#include "umc\world\WorldObject.h"

namespace umc {

	LogicComponent::~LogicComponent() {
	}

	void LogicComponent::attach(WorldObject &obj) {
	}

	void LogicComponent::detach(WorldObject &obj) {
	}


	WorldObject::WorldObject() {
		transform.object = this;
	}

	WorldObject::~WorldObject() {
		if (logic) {
			logic->detach(*this);
			logic = nullptr;
		}
	}

	void WorldObject::setLogicComponent(LogicComponent *component) {
		if (logic) {
			logic->detach(*this);
		}

		logic = component;

		if (logic) {
			logic->attach(*this);
		}
	}

	LogicComponent *WorldObject::getLogicComponent() {
		return logic;
	}

	void WorldObject::update(F32 dt) {
		if (logic) {
			logic->update(dt, *this);
		}
	}
}