#include "umc\world\BlocksRenderer.h"

#include "umc\Engine.h"
#include "umc\renderer\native\OpenGL.h"

#include "umc\renderer\GLObjects.h"
#include "umc\io\resource\TextLoader.h"
#include "umc\io\ResourceManager.h"

#include <iostream>
#include <glm\gtx\string_cast.hpp>

namespace umc {

	GLBuffer BlocksRenderer::vertecies;
	GLProgram BlocksRenderer::program;

	BlocksRenderer::BlocksRenderer() {
	}

	BlocksRenderer::~BlocksRenderer() {
	}

	void BlocksRenderer::deinit() {
		for (ttypeset &set : data) {
			set.deinit();
		}
		data.clear();
		indexes.clear();
		faces.clear();
	}

	void BlocksRenderer::insert(const ivec3 *pos, const BlockFacing *dir, const UI8 *side, ResourceID texture, size_t len) {
		ttypeset *set;

		auto tex = indexes.find(texture);

		if (tex == indexes.end()) {
			ttypeset nset;
			nset.texture = texture;
			nset.init(4);

			data.push_back(nset);
			UI32 index = static_cast<UI32>(data.size() - 1);

			indexes[texture] = index;
			set = &(data[index]);
		}
		else {
			set = &(data[tex->second]);
		}

		I32 dindex = set->reserve(len);
		faceData *data = new faceData[len];
		for (size_t i = 0; i < len; i++) {
			imat3 baserot{ 1 };
			switch (side[i]) {
			case BlockSide::FRONT:
				baserot = BlockFacing{ WorldDirection::NORTH, WorldDirection::UP }.toMatrixLTW();
				break;
			case BlockSide::RIGHT:
				baserot = BlockFacing{ WorldDirection::EAST, WorldDirection::UP }.toMatrixLTW();
				break;
			case BlockSide::BACK:
				baserot = BlockFacing{ WorldDirection::SOUTH, WorldDirection::UP }.toMatrixLTW();
				break;
			case BlockSide::LEFT:
				baserot = BlockFacing{ WorldDirection::WEST, WorldDirection::UP }.toMatrixLTW();
				break;
			case BlockSide::TOP:
				baserot = BlockFacing{ WorldDirection::UP, WorldDirection::SOUTH }.toMatrixLTW();
				break;
			case BlockSide::BOTTOM:
				baserot = BlockFacing{ WorldDirection::DOWN, WorldDirection::NORTH }.toMatrixLTW();
				break;
			default:
				std::cout << "[ERROR][BlocksRenderer.cpp]: Invalid block side!" << std::endl;
			}

			faceInfo &face = faces[ivec3ToHash(pos[i])];

			if (face.index[side[i]] >= 0) {
				remove(pos[i], side[i]);
			}

			face.texture[side[i]] = texture;
			face.index[side[i]] = dindex + static_cast<I32>(i);
			
			data[i].pos = pos[i];
			data[i].rot = dir[i].toMatrixLTW() * baserot;
		}

		set->insert(dindex, data, len);

		delete[] data;
	}

	void BlocksRenderer::remove(ivec3 pos) {
		auto it = faces.find(ivec3ToHash(pos));

		if (it != faces.end()) {
			for (int i = 0; i < 6; i++) {
				if (it->second.index[i] >= 0) {
					removeFace(it->second.index[i], it->second.texture[i]);
				}
			}
			faces.erase(it);
		}
	}

	void BlocksRenderer::remove(ivec3 pos, UI8 side) {
		auto it = faces.find(ivec3ToHash(pos));

		if (it != faces.end()) {
			if (it->second.index[side] >= 0) {
				removeFace(it->second.index[side], it->second.texture[side]);
				it->second.index[side] = -1;
			}
		}
	}

	void BlocksRenderer::draw(const PipelineInfo &info) {
		program.use();

		program.setUniformMatrix4("wtoc", gl::GL_FALSE, info.world_to_camera);
		program.setUniformMatrix4("ctos", gl::GL_FALSE, info.camera_to_screen);
		program.setUniformMatrix4("wtocn", gl::GL_FALSE, info.world_to_camera_norm);
		program.setUniformMatrix4("ctosn", gl::GL_FALSE, info.camera_to_screen_norm);

		program.setUniformValue("albedo", 0);
		program.setUniformValue("normal", 1);
		program.setUniformValue("roughness", 2);
		//program.setUniformValue("height", 3);

		for (ttypeset &set : data) {
			if (set.facecnt > 0) {
				umc::Engine::textureRegistry.bindTextureGroup(set.texture);

				gl::glBindVertexArray(set.vao);
				gl::glDrawArraysInstanced(gl::GL_TRIANGLES, 0, 6, set.facecnt);
			}
		}
		gl::glActiveTexture(gl::GL_TEXTURE4);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		gl::glActiveTexture(gl::GL_TEXTURE3);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		gl::glActiveTexture(gl::GL_TEXTURE2);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		gl::glActiveTexture(gl::GL_TEXTURE1);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		gl::glActiveTexture(gl::GL_TEXTURE0);
		gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
		gl::glBindVertexArray(0);

		program.unuse();
	}

	void BlocksRenderer::configureVao(gl::GLuint vao, gl::GLuint buffer) {
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, buffer);
		gl::glBindVertexArray(vao);
		
		gl::GLint loc;
		loc = program.getAttribLocation("offset");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc);
			gl::glVertexAttribIPointer(loc, 3, gl::GL_BYTE, sizeof(faceData), reinterpret_cast<void *>(offsetof(faceData, pos)));
			gl::glVertexAttribDivisor(loc, 1);
		}
		loc = program.getAttribLocation("rotation");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc + 0);
			gl::glEnableVertexAttribArray(loc + 1);
			gl::glEnableVertexAttribArray(loc + 2);
			gl::glVertexAttribPointer(loc + 0, 3, gl::GL_BYTE, gl::GL_FALSE, sizeof(faceData), reinterpret_cast<void *>(offsetof(faceData, rot[0])));
			gl::glVertexAttribPointer(loc + 1, 3, gl::GL_BYTE, gl::GL_FALSE, sizeof(faceData), reinterpret_cast<void *>(offsetof(faceData, rot[1])));
			gl::glVertexAttribPointer(loc + 2, 3, gl::GL_BYTE, gl::GL_FALSE, sizeof(faceData), reinterpret_cast<void *>(offsetof(faceData, rot[2])));
			gl::glVertexAttribDivisor(loc + 0, 1);
			gl::glVertexAttribDivisor(loc + 1, 1);
			gl::glVertexAttribDivisor(loc + 2, 1);
		}

		gl::glBindVertexArray(0);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	void BlocksRenderer::initStatic() {
		const F32 verts[]{
			// --position--        --normal--          --uv--       --tangent--
			// Front face
			 0.f,  0.f,  1.f,    0.f,  0.f,  1.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  0.f,  1.f,    1.f,  0.f,    1.f,  0.f,  0.f,
			 0.f,  1.f,  1.f,    0.f,  0.f,  1.f,    0.f,  0.f,    1.f,  0.f,  0.f,

			 0.f,  0.f,  1.f,    0.f,  0.f,  1.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  0.f,  1.f,    0.f,  0.f,  1.f,    1.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  0.f,  1.f,    1.f,  0.f,    1.f,  0.f,  0.f,

			// Right face
			 1.f,  0.f,  1.f,    1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f, -1.f,
			 1.f,  1.f,  0.f,    1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f, -1.f,
			 1.f,  1.f,  1.f,    1.f,  0.f,  0.f,    0.f,  0.f,    0.f,  0.f, -1.f,

			 1.f,  0.f,  1.f,    1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f, -1.f,
			 1.f,  0.f,  0.f,    1.f,  0.f,  0.f,    1.f,  1.f,    0.f,  0.f, -1.f,
			 1.f,  1.f,  0.f,    1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f, -1.f,

			// Back face
			 1.f,  0.f,  0.f,    0.f,  0.f, -1.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 0.f,  1.f,  0.f,    0.f,  0.f, -1.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
			 1.f,  1.f,  0.f,    0.f,  0.f, -1.f,    0.f,  0.f,   -1.f,  0.f,  0.f,

			 1.f,  0.f,  0.f,    0.f,  0.f, -1.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 0.f,  0.f,  0.f,    0.f,  0.f, -1.f,    1.f,  1.f,   -1.f,  0.f,  0.f,
			 0.f,  1.f,  0.f,    0.f,  0.f, -1.f,    1.f,  0.f,   -1.f,  0.f,  0.f,

			// Left face
			 0.f,  0.f,  0.f,   -1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f,  1.f,
			 0.f,  1.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f,  1.f,
			 0.f,  1.f,  0.f,   -1.f,  0.f,  0.f,    0.f,  0.f,    0.f,  0.f,  1.f,

			 0.f,  0.f,  0.f,   -1.f,  0.f,  0.f,    0.f,  1.f,    0.f,  0.f,  1.f,
			 0.f,  0.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  1.f,    0.f,  0.f,  1.f,
			 0.f,  1.f,  1.f,   -1.f,  0.f,  0.f,    1.f,  0.f,    0.f,  0.f,  1.f,

			// Top face
			 0.f,  1.f,  1.f,    0.f,  1.f,  0.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  0.f,    0.f,  1.f,  0.f,    1.f,  0.f,    1.f,  0.f,  0.f,
			 0.f,  1.f,  0.f,    0.f,  1.f,  0.f,    0.f,  0.f,    1.f,  0.f,  0.f,

			 0.f,  1.f,  1.f,    0.f,  1.f,  0.f,    0.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  1.f,    0.f,  1.f,  0.f,    1.f,  1.f,    1.f,  0.f,  0.f,
			 1.f,  1.f,  0.f,    0.f,  1.f,  0.f,    1.f,  0.f,    1.f,  0.f,  0.f,

			// Bottom face
			 0.f,  0.f,  0.f,    0.f, -1.f,  0.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f,  0.f,  1.f,    0.f, -1.f,  0.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
			 0.f,  0.f,  1.f,    0.f, -1.f,  0.f,    0.f,  0.f,   -1.f,  0.f,  0.f,

			 0.f,  0.f,  0.f,    0.f, -1.f,  0.f,    0.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f,  0.f,  0.f,    0.f, -1.f,  0.f,    1.f,  1.f,   -1.f,  0.f,  0.f,
			 1.f,  0.f,  1.f,    0.f, -1.f,  0.f,    1.f,  0.f,   -1.f,  0.f,  0.f,
		};

		vertecies.init(sizeof(verts) / sizeof(F32), gl::GL_ARRAY_BUFFER, verts);

		ResourcePath vertName{ "umc:assets/shaders/Blocks.vert" };
		ResourcePath fragName{ "umc:assets/shaders/Blocks.frag" };

		ResourceReference<TextResource> vertRes = ResourceManager::get().loadResource<TextResource>(vertName);
		ResourceReference<TextResource> fragRes = ResourceManager::get().loadResource<TextResource>(fragName);

		GLShader vert{ vertRes->get(), static_cast<gl::GLsizei>(vertRes->size()), gl::GL_VERTEX_SHADER };
		GLShader frag{ fragRes->get(), static_cast<gl::GLsizei>(fragRes->size()), gl::GL_FRAGMENT_SHADER };

		program.init();
		program.attach(vert);
		program.attach(frag);

		if (!program.link()) {
			std::cout << "[ERROR][BlocksRenderer.cpp]: Failed to link blocks program!" << std::endl;
		}
	}

	void BlocksRenderer::deinitStatic() {
		vertecies.~GLBuffer();
		program.~GLProgram();
	}

	void BlocksRenderer::removeFace(I32 index, ResourceID texture) {
		auto it = indexes.find(texture);

		if (it != indexes.end()) {
			ivec3 lpos;
			I32 oindex;

			if (data[it->second].removeFace(index, lpos, oindex)) {
				auto f = faces.find(ivec3ToHash(lpos));

				if (f != faces.end()) {
					for (int i = 0; i < 6; i++) {
						if (f->second.index[i] == oindex && f->second.texture[i] == texture) {
							f->second.index[i] = index;
							break;
						}
					}
				}
			}
		}
	}

	void BlocksRenderer::ttypeset::deinit() {
		if (vao > 0) {
			gl::glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
		if (buffer > 0) {
			gl::glDeleteBuffers(1, &buffer);
			buffer = 0;
		}
		memsize = 0;
		facecnt = 0;
	}

	void BlocksRenderer::ttypeset::init(gl::GLsizei size) {
		gl::glGenVertexArrays(1, &vao);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vertecies);
		gl::glBindVertexArray(vao);
		gl::GLint loc;
		loc = program.getAttribLocation("pos");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc);
			gl::glVertexAttribPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 11, nullptr);
		}

		loc = program.getAttribLocation("normal");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc);
			gl::glVertexAttribPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 11, reinterpret_cast<void *>(sizeof(F32) * 3));
		}

		loc = program.getAttribLocation("uv");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc);
			gl::glVertexAttribPointer(loc, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 11, reinterpret_cast<void *>(sizeof(F32) * 6));
		}

		loc = program.getAttribLocation("tangent");
		if (loc >= 0) {
			gl::glEnableVertexAttribArray(loc);
			gl::glVertexAttribPointer(loc, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(F32) * 11, reinterpret_cast<void *>(sizeof(F32) * 8));
		}
		gl::glBindVertexArray(0);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		gl::glGenBuffers(1, &buffer);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, buffer);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, size * sizeof(faceData), nullptr, gl::GL_DYNAMIC_STORAGE_BIT);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		BlocksRenderer::configureVao(vao, buffer);

		memsize = size;
	}

	I32 BlocksRenderer::ttypeset::reserve(size_t n) {
		if (facecnt + n >= memsize) {
			gl::GLsizei nsize{ (memsize + 1) * 2 };
			while (facecnt + n >= nsize) {
				nsize *= 2;
			}
			resize(nsize);
		}

		I32 ret = facecnt;
		facecnt += static_cast<gl::GLsizei>(n);

		return ret;
	}

	void BlocksRenderer::ttypeset::insert(I32 index, faceData *data, size_t n) {
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, buffer);
		gl::glBufferSubData(gl::GL_ARRAY_BUFFER, index * sizeof(faceData), sizeof(faceData) * n, data);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	}

	bool BlocksRenderer::ttypeset::removeFace(I32 index, ivec3 &lpos, I32 &oindex) {
		if (facecnt <= 1) {
			facecnt = 0;
			return false;
		}
		if (index == facecnt - 1) {
			facecnt--;
			return false;
		}
		else {
			oindex = --facecnt;

			faceData last;

			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, buffer);
			gl::glGetBufferSubData(gl::GL_ARRAY_BUFFER, oindex * sizeof(faceData), sizeof(faceData), &last);
			gl::glBufferSubData(gl::GL_ARRAY_BUFFER, index * sizeof(faceData), sizeof(faceData), &last);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

			lpos = last.pos;
			return true;
		}
	}

	void BlocksRenderer::ttypeset::resize(gl::GLsizei nsize) {
		gl::GLuint nbuffer;
		gl::glGenBuffers(1, &nbuffer);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, nbuffer);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, nsize * sizeof(faceData), nullptr, gl::GL_DYNAMIC_STORAGE_BIT);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		BlocksRenderer::configureVao(vao, nbuffer);

		gl::glBindBuffer(gl::GL_COPY_READ_BUFFER, buffer);
		gl::glBindBuffer(gl::GL_COPY_WRITE_BUFFER, nbuffer);
		gl::glCopyBufferSubData(gl::GL_COPY_READ_BUFFER, gl::GL_COPY_WRITE_BUFFER, 0, 0, std::min(facecnt, nsize - 1) * sizeof(faceData));
		gl::glBindBuffer(gl::GL_COPY_WRITE_BUFFER, 0);
		gl::glBindBuffer(gl::GL_COPY_READ_BUFFER, 0);

		gl::glDeleteBuffers(1, &buffer);

		buffer = nbuffer;
		memsize = nsize;
		facecnt = std::min(facecnt, nsize - 1);
	}
}