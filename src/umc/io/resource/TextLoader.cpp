#include "umc\io\resource\TextLoader.h"

#include <fstream>
#include <iostream>

namespace umc {

	TextResource::TextResource(const char *text, size_t size) : data{ text }, siz{ size } {
	}

	TextResource::~TextResource() {
		delete[] data;
		data = nullptr;
	}

	TextResourceInstance::TextResourceInstance(const char *text, size_t size) : resource{ text, size } {
	}

	TextResourceInstance::~TextResourceInstance() {
	}

	void *TextResourceInstance::getReference() {
		return &resource;
	}

	const type_info &TextResourceInstance::getResourceType() const {
		return typeid(TextResource);
	}

	void TextResourceInstance::close() {
	}

	TextResourceLoader::TextResourceLoader() {
	}

	TextResourceLoader::~TextResourceLoader() {
	}

	ResourceInstance *TextResourceLoader::load(const boost::filesystem::path &path) {
		std::ifstream in{ path.c_str(), std::ios::in | std::ios::ate };

		if (!in.is_open()) {
			std::cout << "[ERROR][TextLoader.cpp]: Unable to open file " << path << std::endl;
			return nullptr;
		}

		size_t size = static_cast<size_t>(in.tellg()) + 1;

		char *data = new char[size];
		
		if (size > 1) {
			in.seekg(0, in.beg);

			in.read(data, size - 1);
			size = in.gcount();
			data[size] = 0;
		}
		else {
			data[size - 1] = 0;
		}

		return new TextResourceInstance{ data, size };
	}

	bool TextResourceLoader::canLoad(const boost::filesystem::path &path) {
		return true;
	}

	int TextResourceLoader::loadPriority(const boost::filesystem::path &path) {
		if (wcscmp(path.extension().c_str(), L".txt") == 0) {
			return 1;
		}
		return 10000;
	}

	const char *TextResourceLoader::getName() const {
		return "umc::TextResourceLoader";
	}

	const type_info &TextResourceLoader::getResourceType() const {
		return typeid(TextResource);
	}
}