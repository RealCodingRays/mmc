#include "umc\io\resource\FontLoader.h"

#include <fstream>
#include <iostream>

#include "umc\io\ResourceManager.h"

namespace umc {

	FontResourceInstance::FontResourceInstance() {
	}

	FontResourceInstance::~FontResourceInstance() {
	}

	void *FontResourceInstance::getReference() {
		return &font;
	}

	const type_info &FontResourceInstance::getResourceType() const {
		return typeid(Font);
	}

	void FontResourceInstance::close() {
	}

	Font &FontResourceInstance::getFont() {
		return font;
	}


	const LoaderID FontResourceLoader::id{ 4 };

	FontResourceLoader::FontResourceLoader() {
	}

	FontResourceLoader::~FontResourceLoader() {
	}

	ResourceInstance *FontResourceLoader::load(const boost::filesystem::path &path) {
		boost::filesystem::path imgpath{ path };
		imgpath.replace_extension(".png");

		std::ifstream in{ path.c_str() };

		if (!in.is_open()) {
			std::cout << "[ERROR][FontLoader.cpp]: Unable to open file " << path << std::endl;
			return nullptr;
		}

		FontResourceInstance *inst = new FontResourceInstance{};
		
		ResourceReference<ImageResource> img = ResourceManager::get().loadResource<ImageResource>(imgpath);
		if (!img.ready()) {
			std::cout << "[ERROR][FontLoader.cpp]: Unable to load image file " << imgpath << std::endl;
			in.close();
			return nullptr;
		}

		inst->getFont().setTexture(img, 63, 53);

		std::string line;
		while (in.good()) {
			std::getline(in, line);

			size_t offset = line.find_first_of(' ');
			if (line.substr(0, offset) != "char") {
				continue;
			}

			offset = line.find_first_not_of(' ', offset);
			UI32 chr, x, y, width, height, advance;
			I32 xoff, yoff;
			UI8 chnl;
			size_t end;
			while ((end = line.find_first_of(' ', offset)) != std::string::npos) {
				std::string pair = line.substr(offset, end - offset);

				size_t equ = pair.find_first_of('=');
				if (equ != std::string::npos) {
					std::string key = pair.substr(0, equ);
					int val = atoi(&(pair.c_str()[equ + 1]));
					if (key == "id") {
						chr = val;
					}
					else if (key == "x") {
						x = val;
					}
					else if (key == "y") {
						y = val;
					}
					else if (key == "width") {
						width = val;
					}
					else if (key == "height") {
						height = val;
					}
					else if (key == "xoffset") {
						xoff = val;
					}
					else if (key == "yoffset") {
						yoff = val;
					}
					else if (key == "xadvance") {
						advance = val;
					}
					else if (key == "chnl") {
						chnl = val;
					}
					else if (key == "page") {
					}
					else {
						std::cout << "[WARNING][FontLoader.cpp]: Unknown key " << key << std::endl;
					}
				}
				else {
					std::cout << "[ERROR][FontLoader.cpp]: Error parsing value expected =" << std::endl;
				}

				offset = line.find_first_not_of(' ', end);
			}

			inst->getFont().addChar(chr, x, y, width, height, xoff, yoff, advance, chnl);
		}

		if (!in.eof()) {
			std::cout << "[WARNING][FontLoader.cpp]: Error while reading file " << path << std::endl;
		}

		return inst;
	}

	bool FontResourceLoader::canLoad(const boost::filesystem::path &path) {
		return true;
	}

	int FontResourceLoader::loadPriority(const boost::filesystem::path &path) {
		if (path.extension() == ".fnt") {
			return 1;
		}
		else {
			return 10000;
		}
	}

	const char *FontResourceLoader::getName() const {
		return "umc::FontResourceLoader";
	}

	const type_info &FontResourceLoader::getResourceType() const {
		return typeid(Font);
	}
}