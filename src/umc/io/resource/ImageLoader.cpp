#include "umc\io\resource\ImageLoader.h"

#include <png.h>
#include <fstream>
#include <iostream>

namespace umc {

	ImageResource::ImageResource(void *data, size_t width, size_t height, int chncnt, int depth) : data{ data }, width{ width }, height{ height }, chncnt{ chncnt }, depth{ depth } {
	}

	ImageResource::~ImageResource() {
		delete[] data;
	}

	ImageResource::ImageResource(ImageResource &&other) : data{ other.data }, width{ other.width }, height{ other.height }, chncnt{ other.chncnt }, depth{ other.depth } {
		other.data = nullptr;
	}

	UI8 &ImageResource::operator()(size_t x, size_t y, int chn) {
		return static_cast<UI8*>(data)[(y * width + x) * chncnt + chn];
	}

	void *ImageResource::get() {
		return data;
	}

	const void *ImageResource::get() const {
		return data;
	}

	size_t ImageResource::getWidth() const {
		return width;
	}

	size_t ImageResource::getHeight() const {
		return height;
	}

	int ImageResource::getNumChannels() const {
		return chncnt;
	}

	int ImageResource::getBitDepth() const {
		return depth;
	}


	ImageResourceInstance::ImageResourceInstance(void *data, size_t width, size_t height, int chncnt, int depth) : res{ data, width, height, chncnt, depth } {
	}

	ImageResourceInstance::~ImageResourceInstance() {
	}

	void *ImageResourceInstance::getReference() {
		return &res;
	}

	const type_info &ImageResourceInstance::getResourceType() const {
		return typeid(ImageResource);
	}

	void ImageResourceInstance::close() {
	}


	bool ImageResourceLoader::libpnginit{ false };
	const LoaderID ImageResourceLoader::id{ 3 };

	ImageResourceLoader::ImageResourceLoader() {
		if (!libpnginit) {

		}
	}

	ImageResourceLoader::~ImageResourceLoader() {
	}

	ResourceInstance *ImageResourceLoader::load(const boost::filesystem::path &path) {
		std::ifstream in{ path.c_str(), std::ios::in | std::ios::binary };

		if (!in.is_open()) {
			std::cout << "[ERROR][ImageLoader.cpp]: Failed to open file " << path << std::endl;
			return nullptr;
		}
		
		unsigned char sig[8];
		for (int i = 0; i < 8; i++) {
			char n;
			in.read(&n, 1);
			sig[i] = n;
		}
		if (in.eof()) {
			std::cout << "[ERROR][ImageLoader.cpp]: Invalid file header; too short!" << std::endl;
			in.close();
			return nullptr;
		}
		in.close();

		if (png_sig_cmp(sig, 0, 8) != 0) {
			std::cout << "[ERROR][ImageLoader.cpp]: Invalid file signature!" << std::endl;
			return nullptr;
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
		if (png_ptr == nullptr) {
			std::cout << "[ERROR][ImageLoader.cpp]: Failed to allocate data structures!" << std::endl;
			return nullptr;
		}

		png_infop info_ptr = png_create_info_struct(png_ptr);
		if (info_ptr == nullptr) {
			std::cout << "[ERROR][ImageLoader.cpp]: Failed to allocate info structure!" << std::endl;
			png_destroy_read_struct(&png_ptr, nullptr, nullptr);
			return nullptr;
		}

		png_infop end_ptr = png_create_info_struct(png_ptr);
		if (end_ptr == nullptr) {
			std::cout << "[ERROR][ImageLoader.cpp]: Failed to allocate info structure!" << std::endl;
			png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
			return nullptr;
		}

		size_t len = wcslen(path.c_str());
		char *name = new char[len + 1];
		for (size_t i = 0; i < len; i++) {
			name[i] = static_cast<char>(path.c_str()[i]);
		}
		name[len] = 0;

		FILE *fp;
		fopen_s(&fp, name, "rb");
		delete[] name;

		if (setjmp(png_jmpbuf(png_ptr))) {
			std::cout << "[ERROR][ImageLoader.cpp]: longjmp called!" << std::endl;
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
			fclose(fp);
			return nullptr;
		}

		png_set_sig_bytes(png_ptr, 0);

		png_init_io(png_ptr, fp);

		png_read_png(png_ptr, info_ptr, 0, nullptr);

		fclose(fp);

		png_bytepp ptr = png_get_rows(png_ptr, info_ptr);
		UI8 chncnt = png_get_channels(png_ptr, info_ptr);
		UI32 width = png_get_image_width(png_ptr, info_ptr);
		UI32 height = png_get_image_height(png_ptr, info_ptr);
		UI8 depth = png_get_bit_depth(png_ptr, info_ptr);

		UI8 bpp = depth / 8;
		UI32 bpl = width * chncnt * bpp;
		UI8 *data = new UI8[chncnt * width * height * bpp];

		for (UI32 y = 0; y < height; y++) {
			for (UI32 x = 0; x < width * chncnt; x++) {
				for (UI8 b = 0; b < bpp; b++) {
					data[y * bpl + x * bpp + b] = ptr[y][x * bpp + (bpp - b - 1)];
				}
			}
		}

		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);

		return new ImageResourceInstance(data, width, height, chncnt, depth);
	}

	bool ImageResourceLoader::canLoad(const boost::filesystem::path &path) {
		std::ifstream in{ path.c_str(), std::ios::in | std::ios::binary };

		if (!in.is_open()) {
			return false;
		}

		unsigned char sig[8];
		for (int i = 0; i < 8; i++) {
			char n;
			in.read(&n, 1);
			sig[i] = n;
		}
		if (in.eof()) {
			in.close();
			return false;
		}

		in.close();
		return png_sig_cmp(sig, 0, 8) == 0;
	}

	int ImageResourceLoader::loadPriority(const boost::filesystem::path &path) {
		if (wcscmp(path.extension().c_str(), L".png") == 0) {
			return 0;
		}
		return 10000;
	}

	const char *ImageResourceLoader::getName() const {
		return "umc::ImageResourceLoader";
	}

	const type_info &ImageResourceLoader::getResourceType() const {
		return typeid(ImageResource);
	}
}