#include "umc\io\resource\PropertiesLoader.h"

namespace umc {

	PropertiesResourceInstance::PropertiesResourceInstance(const boost::filesystem::path &path) : properties{ boost::filesystem::path{ path } } {
		properties.load();
	}

	PropertiesResourceInstance::~PropertiesResourceInstance() {
	}

	void *PropertiesResourceInstance::getReference() {
		return &properties;
	}

	const type_info &PropertiesResourceInstance::getResourceType() const {
		return typeid(Properties);
	}

	void PropertiesResourceInstance::close() {
		properties.write();
	}

	PropertiesResourceLoader::PropertiesResourceLoader() {
	}

	PropertiesResourceLoader::~PropertiesResourceLoader() {
	}

	ResourceInstance *PropertiesResourceLoader::load(const boost::filesystem::path &path) {
		return new PropertiesResourceInstance{ path };
	}

	bool PropertiesResourceLoader::canLoad(const boost::filesystem::path &path) {
		return true;
	}

	int PropertiesResourceLoader::loadPriority(const boost::filesystem::path &path) {
		if (wcscmp(path.extension().c_str(), L".cfg") == 0) {
			return 1;
		}
		return 10000;
	}

	const char *PropertiesResourceLoader::getName() const {
		return "umc::PropertiesResourceLoader";
	}

	const type_info &PropertiesResourceLoader::getResourceType() const {
		return typeid(Properties);
	}
}