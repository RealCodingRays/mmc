#include "umc\io\ResourceManager.h"

#include "umc\io\resource\TextLoader.h"
#include "umc\io\resource\PropertiesLoader.h"
#include "umc\io\resource\ImageLoader.h"
#include "umc\io\resource\FontLoader.h"

#include "umc\Platform.h"
#include <cstring>
#include <queue>
#include <iostream>
#include <fstream>

#include "umc\utils\Hash.h"

namespace umc {
	// ResourceManager

	ResourceManager ResourceManager::mgr;

	ResourceManager::ResourceManager() {
		packages[std::string{ "umc" }] = boost::filesystem::current_path();
		packages[std::string{ "umc" }] += "/";

		nextID = 1024;
		ResourceLoader *textLdr = new TextResourceLoader{};
		loaderNames[std::string{ textLdr->getName() }] = TextResourceLoader::id;
		loaders[TextResourceLoader::id] = textLdr;

		ResourceLoader *propLdr = new PropertiesResourceLoader{};
		loaderNames[std::string{ propLdr->getName() }] = PropertiesResourceLoader::id;
		loaders[PropertiesResourceLoader::id] = propLdr;

		ResourceLoader *pngLdr = new ImageResourceLoader{};
		loaderNames[std::string{ pngLdr->getName() }] = ImageResourceLoader::id;
		loaders[ImageResourceLoader::id] = pngLdr;

		ResourceLoader *fntLdr = new FontResourceLoader{};
		loaderNames[std::string{ fntLdr->getName() }] = FontResourceLoader::id;
		loaders[FontResourceLoader::id] = fntLdr;
	}

	ResourceManager::~ResourceManager() {
		for (auto data : resources) {
			if (data.second.refcounter != 0) {
				std::cout << "Dangeling resource: " << data.second.path << std::endl;
			}
			data.second.instance->close();
			delete data.second.instance;
		}

		for (auto loader : loaders) {
			delete loader.second;
		}
	}

	ResourceManager &ResourceManager::get() {
		return mgr;
	}

	bool ResourceManager::registerPackage(const char *name, const boost::filesystem::path &path) {
		std::string sname{ name };
		if (sname.compare("umc") == 0) {
			std::cout << "[ERROR][ResourceManager.cpp]: Tried to register package umc!" << std::endl;
			return false;
		}

		if (!ResourceName::isValidName(name)) {
			std::cout << "[ERROR][ResourceManager.cpp]: Invalid package name: " << sname << std::endl;
			return false;
		}

		if (!boost::filesystem::is_directory(path)) {
			std::cout << "[ERROR][ResourceManager.cpp]: Tried to register non directory package: " << sname << " -> " << path << std::endl;
			return false;
		}

		auto it = packages.find(std::string{ name });
		if (it != packages.end()) {
			std::cout << "[ERROR][ResourceManager.cpp]: Package is already registered: " << sname << std::endl;
			return false;
		}

		packages[std::string{ name }] = path;
		return true;
	}

	const boost::filesystem::path ResourceManager::getPackage(const char *name) {
		auto it = packages.find(std::string{ name });
		if (it != packages.end()) {
			return it->second;
		}

		throw std::runtime_error("Failed to find package!");
	}

	LoaderID ResourceManager::registerLoader(ResourceLoader *loader) {
		if (loader != nullptr) {
			loaderNames[std::string{ loader->getName() }] = nextID;
			loaders[nextID] = loader;
			return nextID++;
		}
		else {
			std::cout << "[ERROR][ResourceManager.cpp]: Tried to register nullptr loader!" << std::endl;
			return 0;
		}
	}

	LoaderID ResourceManager::getLoader(const char *name) {
		auto it = loaderNames.find(std::string{ name });
		if (it != loaderNames.end()) {
			return it->second;
		}
		else {
			return 0;
		}
	}

	ResourceLoader *ResourceManager::findLoader( const boost::filesystem::path &path, const type_info &info) {
		struct lstrc {
		public:
			ResourceLoader *loader;
			int priority;

			bool operator >(const lstrc &other) const {
				return priority < other.priority;
			}
			bool operator <(const lstrc &other) const {
				return priority > other.priority;
			}
			bool operator >=(const lstrc &other) const {
				return priority <= other.priority;
			}
			bool operator <=(const lstrc &other) const {
				return priority >= other.priority;
			}
			bool operator ==(const lstrc &other) const {
				return priority == other.priority;
			}
			bool operator !=(const lstrc &other) const {
				return priority != other.priority;
			}
		};
		std::priority_queue<lstrc> sloaders;

		for (auto p : loaders) {
			if (p.second->getResourceType() == info) {
				sloaders.push(lstrc{ p.second, p.second->loadPriority(path) });
			}
		}

		do {
			const lstrc &ldr = sloaders.top();
			if (ldr.loader->canLoad(path)) {
				return ldr.loader;
			}
			std::cout << "Loader " << ldr.loader->getName() << " unable to load" << std::endl;
			sloaders.pop();
		} while (!sloaders.empty());

		return nullptr;
	}

	ResourceManager::ResourceData *ResourceManager::getResource(const boost::filesystem::path &path, const type_info &info) {
		UI64 hash = arrayHash<wchar_t>(path.c_str(), wcslen(path.c_str()));

		auto it = resources.find(hash);
		if (it != resources.end()) {
			if (it->second.instance->getResourceType() == info) {
				return &(it->second);
			}
			else {
				std::cout << "[ERROR][ResourceManager.cpp]: Resource type collision!" << std::endl;
				return nullptr;
			}
		}

		ResourceLoader *loader = findLoader(path, info);
		if (loader == nullptr) {
			std::cout << "[ERROR][ResourceManager.cpp]: Unable to find compatible loader " << path << std::endl;
			return nullptr;
		}

		ResourceInstance *resource = loader->load(path);
		if (resource) {
			ResourceData &ref = resources[hash];
			ref.instance = resource;
			ref.path = path;
			ref.refcounter = 0;

			return &ref;
		}
		else {
			std::cout << "[ERROR][ResourceManager.cpp]: Failed to load resource " << path << std::endl;
			return nullptr;
		}
	}

	ResourceManager::ResourceData *ResourceManager::getResourceExplicit(const boost::filesystem::path &path, const type_info &info, LoaderID id) {
		UI64 hash = arrayHash<wchar_t>(path.c_str(), wcslen(path.c_str()));

		auto it = resources.find(hash);
		if (it != resources.end()) {
			if (it->second.instance->getResourceType() == info) {
				return &(it->second);
			}
			else {
				std::cout << "[ERROR][ResourceManager.cpp]: Resource type collision!" << std::endl;
				return nullptr;
			}
		}

		auto ld = loaders.find(id);
		if (ld == loaders.end()) {
			std::cout << "[ERROR][ResourceManager.cpp]: Unable to find specified loader!" << std::endl;
			return nullptr;
		}

		if (ld->second->getResourceType() != info) {
			std::cout << "[ERROR][ResourceManager.cpp]: Requested type and type supplied by specified loader do not match!" << std::endl;
			return nullptr;
		}

		ResourceInstance *resource = ld->second->load(path);
		if (resource) {
			ResourceData &ref = resources[hash];
			ref.instance = resource;
			ref.path = path;
			ref.refcounter = 0;

			return &ref;
		}
		else {
			std::cout << "[ERROR][ResourceManager.cpp]: Failed to load resource " << path << std::endl;
			return nullptr;
		}
	}

	void ResourceManager::doGC() {
		for (auto it = resources.cbegin(); it != resources.cend();) {
			if (it->second.refcounter == 0) {
				it->second.instance->close();
				delete it->second.instance;
				it = resources.erase(it);
			}
			else {
				++it;
			}
		}
	}
}