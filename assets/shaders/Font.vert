#version 450

in vec2 pos;

uniform sampler1D dim;
uniform sampler1D uvs;
uniform vec2 base_size;
uniform vec2 base_pos;
uniform float uv_scale;

out vec2 frag_uv;
flat out uint frag_ch;

void main() {
	vec4 dimensions = texelFetch(dim, gl_InstanceID, 0);
	vec2 offset = dimensions.rg;
	vec2 size = dimensions.ba;
	vec4 uvdata = texelFetch(uvs, gl_InstanceID, 0);
	vec2 uvbase = uvdata.rg;
	uint uvchnl = uint(uvdata.b);

	frag_uv = vec2(uvbase + vec2(pos.x, 1 - pos.y) * size) * uv_scale;
	frag_ch = uvchnl;

	gl_Position = vec4((pos * size + offset) * base_size + base_pos, 0, 1);
}