#version 450

in vec2 frag_uv;

uniform sampler2D img;
uniform sampler2D bloom;

uniform int bloomlvls = 1;

out vec3 color;

void main() {
	vec3 col = texture(img, frag_uv).rgb;

	vec3 blo;
	for(int i = 0; i < bloomlvls; i++) {
		blo += textureLod(bloom, frag_uv, i).rgb;
	}
	blo = blo / (bloomlvls / 2);

	color = col + blo;
}