#version 450

in vec3 frag_pos;
in mat3 frag_tbn;
in vec2 frag_uv;

uniform sampler2D albedo;
uniform sampler2D normal;
uniform sampler2D metalic;

out vec3 g_pos;
out vec3 g_normal;
out vec3 g_albedo;
out vec3 g_material;

void main() {
	g_pos = frag_pos;

	vec3 tnormal = texture(normal, frag_uv).rgb;
	if(tnormal == vec3(0, 0, 0)) {
		g_normal = normalize(frag_tbn * vec3(0, 0, 1));
	} else {
		g_normal = normalize(frag_tbn * normalize(tnormal * 2.0 - 1.0));
	}
	//g_normal = normalize(frag_tbn * vec3(1, 0, 0));

	g_albedo = texture(albedo, frag_uv).rgb;

	g_material = vec3(texture(metalic, frag_uv).r, 0.0, 0.0);
}