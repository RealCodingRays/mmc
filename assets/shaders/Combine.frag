#version 450

in vec2 frag_uv;

uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D albedo;
uniform sampler2D material;

uniform sampler2DShadow sun;
uniform mat4 sunwtos;
uniform vec3 sunDir;
uniform vec3 sunCol;

uniform sampler1D lightsColor;
uniform sampler1D lightsPosition;
uniform uint lightcnt;

uniform vec3 camPos;

out vec3 color;

const float PI = 3.14159265359;

vec3 fresnelSchlick(float cosTheta, vec3 f0);
float DistributionGGX(vec3 n, vec3 h, float roughness);
float GeometrySchlickGGX(float ndotv, float roughness);
float GeometrySmith(vec3 n, vec3 v, vec3 l, float roughness);
float ShadowVal(vec3 spos, vec3 normal, vec3 ldir);

void main() {
	vec3 frag_pos = texture(position, frag_uv).rgb;
	vec3 frag_normal = texture(normal, frag_uv).rgb;
	vec3 frag_albedo = texture(albedo, frag_uv).rgb;
	vec3 frag_material = texture(material, frag_uv).rgb;
	float frag_roughness = frag_material.r;
	float frag_metallic = frag_material.g;
	float frag_ao = 1 - frag_material.b;

	if(frag_normal == vec3(0, 0, 0)) {
		discard;
		return;
	}

	vec3 v = normalize(camPos - frag_pos);
	vec3 lo =  vec3(0);

	// Calculate sun light
	vec4 sunspos = sunwtos * vec4(frag_pos, 1);
	float shadval = ShadowVal(sunspos.xyz / sunspos.w, normalize(frag_normal), normalize(-sunDir));
	if(shadval > 0.2) {
		vec3 l = normalize(-sunDir);
		vec3 h = normalize(l + v);

		vec3 radiance = sunCol * shadval;

		vec3 f0 = vec3(0.04); 
		f0 = mix(f0, frag_albedo, frag_metallic);
		vec3 f = fresnelSchlick(max(dot(h, v), 0.0), f0);
	
		float ndf = DistributionGGX(frag_normal, h, frag_roughness);       
		float g   = GeometrySmith(frag_normal, v, l, frag_roughness);

		vec3 nominator    = ndf * g * f;
		float denominator = 4 * max(dot(frag_normal, v), 0.0) * max(dot(frag_normal, l), 0.0) + 0.001; 
		vec3 specular     = nominator / denominator;

		vec3 kS = f;
		vec3 kD = vec3(1.0) - kS;
  
		kD *= 1.0 - frag_metallic;
  
		float ndotl = max(dot(frag_normal, l), 0.0);        
		lo += (kD * frag_albedo / PI + specular) * radiance * ndotl;
	}

	// Calculate point lights
	for(int i = 0; i < lightcnt; i++) {
		vec3 lpos = texelFetch(lightsPosition, i, 0).rgb;
		vec3 lcol = texelFetch(lightsColor   , i, 0).rgb;

		vec3 l = normalize(lpos - frag_pos);
		vec3 h = normalize(v + l);
		
		float distance    = length(lpos - frag_pos);
		float attenuation = 1.0 / (distance * distance);
		vec3 radiance     = lcol * attenuation;

		if(length(radiance) < 0.05) {
			continue;
		}

		vec3 f0 = vec3(0.04); 
		f0      = mix(f0, frag_albedo, frag_metallic);
		vec3 f  = fresnelSchlick(max(dot(h, v), 0.0), f0);
		
		float ndf = DistributionGGX(frag_normal, h, frag_roughness);       
		float g   = GeometrySmith(frag_normal, v, l, frag_roughness);

		vec3 nominator    = ndf * g * f;
		float denominator = 4 * max(dot(frag_normal, v), 0.0) * max(dot(frag_normal, l), 0.0) + 0.001; 
		vec3 specular     = nominator / denominator;
		
		vec3 kS = f;
		vec3 kD = vec3(1.0) - kS;
  
		kD *= 1.0 - frag_metallic;
  
		float ndotl = max(dot(frag_normal, l), 0.0);        
		lo += (kD * frag_albedo / PI + specular) * radiance * ndotl;
	}

	vec3 ambient = vec3(0.1) * frag_albedo * frag_ao;
	color		 = ambient + lo; 
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

float ShadowVal(vec3 spos, vec3 normal, vec3 ldir) {
	spos = spos * 0.5 + 0.5;

	float bias = max(0.005 * (1.0 - dot(normal, ldir)), 0.0005);
	spos.z -= bias;

	return texture(sun, spos);
}