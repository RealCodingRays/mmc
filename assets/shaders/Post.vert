#version 450

in vec2 pos;
in vec2 uv;

out vec2 frag_uv;

void main() {
	gl_Position = vec4(pos, 0, 1);
	frag_uv = uv;
}