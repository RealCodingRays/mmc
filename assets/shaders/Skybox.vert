#version 450

in vec3 pos;

uniform mat4 wtoc;
uniform mat4 ctos;

out vec3 frag_uv;
out vec3 frag_pos;

void main() {
	vec4 wpos = ctos * wtoc * vec4(pos * 3, 1.0);
	frag_pos = wpos.xyz / wpos.w;
	gl_Position = wpos;
	frag_uv = pos;
}