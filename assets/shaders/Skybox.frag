#version 450

in vec3 frag_pos;
in vec3 frag_uv;

uniform vec3 color_horizon = vec3(0.4824, 0.7216, 0.8784);
uniform vec3 color_sky	   = vec3(0.0941, 0.6118, 0.9569);
uniform vec3 sunDir;

out vec3 color;

void main() {
	vec3 v = normalize(frag_uv);

	float vdt = -dot(v, vec3(0, 1, 0));
	if(vdt < 0) {
		vdt = 0;
	}
	color = mix(color_horizon, color_sky, vdt);

	float vds = -dot(v, normalize(sunDir));
	if(vds > 0.999) {
		vds = 8;
	} else {
		vds = 0;
	}
	color = color + vec3(0.98f, 0.97f, 0.9f) * vds;
}