#version 450

in vec2 frag_uv;

uniform sampler2D img;
uniform float luminance = 0.08;
uniform vec3 lumfac = vec3(0.2126, 0.7152, 0.0722);

out vec3 color;

void main() {
	vec3 frag_color = texture(img, frag_uv).rgb;

	float brightness = dot(frag_color, lumfac);

	frag_color -= dot(vec3(luminance), lumfac);
	color = max(frag_color, 0);

	/*frag_color *= 0.18 / (luminance + 0.001);
	frag_color *= (1.0 + (frag_color / (0.8 * 0.8)));
	frag_color -= 5;

	frag_color = max(frag_color, 0);

	color = frag_color / 15;*/
}