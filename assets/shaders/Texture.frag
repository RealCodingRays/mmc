#version 450

uniform sampler2D tex;

in vec2 frag_uv;

out vec4 color;

void main() {
	vec4 col = texture(tex, frag_uv);
	col.a = 1;
	color = col;
}