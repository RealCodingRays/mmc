#version 450

in vec3 pos;
in vec3 normal;
in vec3 tangent;
in vec2 uv;

in ivec3 offset;
in mat3 rotation;

uniform mat4 wtoc;
uniform mat4 ctos;
uniform mat4 wtocn;
uniform mat4 ctosn;

out vec3 frag_pos;
out mat3 frag_tbn;
out vec2 frag_uv;

void main() {
	vec4 wpos = vec4(((rotation * (pos - 0.5)) + offset), 1);
	//vec4 wpos = vec4(pos + offset, 1);
	frag_pos = wpos.xyz;
	gl_Position = ctos * wtoc * wpos;

	vec3 t = rotation * normalize(vec3(vec4(tangent, 0.0)));
	vec3 n = rotation * normalize(vec3(vec4(normal, 0.0)));
	t = normalize(t - dot(t, n) * n);
	vec3 b = cross(n, t);
	frag_tbn = mat3(t, b, n);

	//vec3 uvsx = pos * t;
	//vec3 uvsy = pos * b;
	//vec2 uvscale = vec2(uvsx.x + uvsx.y + uvsx.z, uvsy.x + uvsy.y + uvsy.z);
	frag_uv = uv;
}