#version 450

in vec2 frag_uv;
flat in uint frag_ch;

uniform vec4 frag_color;
uniform sampler2D font;

out vec4 color;

void main() {
	switch(frag_ch) {
	case 1:
		color = vec4(1, 1, 1, texture(font, frag_uv).b) * frag_color;
		break;
	case 2:
		color = vec4(1, 1, 1, texture(font, frag_uv).g) * frag_color;
		break;
	case 4:
		color = vec4(1, 1, 1, texture(font, frag_uv).r) * frag_color;
		break;
	case 8:
		color = vec4(1, 1, 1, texture(font, frag_uv).a) * frag_color;
		break;
	default:
		color = vec4(1, 0, 1, 1);
	}
	if(color.a == 0) {
		discard;
	}
}