#version 450

in vec3 pos;
in vec3 normal;
in vec3 tangent;
in vec2 uv;

uniform mat4 mtow;
uniform mat4 wtoc;
uniform mat4 ctos;
uniform mat4 mtown;
uniform mat4 wtocn;
uniform mat4 ctosn;

out vec3 frag_pos;
out mat3 frag_tbn;
out vec2 frag_uv;

void main() {
	vec4 cpos = mtow * vec4(pos, 1);
	frag_pos = cpos.xyz / cpos.w;
	gl_Position = ctos * wtoc * cpos;

	vec3 t = normalize(vec3(mtown * vec4(tangent, 0.0)));
	vec3 n = normalize(vec3(mtown * vec4(normal, 0.0)));
	t = normalize(t - dot(t, n) * n);
	vec3 b = cross(n, t);
	frag_tbn = mat3(t, b, n);

	frag_uv = uv;
}