#version 450

in vec2 frag_uv;

uniform sampler2D img;

out vec4 color;

void main() {
	color = vec4(textureLod(img, frag_uv, 0).rgb, 1);
}