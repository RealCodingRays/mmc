#version 450

in vec2 frag_uv;

uniform sampler2D img;
uniform int lod = 0;

out vec3 color;

uniform float offset[3] = float[]( 0.0, 1.3846153846, 3.2307692308 );
uniform float weight[3] = float[]( 0.2270270270, 0.3162162162, 0.0702702703 );

void main() {
	vec2 uv_size = 1.0 / textureSize(img, lod);
	uv_size.y = 0;

	vec3 result = texture(img, frag_uv).rgb * weight[0];
	
	result += textureLod(img, frag_uv + offset[1] * uv_size, lod).rgb * weight[1];
	result += textureLod(img, frag_uv - offset[1] * uv_size, lod).rgb * weight[1];
		
	result += textureLod(img, frag_uv + offset[2] * uv_size, lod).rgb * weight[2];
	result += textureLod(img, frag_uv - offset[2] * uv_size, lod).rgb * weight[2];

	color = result;
}