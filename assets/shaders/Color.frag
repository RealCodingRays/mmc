#version 450

in vec2 frag_uv;

uniform sampler2D img;

uniform float avrgLum = 1;
uniform float exposure = 0.15;
uniform float white = 0.928;
uniform mat3 rgb2xyz = mat3(0.5767309, 0.1855540, 0.1881852,
							0.2973769, 0.6273491, 0.0752741,
							0.0270343, 0.0706872, 0.9911085);
uniform mat3 xyz2rgb = mat3(2.0413690,-0.5649464,-0.3446944,
						   -0.9692660, 1.8760108, 0.0415560,
							0.0134474,-0.1183897, 1.0154096);

out vec3 color;

void main() {
    const float gamma = 2.2;
    vec3 hdrColor = texture(img, frag_uv).rgb;

	vec3 xyz = rgb2xyz * hdrColor;
	
	float sumxyz = xyz.x + xyz.y + xyz.z + 0.00001;
	vec2 xyY = xyz.xy / sumxyz;

	float l = (exposure * xyz.y) / avrgLum;
	l = (l * (1 + (l / (white * white)))) / (1 + l);

	xyz = vec3((l * xyY.x) / xyY.y,
			   l,
			   (l * (1 - xyY.x - xyY.y)) / xyY.y);

	vec3 mapped = xyz2rgb * xyz;
    mapped = pow(mapped, vec3(1.0 / gamma));
  
    color = mapped;
}