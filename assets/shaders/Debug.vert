#version 450

in vec3 vert;
in vec3 color;

uniform mat4 wtoc;
uniform mat4 ctos;

uniform vec3 pos;

out vec3 frag_color;

void main() {
	frag_color = color;

	gl_Position = ctos * wtoc * vec4(vert + pos, 1);
}