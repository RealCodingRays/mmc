#version 450 

in vec3 frag_color;

out vec3 color;

void main() {
	color = frag_color;
}